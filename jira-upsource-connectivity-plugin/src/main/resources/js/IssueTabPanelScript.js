

(function ($) {
        JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function(e, context, reason){
            if(reason == JIRA.CONTENT_ADDED_REASON.pageLoad){
                JIRA.ViewIssueTabs.onTabReady(function () {
                    $diff_display = AJS.$("#diff_display");
                    AJS.$(document).on({
                        diffLoadStart: function(){$diff_display.addClass("loading");},
                        diffLoadStop: function(){$diff_display.removeClass("loading");}
                    })
                    actionButtons();
                    accordion();
                });
            }else if(reason == JIRA.CONTENT_ADDED_REASON.panelRefreshed){
                JIRA.ViewIssueTabs.onTabReady(function () {
                    $diff_display = AJS.$("#spinner_gif");
                    AJS.$(document).on({
                        diffLoadStart: function(){$diff_display.addClass("loading");},
                        diffLoadStop: function(){$diff_display.removeClass("loading");}
                    })
                    actionButtons();
                    accordion();
                });
            }
        });
    console.log(">>> UPSOURCE CONNECTIVITY PLUGIN ISSUE TAB PANEL JS LOADED <<<");
})(AJS.$ || jQuery);

function accordion() {
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        }
    }
}

function displayDiff(diff, rev){
    AJS.$.event.trigger({type:'diffLoadStart',source:'ajax'});
    var message = "Clicked diff " + diff + " on rev " + rev;
    var diffList = AJS.$("#diffList_"+rev);
    var diffRow = diffList.find("#diff_"+diff);
    var project = diffRow.attr("data-project");
    var review = diffRow.attr("data-reviewid");
    var projectid = diffRow.attr("data-projectid");
    var revision = diffRow.attr("data-revisionid");
    var file = diffRow.attr("data-filename");
    var data = '{'
            +'"projectKey":"'+project
            +'","reviewId":"'+review
            +'","projectId":"'+projectid
            +'","revisionId":"'+revision
            +'","fileName":"'+file
            +'"}';
    AJS.$.ajax({
            url:AJS.contextPath() + "/rest/upsourceAction/1.0/reviewStateChange/displayDiff",
            type:"POST",
            contentType: "application/json",
            data:data,
            processData:false
        })
                .done(function(data){                    
                    console.log("success");
                    message = data.text;
                    var table=AJS.$("<table></table>");                    
                    for(var line in message){
                        var content = message[line]+"\n";
                        var oldnumber = data.oldLineNumbers[line];
                        if(oldnumber == 0){
                            oldnumber = "";
                        }
                        var newnumber = data.newLineNumbers[line];
                        if(newnumber == 0){
                            newnumber = "";
                        }
                        var html = "<tr><td style=\"width:1em\" class=\"unselectable\">"+oldnumber+"</td><td style=\"width:1em\" class=\"unselectable\">"+newnumber+"</td><td><pre><code>"+content+"</code></pre></td></tr>"
                        table.append(html);
                    }
                    var style = "width:100%;"
                        +"height:30em;"
                        +"overflow-x:scroll;"
                        +"overflow-y:scroll;";
                    AJS.$("#diff_display").attr("style",style);
                    AJS.$.event.trigger({type:'diffLoadStop',source:'ajax'});
                    AJS.$("#diff_display").html(table);
                })
                .fail(function (xhr, ajaxOptions, thrownError) {
                    console.log("failure");
                    if(xhr.status == 401){
                        var flag = AJS.flag({
                            type: 'error',
                            title: 'Authorization failed.',
                            close: 'auto'
                        });
                    }else{
                        var flag = AJS.flag({
                            type: 'error',
                            title: 'action failed, please try again. If this problem persists, contact an administrator.',
                            close: 'auto'
                        });
                    }
                    console.log(xhr.responseText);
                    
                    AJS.$.event.trigger({type:'diffLoadStop',source:'ajax'});
                    AJS.$("#codeWindow").text(message);
                });
}

function actionButtons() {
    var cookie = getCookie("Upsource_Auth");
    if(AJS.$("#empty_block").length > 0){
        AJS.$("#creation_button").off('click').click(function(){
            if(cookie){
                createReview();
            }else{
                AJS.dialog2("#creation_dialog").show();
            }
        });
        AJS.$("#creation_form").off('submit').submit(function(e){
            e.preventDefault();
            createReview();
        });

        AJS.$(".closebutton").off('click').click(function (e) {
            e.preventDefault();
            AJS.dialog2("#creation_dialog").hide();
        });
    }
    else{
        AJS.$(".accshowbutton").off('click').click(function () {
            var count = detectCount(this);
            AJS.$("#state_"+count).val(3);
            AJS.$("#actiontype_"+count).text("Accept");
            if(cookie){
                authorizeAction(count);
            }else{
                AJS.dialog2("#Acc_dialog_" + count).show();
            }
        });

        AJS.$(".decshowbutton").off('click').click(function () {
            var count = detectCount(this);
            AJS.$("#state_"+count).val(4);
            AJS.$("#actiontype_"+count).text("Raise Concern");
            if(cookie){
                authorizeAction(count);
            }else{
                AJS.dialog2("#Acc_dialog_" + count).show();
            }
        });

        AJS.$(".acc").off('submit').submit(function(e){
            var count = detectCount(this);
            e.preventDefault();
            authorizeAction(count)
        });

        AJS.$(".closebutton").off('click').click(function (e) {
            var count = detectCount(this);
            e.preventDefault();
            AJS.dialog2("#Acc_dialog_" + count).hide();
        });
    }
}

function authorizeAction(i){
    var data = getData(i);
    if (AJS.$.isArray(data)){
        data.forEach(function(element){
            var flag = AJS.flag({
                type: 'error',
                title: element,
                close: 'manual'
            });
        });
    }else{
        var components = data.user + ":" + data.password;
        var payload = JSON.stringify(data);
        AJS.$.ajax({
            url:AJS.contextPath() + "/rest/upsourceAction/1.0/reviewStateChange/state",
            type:"POST",
            contentType: "application/json",
            data:payload,
            processData:false
        })
                .done(function(){
                    console.log("succes");
                    var flag = AJS.flag({
                        type: 'success',
                        title: 'action succesful.',
                        close: 'auto'
                    });
                    AJS.dialog2("#Acc_dialog_" + i).hide();
                    AJS.$("#Acc_dialog_show_button_"+i).prop('disabled', true);
                    AJS.$("#Dec_dialog_show_button_"+i).prop('disabled', true);
                    setCookie("Upsource_Auth", components, 61);    
                })
                .fail(function (xhr, ajaxOptions, thrownError) {
                    console.log("failure");
                    if(xhr.status == 401){
                        var flag = AJS.flag({
                            type: 'error',
                            title: xhr.responseText,
                            close: 'auto'
                        });
                    }else{
                        var flag = AJS.flag({
                            type: 'error',
                            title: 'action failed, please try again. If this problem persists, contact an administrator.',
                            close: 'auto'
                        });
                    }
                    console.log(xhr.responseText);
                });
    }
}

function getData(i){
    var err = [];
    var cookie = getCookie("Upsource_Auth");
    if(cookie){
        var components = cookie.split(":");
        var user = components[0];
        var pass = components[1];
    }else{
        var user = AJS.$("#Acc_user_" + i).val();
        if(!user){
            err.push("Please fill in user");
        }
        var pass = AJS.$("#Acc_pass_" + i).val();
        if(!pass){
            err.push("Please fill in password")
        }
    }
    var state = AJS.$("#state_" + i).val();
    if(err.length != 0){
        return err;
    }
    var payload = {
        'jiraProjectId':AJS.$("#jira_project_"+i).val(),
        'upsourceProjectId':AJS.$("#upsource_project_"+i).val(),
        'reviewId':AJS.$("#review_"+i).val(),
        'user':user,
        'password':pass,
        'state':state
    };
    return payload;
}

function createReview(){
    var data = getDataForCreateReview();
    if (AJS.$.isArray(data)){
        data.forEach(function(element){
            var flag = AJS.flag({
                type: 'error',
                title: element,
                close: 'manual'
            });
        });
    }else{
        var components = data.user + ":" + data.password;
        var payload = JSON.stringify(data);
        AJS.$.ajax({
            url:AJS.contextPath() + "/rest/upsourceAction/1.0/reviewStateChange/create",
            type:"POST",
            contentType: "application/json",
            data:payload,
            processData:false
        })
                .done(function(data){
                    console.log("succesfully completed AJAX");
                    console.log(data);
                    switch(data.status){
                        case "Could not resolve revisions.":
                            var flag = AJS.flag({
                                type: 'error',
                                title: 'Could not resolve revision, direct interaction required.',
                                close: 'auto'
                            });
                            break;
                        case "revision created":
                            var flag = AJS.flag({
                                type: 'success',
                                title: 'Review created. Page reloads in 10 seconds.',
                                close: 'auto'
                            });
                            if(!data.addedSelf){
                                var flag = AJS.flag({
                                    type: 'warning',
                                    title: 'You have not been added as a reviewer.',
                                    close: 'auto'
                                });
                            }else{
                                var flag = AJS.flag({
                                    type: 'warning',
                                    title: 'You have been added as a reviewer, but no other reviewers have been assigned.',
                                    close: 'auto'
                                });
                            }
                            AJS.$("#creation_button").prop('disabled', true);
                            setCookie("Upsource_Auth", components, 61);    
                            break;
                    }
                    
                    AJS.dialog2("#creation_dialog").hide();
                    setTimeout(location.reload(true), 10000);
                })
                .fail(function (xhr, ajaxOptions, thrownError) {
                    console.log("failure");
                    if(xhr.status == 401){
                        var flag = AJS.flag({
                            type: 'error',
                            title: 'Authorization failed.',
                            close: 'auto'
                        });
                    }else{
                        var flag = AJS.flag({
                            type: 'error',
                            title: 'action failed, please try again. If this problem persists, contact an administrator.',
                            close: 'auto'
                        });
                    }
                    console.log(xhr.responseText);
                });
    }
        
    
}

function getDataForCreateReview(){
    var err = [];
    var cookie = getCookie("Upsource_Auth");
    if(cookie){
        var components = cookie.split(":");
        var user = components[0];
        var pass = components[1];
    }else{
        var user = AJS.$("#creation_user").val();
        if(!user){
            err.push("Please fill in user");
        }
        var pass = AJS.$("#creation_pass").val();
        if(!pass){
            err.push("Please fill in password")
        }
    }
    var issueKey = AJS.$("#creation_issueKey").val();
    var projectKey = AJS.$("#creation_projectKey").val();
    if(err.length != 0){
        return err;
    }
    var payload = {
        'user':user,
        'password':pass,
        'jiraIssueKey':issueKey,
        'jiraProjectKey':projectKey
    };
    return payload;
}
