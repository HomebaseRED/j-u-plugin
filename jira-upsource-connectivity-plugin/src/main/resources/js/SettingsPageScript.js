
(function ($) { // this closure helps us keep our variables to ourselves.
// This pattern is known as an "iife" - immediately invoked function expression
    console.log(">>> UPSOURCE CONNECTIVITY PLUGIN SETTINGS JS LOADED <<<"); 
    // wait for the DOM (i.e., document "skeleton") to load. This likely isn't necessary for the current case,
    // but may be helpful for AJAX that provides secondary content.
    $(document).ready(function () {        
        // Set triggers for saving and wiping connectiondata on settings page
        AJS.$(".hook").submit(function (e) {
            var count = detectCount(this);
            e.preventDefault();
            updateConfig(count);
        });
        AJS.$(".wipe").click(function (e) {
            var count = detectCount(this);
            e.preventDefault();
            wipeConfig(count);
        });
        AJS.$(".test").click(function (e){
            var count = detectCount(this);
            e.preventDefault();
            testConfig(count);
        });
    });

})(AJS.$ || jQuery);

function testConfig(i){
    AJS.$.ajax({
        url: AJS.contextPath() + "/rest/upsourceAdmin/1.0/testConnection",
        type: "POST",
        contentType: "application/json",
        data: getWipeData(i),
        processData: false
    })
            .done(function (data) {
                console.log(data);
                var message = data.projectName;
                var flag = AJS.flag({
                    type: 'success',
                    title: 'Connection to '+message+' successful.',
                    close: 'auto'
                });
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                var flag = AJS.flag({
                    type: 'error',
                    title: 'Connection failed. Error logged in the browser console.',
                    close: 'auto'
                });
                console.log(xhr.responseText);
            });
}

function wipeConfig(i) {
    AJS.$.ajax({
        url: AJS.contextPath() + "/rest/upsourceAdmin/1.0/wipe",
        type: "POST",
        contentType: "application/json",
        data: getWipeData(i),
        processData: false
    })
            .done(function () {
                console.log("wipe succes");
                var flag = AJS.flag({
                    type: 'success',
                    title: 'wipe succesful',
                    close: 'auto'
                });
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                console.log("wipe failed");
                var flag = AJS.flag({
                    type: 'error',
                    title: 'wipe failed',
                    close: 'auto'
                });
                console.log(xhr.responseText);
            });
}

function updateConfig(i) {
    var data = getData(i);
    if(typeof data === 'string'){
        AJS.$.ajax({
            url: AJS.contextPath() + "/rest/upsourceAdmin/1.0/",
            type: "PUT",
            contentType: "application/json",
            data: data,
            processData: false
        })
                .done(function () {
                    console.log("succes");
                    var flag = AJS.flag({
                        type: 'success',
                        title: 'save succesful',
                        close: 'auto'
                    });
                })
                .fail(function (xhr, ajaxOptions, thrownError) {
                    console.log("failure");
                    var flag = AJS.flag({
                        type: 'error',
                        title: 'save failed, please try again. If this problem persists, contact an administrator',
                        close: 'auto'
                    });
                    console.log(xhr.responseText);
                }); 
    }else{
        data.forEach(function(element){
            var flag = AJS.flag({
                type: 'error',
                title: element,
                close: 'auto'
            });
            JIRA.Messages.showErrorMsg(element);
        });
    }
}

function getData(i) {
    var err = [];
    var host = AJS.$("#host_" + i).val();
    if(!host){
        err.push("Please fill in host");
    }
    var user = AJS.$("#username_" + i).val();
    if(!user){
        err.push("Please fill in user");
    }
    var pass = AJS.$("#password_" + i).val();
    if(!pass){
        err.push("Please fill in password")
    }
    if(err.length != 0){
        return err;
    }
    var payload =
            '{ "host": "' + host
            + '", "username": "' + user
            + '", "password": "' + pass
            + '", "projectKey": "' + AJS.$("#projectKey_" + i).val()
            + '", "upsourceProjectId": "' + AJS.$("#upsrc_" + i).val()
            + '" }';
    console.log(payload);
    return payload;
}

function getWipeData(i) {
    var payload =
            '{ "projectKey": "' + AJS.$("#projectKey_" + i).val() + '" }';
    console.log(payload);
    return payload;
}
