
console.log(">>> UPSOURCE CONNECTIVITY PLUGIN GENERAL JS LOADED <<<")

function detectCount(object) {
    var id = object.id;
    var regex = new RegExp('_\\d+$');
    var match = id.search(regex);
    var count;
    if (match > 0) {
        count = id.substring(match + 1);
    }
    return count;
}

//src:https://www.w3schools.com/js/js_cookies.asp
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function deleteCookie(cname){
    console.log('deleting cookie ' + cname);
    document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:01 UTC;path=/";
}