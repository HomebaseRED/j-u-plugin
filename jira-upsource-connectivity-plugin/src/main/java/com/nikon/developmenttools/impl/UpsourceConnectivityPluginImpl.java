package com.nikon.developmenttools.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;

import javax.inject.Inject;
import javax.inject.Named;
import com.nikon.developmenttools.api.UpsourceConnectivityPlugin;

@ExportAsService ({UpsourceConnectivityPlugin.class})
@Named ("UpsourceConnectivityPlugin")
public class UpsourceConnectivityPluginImpl implements UpsourceConnectivityPlugin
{
    @ComponentImport
    private final ApplicationProperties applicationProperties;

    @Inject
    public UpsourceConnectivityPluginImpl(final ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    public String getName()
    {
        if(null != applicationProperties)
        {
            return applicationProperties.getDisplayName();
        }
        
        return "UpsourceConnectivityPlugin";
    }
}