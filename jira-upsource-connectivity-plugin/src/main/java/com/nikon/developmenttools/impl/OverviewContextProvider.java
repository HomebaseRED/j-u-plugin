/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.developmenttools.impl;

import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.nikon.upsourcedtos.ResultDTO;
import com.nikon.upsourcedtos.ReviewListDTO;
import dto.UpsourceConfig;
import dto.UpsourceConnectionFailureException;
import dto.UpsourceConnectivityException;
import dto.UpsourceUnsetConfigException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.*;
import javax.inject.Inject;

/**
 *
 * @author RVanhuysse
 */
public class OverviewContextProvider extends AbstractJiraContextProvider {

    @ComponentImport
    private final ProjectManager projectManager;
    
    @Inject
    private UpsourceConnectivityExtentionMethods methodContainer;
    
    @Inject
    public OverviewContextProvider(ProjectManager projectManager){
        this.projectManager = projectManager;
    }

    public UpsourceConnectivityExtentionMethods getMethodContainer() {
        return methodContainer;
    }

    public void setMethodContainer(UpsourceConnectivityExtentionMethods methodContainer) {
        this.methodContainer = methodContainer;
    }
    
    @Override
    public Map getContextMap(ApplicationUser au, JiraHelper jh) {
        Map contextMap = new HashMap();
        List<String> exceptionMessages = new ArrayList<String>();
        try{
            Project currentProject = (Project) (jh.getProject());
            String parseResult = "";
            if(currentProject == null){ 
                String target = jh.getRequest().getRequestURI();
                Pattern p = Pattern.compile("\\w+$");
                Matcher m = p.matcher(target);
                m.find();
                parseResult = m.group();
                currentProject = this.projectManager.getProjectObjByKey(parseResult);
            }
            
            UpsourceConfig uc = this.methodContainer.loadConfig(currentProject.getKey());
            HashMap<String, String> ucProperties = new HashMap<String, String>();
            ucProperties.put("host", uc.getHost());
            ucProperties.put("user", uc.getUsername());
            ucProperties.put("upsourceId", uc.getUpsourceProjectId());
            contextMap.put("config", ucProperties);
            
            ResultDTO result = this.methodContainer.getReviews(currentProject, "");
            ReviewListDTO response = (ReviewListDTO) result.getResult();
            if(response.getTotalCount()>0){
                response = (ReviewListDTO) this.methodContainer.getCommentsForReviews(response, currentProject).getResult();
            }
            contextMap.put("reviewsDTO", response);
            
        }catch (UpsourceConnectionFailureException ucfe) {
            System.err.println(this.methodContainer.now()+" - UPSOURCE CONNECTION EXCEPTION: " + ucfe.getMessage() + " STACKTRACE: ");
            ucfe.printStackTrace();
            if (ucfe.getCause() != null && ucfe.getCause().getCause() instanceof java.net.UnknownHostException) {
                exceptionMessages.add("Error resolving host, please check the Upsource Connectivity Settings for this project and your network settings.");
            } else {
                exceptionMessages.add(ucfe.toString());
            }
        } catch (UpsourceUnsetConfigException uuce) {
            System.err.println(this.methodContainer.now()+" - UPSOURCE CONFIGRATION EXCEPTION: " + uuce.getMessage() + " STACKTRACE: ");
            uuce.printStackTrace();
            exceptionMessages.add(uuce.getMessage());
        } catch (UpsourceConnectivityException uce) {
            System.err.println(this.methodContainer.now()+" - UPSOURCE EXCEPTION: " + uce.getMessage() + " STACKTRACE: ");
            uce.printStackTrace();
            if (uce.getCause() != null) {
                exceptionMessages.add(uce.getCause().toString());
            } else {
                exceptionMessages.add(uce.toString());
            }
        } catch (Exception ex) {
            exceptionMessages.add(ex.toString());
            System.err.println(this.methodContainer.now()+" - EXCEPTION: " + ex.toString() + " STACKTRACE: ");
            ex.printStackTrace();
        }
        contextMap.put("exceptionList", exceptionMessages);
        return contextMap;
    }
    
}
