package com.nikon.developmenttools.impl;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.IssueService.TransitionValidationResult;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.nikon.upsourcedtos.ResultDTO;
import com.nikon.upsourcedtos.ReviewDescriptorDTO;
import com.nikon.upsourcedtos.ReviewIdDTO;
import com.nikon.upsourcedtos.ReviewStateChangedFeedEventBean;
import com.nikon.upsourcedtos.UpsourceWebhookResponse;
import com.nikon.upsourcedtos.UserInfoResponseDTO;
import com.opensymphony.workflow.loader.ActionDescriptor;
import dto.UpsourceConfig;
import dto.UpsourceConnectivityException;
import static dto.UpsourcePluginHttpStatus.ACTOR_UNRESOLVED;
import static dto.UpsourcePluginHttpStatus.AUTHORIZING_USER_NULL;
import static dto.UpsourcePluginHttpStatus.CRITICAL_ERROR;
import static dto.UpsourcePluginHttpStatus.DESERIALIZATION_ERROR;
import static dto.UpsourcePluginHttpStatus.ISSUE_UNRESOLVED;
import static dto.UpsourcePluginHttpStatus.TARGET_ISSUE_NULL;
import static dto.UpsourcePluginHttpStatus.TRANSITION_UNAVAILABLE;
import static dto.UpsourcePluginHttpStatus.TRANSITION_VALIDATION_FAILED;
import static dto.UpsourcePluginHttpStatus.WEBHOOK_DATA_ACTOR_NULL;
import static dto.UpsourcePluginHttpStatus.WEBHOOK_DATA_NULL;
import static dto.UpsourcePluginHttpStatus.WEBHOOK_NOT_SUPPORTED;
import dto.UpsourceUnsetConfigException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static org.codehaus.jackson.map.DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * A resource of message.
 */
@AnonymousAllowed
@Path("/Listener")
public class UpsourceWebhookListener {

    private final UserManager userManager;
    private final ProjectManager projectManager;
    private final IssueService issueService;
    private final WorkflowManager workflowManager;

    private UpsourceConnectivityExtentionMethodsImp methodContainter;

    @Inject
    public UpsourceWebhookListener(
            UpsourceConnectivityExtentionMethodsImp methodContainer) {
        this.userManager = ComponentAccessor.getUserManager();
        this.projectManager = ComponentAccessor.getProjectManager();
        this.issueService = ComponentAccessor.getIssueService();
        this.workflowManager = ComponentAccessor.getWorkflowManager();
        this.methodContainter = methodContainer;
    }

    //<editor-fold desc="getters & setters">    
    public UpsourceConnectivityExtentionMethodsImp getMethodContainter() {
        return methodContainter;
    }

    public void setMethodContainter(UpsourceConnectivityExtentionMethodsImp methodContainter) {
        this.methodContainter = methodContainter;
    }
    //</editor-fold>

    private List<UpsourceConfig> loadAllConfigs() {
        List<UpsourceConfig> response = new ArrayList<UpsourceConfig>();
        for (Project p : projectManager.getProjects()) {
            try {
                response.add(this.methodContainter.loadConfig(p.getKey()));
            } catch (UpsourceUnsetConfigException uuce) {

            } catch (UpsourceConnectivityException uce) {
                throw uce;
            }
        }
        return response;
    }

    @GET
    @Path("/hello")
    @AnonymousAllowed
    @Produces(MediaType.TEXT_PLAIN)
    public Response getMessage() {
        String message = this.methodContainter.now() + " >>> Hello Server";
        System.err.println(message);
        return Response
                .status(200)
                .type(MediaType.TEXT_PLAIN)
                .entity(message).build();
    }

    @POST
    @Path("/Handler")
    @AnonymousAllowed
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response handleWebhook(String json, @Context HttpServletRequest request) {
        if (json != null) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            UpsourceWebhookResponse webhook = null;
            try {
                webhook = (UpsourceWebhookResponse) mapper.readValue(json, UpsourceWebhookResponse.class);
            } catch (IOException ex) {
                String message = this.methodContainter.now()
                    + " >>> closeReview executed - Deserialization ERROR: \nMESSAGE: "
                    + ex.getMessage()
                    + ";\nSTACKTRACE: "
                    + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(ex)
                    + " \n- JSON received: " + json;
//                    + " \n- JSON as ROOT: " + root;
                System.err.println(message);
                return Response.status(DESERIALIZATION_ERROR)
                        .type(MediaType.TEXT_PLAIN)
                        .entity(message)
                        .build();
            }
            switch(webhook.getDataType()){
                case "ReviewStateChangedFeedEventBean":
                    return this.changeReviewState(webhook);
                default:
                    return Response.status(WEBHOOK_NOT_SUPPORTED)
                            .type(MediaType.TEXT_PLAIN)
                            .entity("Received Webhook is not supported.")
                            .build();
            }    
        }
        String message = this.methodContainter.now() 
                + " >>> closeReview executed - Critical error: jsonstring could not be extracted or is null.";
        System.err.println(message);
        return Response.status(CRITICAL_ERROR)
                .type(MediaType.TEXT_PLAIN)
                .entity(message)
                .build();
    }

    private Response changeReviewState(UpsourceWebhookResponse webhook){
        ReviewStateChangedFeedEventBean data = (ReviewStateChangedFeedEventBean) webhook.getData();
        if (data == null) {
            String message = this.methodContainter.now() + " >>> changeReviewState - No data in webhook response";
            System.err.println(message);
            return Response.status(WEBHOOK_DATA_NULL)
                    .type(MediaType.TEXT_PLAIN)
                    .entity(message)
                    .build();
        }
        String actor = null;
        String targetProjectKey = getTargetProjectKey(webhook.getProjectId());
        //extract executing user for authorization
        actor = data.getBase().getActor().getUserName();
        System.err.println(this.methodContainter.now()+" >>> actor: "+actor);
        ApplicationUser user = null;
        if (actor == null || actor.isEmpty()) {
            String message = this.methodContainter.now()+">>> changeReviewState: extracted actor is null";
            System.err.println(message);
            return Response.status(WEBHOOK_DATA_ACTOR_NULL)
                    .type(MediaType.TEXT_PLAIN)
                    .entity(message)
                    .build();
        } else if(actor.equalsIgnoreCase("upsource")){
            user = this.userManager.getUserByName("Upsource");
        }else {
            ResultDTO res = this.methodContainter.findUsers(targetProjectKey, actor);
            try{
                actor = ((UserInfoResponseDTO) res.getResult()).getInfos().get(0).getLogin();
            }catch(Exception ex){
                return Response.status(ACTOR_UNRESOLVED)
                        .type(MediaType.TEXT_PLAIN)
                        .entity(ex)
                        .build();
            }
            user = this.userManager.getUserByName(actor);
        }
        String targetIssueId = "";
        ReviewDescriptorDTO reviewDetails = (ReviewDescriptorDTO)
                this.methodContainter.getReviewDetails(
                        new ReviewIdDTO(webhook.getProjectId(), data.getBase().getReviewId()), targetProjectKey)
                .getResult();
        if(reviewDetails.getIssue() != null && !reviewDetails.getIssue().isEmpty()){
            targetIssueId = reviewDetails.getIssue().get(0).getIssueId();
        }
        if (targetIssueId == null) {
            String message = this.methodContainter.now()+">>> changeReviewState: no target issue was retrieved form upsource";
            System.err.println(message);
            return Response.status(ISSUE_UNRESOLVED)
                    .type(MediaType.TEXT_PLAIN)
                    .entity(message)
                    .build();
        }
        if (user == null) {
            String message = this.methodContainter.now()+">>> changeReviewState: authorizing user is null";
            System.err.println(message);
            return Response.status(AUTHORIZING_USER_NULL)
                    .type(MediaType.TEXT_PLAIN)
                    .entity(message)
                    .build();
        }
        //load the target issue from the system issueService
        IssueService.IssueResult issueLookup = this.issueService.getIssue(user, targetIssueId);
        Issue i = issueLookup.getIssue();
        if(i == null){
            String message = this.methodContainter.now()
                            +" >>> targetIssueId: "+targetIssueId
                            +"; ApplicationUser: "+user
                            +"; issueServiceErrors: "+issueLookup.getErrorCollection();
            System.err.println(message);
            return Response.status(TARGET_ISSUE_NULL)
                    .type(MediaType.TEXT_PLAIN
                    ).entity(message)
                    .build();
        }
        System.err.println(methodContainter.now() + " >>> Issue: " + i.toString() + "; user: "+ user.toString());    	
        //load the workflow for the target issue from the workflowManager
        JiraWorkflow currentWorkflow = workflowManager.getWorkflow(i);
        /*
        Load all actions available for the current status for this issue.
        Note: even though certain actions/transitions are called the same, internally
        they have different ids. ALWAYS load the actions/actionids based on the status of the
        current issue, not based on global workflow name.
        Example:
        'In Review' -> REOPEN -> 'TODO' where REOPEN has ID = 51
        'In Testing' -> REOPEN -> 'TODO' where REOPEN has ID = 131
        Loading the REOPEN action directly from the workflow will return ID = 51;
        Loading REOPEN from workflow by Issue with status 'In Testing' will return ID = 131
        */
        Collection<ActionDescriptor> availableActions = currentWorkflow.getLinkedStep(i.getStatus()).getActions();
        /*
        Select appropriate status based Upsource action
        Selection goes by name since the transitionaction id varies and is not normally 
        known by admins/programmers;
        */
        String targetTransition= "";
        if(data.getNewState() == 0){
            targetTransition = "REOPEN";
        }else{
            targetTransition = "ACCEPT";
        }
        ActionDescriptor targetAction = null;
        boolean actionAvailable = false;
        for (ActionDescriptor ad : availableActions) {
            System.err.println(methodContainter.now()+" >>> Name: "+ad.getName()+"; Id: "+ad.getId());
            if (ad.getName().toUpperCase().contains(targetTransition)) {
                targetAction = ad;
                actionAvailable = true;
                break;
            }
        }
        if(!actionAvailable){
            String message = this.methodContainter.now()
                    + " >>> Requested transition " + targetTransition
                    + " is unavailable for this Issue " + i.getKey() 
                    + " in the current state " + i.getStatus().getName();
            System.err.println(message);
            return Response.status(TRANSITION_UNAVAILABLE)
                    .type(MediaType.TEXT_PLAIN)
                    .entity(message)
                    .build();
        }
        if (targetAction != null) {
            System.err.println(methodContainter.now()+" >>> Selected TargetAction: "+targetAction.getId());
            //validate and execute transition
            TransitionValidationResult transitionValidation
                    = this.issueService.validateTransition(
                            user,
                            i.getId(),
                            targetAction.getId(),
                            this.issueService.newIssueInputParameters());
            if (transitionValidation.isValid()) {
                this.issueService.transition(user, transitionValidation);
                String message = this.methodContainter.now() 
                        + " >>> closeReview successfully executed" 
                        + targetAction
                        + " - JSON received: " + webhook.toString();
                System.err.println(message);
                return Response.status(200)
                        .type(MediaType.TEXT_PLAIN)
                        .entity(message)
                        .build();
            } else {
                String message = this.methodContainter.now() + " >>> closeReview executed - JSON received: \n" + webhook.toString() + "\n - EXTRACTED ACTOR: " + actor + "\n - ERROR MESSAGES: ";
                for (String error : transitionValidation.getErrorCollection().getErrorMessages()) {
                    message += error + "\n";
                }
                System.err.println(message);
                return Response.status(TRANSITION_VALIDATION_FAILED)
                        .type(MediaType.TEXT_PLAIN)
                        .entity(message)
                        .build();
            }                                   
        }
        String message = this.methodContainter.now() + " >>> changeReviewState(UpsourceWebhookResponse) critically failed.";
        System.err.println(message);
        return Response.status(CRITICAL_ERROR)
                .type(MediaType.TEXT_PLAIN)
                .entity(message)
                .build();
    }
    
    private String getTargetProjectKey(String upsourceProjectId) {
        List<UpsourceConfig> configs = this.loadAllConfigs();
        for (UpsourceConfig uc : configs) {
            if (uc.getUpsourceProjectId().equals(upsourceProjectId)) {
                return uc.getProjectKey();
            }
        }
        return null;
    }
}
