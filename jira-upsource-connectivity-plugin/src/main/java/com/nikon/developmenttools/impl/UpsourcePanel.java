
package com.nikon.developmenttools.impl;

import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel2;
import com.atlassian.jira.plugin.issuetabpanel.GetActionsReply;
import com.atlassian.jira.plugin.issuetabpanel.GetActionsRequest;
import com.atlassian.jira.plugin.issuetabpanel.ShowPanelReply;
import com.atlassian.jira.plugin.issuetabpanel.ShowPanelRequest;
import javax.inject.Inject;

/**
 *
 * @author RVanhuysse
 */
public class UpsourcePanel extends AbstractIssueTabPanel2{

    @Inject
    private UpsourceConnectivityExtentionMethods methodContainer;
    
    public UpsourcePanel(){}

    public UpsourceConnectivityExtentionMethods getMethodContainer() {
        return methodContainer;
    }

    public void setMethodContainer(UpsourceConnectivityExtentionMethods methodContainer) {
        this.methodContainer = methodContainer;
    }
    
    
    
    public ShowPanelReply showPanel(ShowPanelRequest spr) {
        return ShowPanelReply.create(true);
    }

    public GetActionsReply getActions(GetActionsRequest gar) {
        return GetActionsReply.create(new UpsourceAction(super.descriptor(), gar.issue(), methodContainer));
    }
    
    
}
