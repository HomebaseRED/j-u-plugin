package com.nikon.developmenttools.impl;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueAction;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanelModuleDescriptor;
import com.nikon.upsourcedtos.*;
import dto.UpsourceConnectionFailureException;
import dto.UpsourceConnectivityException;
import dto.UpsourceUnsetConfigException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author RVanhuysse
 */
public class UpsourceAction extends AbstractIssueAction {

    private Issue issue;
    private UpsourceConnectivityExtentionMethods methodContainer;

    public UpsourceAction(IssueTabPanelModuleDescriptor descriptor, Issue issue,
            UpsourceConnectivityExtentionMethods methodContainer) {
        super(descriptor);
        this.issue = issue;
        this.methodContainer = methodContainer;
    }

    public Issue getIssue() {
        return issue;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    public UpsourceConnectivityExtentionMethods getMethodContainer() {
        return methodContainer;
    }

    public void setMethodContainer(UpsourceConnectivityExtentionMethods methodContainer) {
        this.methodContainer = methodContainer;
    }

    @Override
    public Date getTimePerformed() {
        Timestamp t = this.issue.getCreated();
        if (t == null) {
            throw new UnsupportedOperationException("No timestamp found in issue " + this.issue.getSummary());
        } else {
            return new Date(t.getTime());
        }
    }

    @Override
    protected void populateVelocityParams(Map map) {
        map.put("action", this);
        List<String> exceptionMessages = new ArrayList<String>();
        try {
            ReviewListDTO r = 
                    (ReviewListDTO) 
                    this.methodContainer.getReviewsForIssueWithComments(this.issue)
                                .getResult();
            for(ReviewDescriptorDTO descriptor : r.getReviews()){
                ReviewSummaryChangesResponseDTO diff = 
                        (ReviewSummaryChangesResponseDTO) 
                        this.methodContainer.getDiffForReview(descriptor, this.issue.getProjectObject())
                                    .getResult();
                if(diff != null && diff.getFileDiffSummary() != null && !diff.getFileDiffSummary().isEmpty()){
                    descriptor.getAdditionalProperties().put("diff", diff.getFileDiffSummary());
                }else{
                    String message = "This review has no changed files for this VCS. Possibly changes were made in a related project?";
                    descriptor.getAdditionalProperties().put("NoDiffMessage", message);
                }
            }
            map.put("reviewsDTO", r);       
        } catch (UpsourceConnectionFailureException ucfe) {
            System.err.println(this.methodContainer.now()+" - UPSOURCE CONNECTION EXCEPTION: " + ucfe.getMessage()+ " STACKTRACE: ");
            ucfe.printStackTrace();
            String message = "";
            if (ucfe.getCause() != null && ucfe.getCause().getCause() instanceof java.net.UnknownHostException) {
                message = "Error resolving host, please check the Upsource Connectivity Settings for this project and your network settings.";
                exceptionMessages.add(message);
            } else {
                message = ucfe.toString();// + " \nSTACKTRACE: " + org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(ucfe);
                exceptionMessages.add(message);
            }
        } catch (UpsourceUnsetConfigException uuce) {
            System.err.println(this.methodContainer.now()+" - UPSOURCE CONFIGRATION EXCEPTION: " + uuce.getMessage()+ " STACKTRACE: ");
            uuce.printStackTrace();
            String message = uuce.getMessage();//+ " \nSTACKTRACE: " + org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(uuce);
            exceptionMessages.add(message);
        } catch (UpsourceConnectivityException uce) {
            System.err.println(this.methodContainer.now()+" - UPSOURCE EXCEPTION: " + uce.getMessage()+ " STACKTRACE: ");
            uce.printStackTrace();
            String message = "";
            if (uce.getCause() != null) {
                message = uce.getCause().toString() + " \nSTACKTRACE: " + org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(uce);
                exceptionMessages.add(message);
            } else {
                message = uce.toString() + " \nSTACKTRACE: " + org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(uce);
                exceptionMessages.add(message);
            }
        } catch (Exception ex) {
            exceptionMessages.add(ex.toString()+"\nStackTrace: "+ org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(ex));
            System.err.println(this.methodContainer.now()+" - EXCEPTION: " + ex.toString()
                    + " \nSTACKTRACE: "+org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(ex));
        }
        map.put("exceptionList", exceptionMessages);
    }
    
    
}
