/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.developmenttools.impl;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import dto.UpsourceConfig;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author RVanhuysse
 */
@Scanned
public class AdminServlet extends HttpServlet{
      
    @ComponentImport
    private final ProjectManager projectManager;
    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final LoginUriProvider loginUriProvider;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
    @ComponentImport
    private final TransactionTemplate transactionTemplate;
    @ComponentImport
    private final TemplateRenderer renderer;
    
    @Inject
    public AdminServlet(ProjectManager projectManager, 
            UserManager userManager, 
            LoginUriProvider loginUriProvider,
            PluginSettingsFactory pluginSettingsFactory,
            TransactionTemplate transactionTemplate,
            TemplateRenderer renderer){
        this.projectManager = projectManager;
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;
        this.renderer = renderer;
    }
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException{
        UserKey userKey = userManager.getRemoteUserKey();
        if(userKey == null || !(userManager.isAdmin(userKey))){
            redirectToLogin(request, response);
            return;
        }
        Map<String, Object> contextMap = new HashMap<String, Object>();
        List<Project> projects = projectManager.getProjectObjects();
        contextMap.put("projectList", projects);
        List<UpsourceConfig> projectConfigs = new ArrayList<UpsourceConfig>();
        for(final Project p : projects){
            Object o = transactionTemplate.execute(
                new TransactionCallback() {
                    public Object doInTransaction() {
                        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(p.getKey()+"_UCPSettings");
                        UpsourceConfig config = new UpsourceConfig();
                        config.setHost((String) settings.get(UpsourceConfig.class.getName() + ".host"));
                        config.setUsername((String) settings.get(UpsourceConfig.class.getName() + ".username"));
                        config.setPassword((String) settings.get(UpsourceConfig.class.getName() + ".password"));
                        config.setProjectKey((String) settings.get(UpsourceConfig.class.getName() + ".projectKey"));
                        config.setUpsourceProjectId((String) settings.get(UpsourceConfig.class.getName() + ".upsourceProjectId"));
                        return config;
                    }
                }
            );
            projectConfigs.add((UpsourceConfig) o);
        }
        contextMap.put("projectConfigs", projectConfigs);
        response.setContentType("text/html;charset=utf-8");
        renderer.render("templates/admin.vsl", contextMap, response.getWriter());
    }
    
    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException{
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }
    
    private URI getUri(HttpServletRequest request){
        StringBuffer builder = request.getRequestURL();
        if(request.getQueryString() != null){
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
    
}
