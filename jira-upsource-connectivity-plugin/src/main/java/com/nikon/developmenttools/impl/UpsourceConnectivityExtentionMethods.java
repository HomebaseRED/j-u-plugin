/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.developmenttools.impl;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.nikon.upsourcedtos.ResultDTO;
import com.nikon.upsourcedtos.ReviewDescriptorDTO;
import com.nikon.upsourcedtos.ReviewIdDTO;
import com.nikon.upsourcedtos.ReviewListDTO;
import dto.UpsourceConfig;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author RVanhuysse
 */
public interface UpsourceConnectivityExtentionMethods {

    UpsourceConfig loadConfig(String projectKey);
    
    Date now();
    
    ResultDTO getProjectInfo(Project p);
    Map<String, Object> createReview(String issueKey, String projectKey, String user, String authHeader);
    ResultDTO getReviews(Project p, String query);
    ResultDTO getReviewsForIssue(Issue i);
    ResultDTO getReviewsForIssueWithComments(Issue i);
    ResultDTO getReviewDetails(ReviewIdDTO id, String projectKey);
    ResultDTO getFeed(Project p);
    ResultDTO getCommentsForReviews(ReviewListDTO reviews, Project p);
    ResultDTO getDiffForReview(ReviewDescriptorDTO review, Project p);
}
