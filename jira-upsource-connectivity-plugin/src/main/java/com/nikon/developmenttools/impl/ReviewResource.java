/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.developmenttools.impl;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.core.util.Base64;
import dto.UpsourceApiInternalException;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author RVanhuysse
 */
@Path("/reviewStateChange")
@Scanned
public class ReviewResource {
    
    private UpsourceConnectivityExtentionMethodsImp methodContainer;
    
    @Inject
    public ReviewResource(UpsourceConnectivityExtentionMethodsImp methodContainer){
        this.methodContainer = methodContainer;
    }
    
    @POST
    @Path("state")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response accept(String data, @Context HttpServletRequest request){
        JsonParser p = new JsonParser();
        JsonObject o = p.parse(data).getAsJsonObject();
        String user = o.get("user").getAsString();
        String pass = o.get("password").getAsString();
        String auth = this.methodContainer.generateAuthenticationHeader(user, pass);
        String jiraProjectKey = o.get("jiraProjectId").getAsString();
        String upsourceProjectId = o.get("upsourceProjectId").getAsString();
        String reviewId = o.get("reviewId").getAsString();
        int state = o.get("state").getAsInt();
        try{
            this.methodContainer.changeReviewState(jiraProjectKey, upsourceProjectId, reviewId, auth, state);
        }catch(UpsourceApiInternalException uaie){
            return Response.status(401).entity(uaie.getMessage()).build();
        }
        return Response.noContent().build();
    }
    
    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createReview(String data, @Context HttpServletRequest request){
        JsonParser p = new JsonParser();
        JsonObject o = p.parse(data).getAsJsonObject();
        String user = o.get("user").getAsString();
        String pass = o.get("password").getAsString();
        String auth = "Basic " 
                + new String(
                        Base64.encode(
                                user + ":" + pass
                        ));
        String jiraProjectKey = o.get("jiraProjectKey").getAsString();
        String jiraIssueKey = o.get("jiraIssueKey").getAsString();
        Map<String, Object> response = new HashMap<String, Object>();
        try{
            response = this.methodContainer.createReview(jiraIssueKey, jiraProjectKey, user, auth);
        }catch(UpsourceApiInternalException uaie){
            return Response.status(500)
                    .entity(this.methodContainer.now()+
                            " >>> " +
                            uaie.getMessage() +
                            "\n STACKTRACE: "+
                            org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(uaie)).build();
        }catch(Exception ex){
            return Response.status(500).entity(this.methodContainer.now()+ " >>> "
                    + ex.getMessage() +"\n STACKTRACE: "+
                    org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(ex)).build();
            
        }
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }
    
    @POST
    @Path("displayDiff")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response displayDiff(String data, @Context HttpServletRequest request){ 
        JsonParser p = new JsonParser();
        JsonObject o = p.parse(data).getAsJsonObject();
        String jiraProjectKey = o.get("projectKey").getAsString();
        String reviewId = o.get("reviewId").getAsString();
        String upsourceProjectId = o.get("projectId").getAsString();
        String revisionId = o.get("revisionId").getAsString();
        String fileName = o.get("fileName").getAsString();
        Map<String, Object> response = null;
        try{
            response = this.methodContainer.displayDiff(
                    jiraProjectKey, reviewId, upsourceProjectId,
                    revisionId, fileName);
        }catch(Exception ex){
            return Response.status(500).entity(ex.toString()+" >>> STACKTRACE: \n"+org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(ex)).build();            
        }
        return Response.ok(response, MediaType.APPLICATION_JSON).build();
    }
}
