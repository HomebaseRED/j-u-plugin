package com.nikon.developmenttools.impl;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nikon.upsourcedtos.*;
import com.nikon.upsourcedtos.io.UpsourceEnumValueConverter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.core.util.Base64;
import dto.UpsourceApiInternalException;
import dto.UpsourceConfig;
import dto.UpsourceConnectionFailureException;
import dto.UpsourceConnectivityException;
import dto.UpsourceDeserializationException;
import dto.UpsourceUnsetConfigException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.MediaType;
import static org.codehaus.jackson.map.DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author RVanhuysse
 */
@ExportAsService({UpsourceConnectivityExtentionMethods.class})
@Named("UpsourceConnectivityExtentionMethods")
public class UpsourceConnectivityExtentionMethodsImp implements UpsourceConnectivityExtentionMethods {

    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
    @ComponentImport
    private final TransactionTemplate transactionTemplate;

    private Client client;

    private Calendar executionTime;

    /**
     * Creates a UpsourceConnectivityExtentionMethodsImp object with framework
     * injected PluginSettingsFactory and TransactionTemplate. Additionally,
     * sets up webclient object and executionTime variable.
     *
     * @param pluginSettingsFactory a PluginSettingsFactory object injected by
     * the framework; responsible for saving and loading settings from database
     * @param transactionTemplate a TransactionTemplate object injected by the
     * framework; responsible for abstracting the locking and accessing the
     * database with pluginSettings
     * @throws UpsourceConnectivityException containing throwable cause when
     * setting up webclient fails.
     */
    @Inject
    public UpsourceConnectivityExtentionMethodsImp(
            PluginSettingsFactory pluginSettingsFactory,
            TransactionTemplate transactionTemplate) {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;
        this.initiateWebClient();
        this.executionTime = new GregorianCalendar();
        executionTime.setTimeInMillis(System.currentTimeMillis());
    }

    /**
     * Utility method to return current system time in human readable format.
     *
     * @return a java Date object
     */
    @Override
    public Date now() {
        this.executionTime.setTimeInMillis(System.currentTimeMillis());
        return executionTime.getTime();
    }

    /**
     * Returns an UpsourceConfig object from the settings database, based on the
     * provided String argument representing the projectKey for the jira project
     * the return object relates to.
     *
     * @throws UpsourceConnectivityException when database returns a null object
     * @throws UpsourceUnsetConfigException when return object has an empty or
     * null host parameter
     * @param projectKey a String representing the jira project for which the
     * UpsourceConfig object needs to be loaded
     * @return an UpsourceConfig object describing the connection to between the
     * provided jira project and its respective upsource project
     */
    @Override
    public UpsourceConfig loadConfig(final String projectKey) {
        UpsourceConfig uc
                = (UpsourceConfig) transactionTemplate.execute(
                        new TransactionCallback() {
                    public Object doInTransaction() {
                        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(projectKey + "_UCPSettings");
                        UpsourceConfig config = new UpsourceConfig();
                        config.setHost((String) settings.get(UpsourceConfig.class.getName() + ".host"));
                        config.setUsername((String) settings.get(UpsourceConfig.class.getName() + ".username"));
                        config.setPassword((String) settings.get(UpsourceConfig.class.getName() + ".password"));
                        config.setProjectKey((String) settings.get(UpsourceConfig.class.getName() + ".projectKey"));
                        config.setUpsourceProjectId((String) settings.get(UpsourceConfig.class.getName() + ".upsourceProjectId"));
                        return config;
                    }
                }
                );
        if (uc == null) {
            throw new UpsourceConnectivityException("Config null while loading.");
        } else if (uc.getHost() == null || uc.getHost().isEmpty()) {
            throw new UpsourceUnsetConfigException("No Upsource connection has been configured for this project. Please contact your system administrator.");
        } else if (uc.getHost() == null || uc.getHost().isEmpty()) {
            throw new UpsourceUnsetConfigException("No Upsource connection has been configured for this project. Please contact your system administrator.");
        }
        return uc;
    }

    /**
     * Method for initializing the webclient that's shared between this classes
     * restcall methods. Jersey documentation suggested that webclients be
     * shared as much as possible, as setting up a new client is resource
     * intensive.
     *
     * @throws UpsourceConnectivityException with throwable cause when setup
     * fails.
     */
    private void initiateWebClient() {
        try {
            ClientConfig cc = new DefaultClientConfig();
            //enable Json responses
            cc.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
            //get client
            this.client = Client.create(cc);
            this.client.setFollowRedirects(Boolean.TRUE);
        } catch (Exception ex) {
            throw new UpsourceConnectivityException("error creating client", ex);
        }
    }

    /**
     * Method for parsing out the wrapping root json element of a response.
     * Functionality could, and should, eventually be replaced by custom
     * deserializers.
     *
     * @throws UpsourceConnectivityException when input is null
     * @throws UpsourceApiInternalException when input contains the error
     * property instead of expected values
     * @throws UpsourceDeserialization error when expected values are null
     * @param input a String representing the json data to be parsed
     * @returns a String representing json data without the wrapping result
     * property
     */
    private String unrootJson(String input) {
        if (input == null) {
            throw new UpsourceConnectivityException("Json root string is null");
        }
        JsonParser parser = new JsonParser();
        JsonObject rootNode = parser.parse(input).getAsJsonObject();
        for (Entry<String, JsonElement> e : rootNode.entrySet()) {
            if (e.getKey().equals("error")) {
                JsonObject error = e.getValue().getAsJsonObject();
                String message = "Code: " + error.get("code").getAsString()
                        + " - Message: " + error.get("message").getAsString();
                throw new UpsourceApiInternalException(message);
            }
        }
        String result = rootNode.get("result").toString();
        if (result == null || result.isEmpty()) {
            throw new UpsourceDeserializationException("Empty root Node");
        }
        return result;
    }

    public String generateAuthenticationHeader(String userName, String password){
        String response = "Basic "
                + new String(Base64.encode(userName+":"+password));
        return response;
    } 

    public ResultDTO callUpsourceRpc(UpsourceConfig config, String rpcMethod, Class dataType, Map<String, Object> arguments) {
        //prepare target for rest call
        String target = config.getHost() + "~rpc/"+ rpcMethod;
        WebResource r = this.client.resource(target);
        //prepare auth header
        String auth = config.getAuthHeader();
        //make Call
        ClientResponse response = null;
        try {
            response = r.accept(MediaType.APPLICATION_JSON)
                    .type(MediaType.APPLICATION_JSON)
                    .header("Authorization", auth)
                    .post(ClientResponse.class, arguments);
        } catch (Exception ex) {
            throw new UpsourceConnectivityException("error making call", ex);
        }
        int responseCode = response.getStatus();
        if (responseCode == 401 || responseCode == 403) {
            String message
                    = "Error: You are not authorized to complete this action";
            System.err.println(message);
            throw new UpsourceApiInternalException(message);
        } else if (responseCode != 200) {
            String message
                    = "A network error occurred: " + response.getStatus()
                    + " - " + response.getStatusInfo().getReasonPhrase();
            throw new UpsourceConnectionFailureException(message);
        } else {
            //parse returned json
            //setup
            ResultDTO result = null;
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
            //get root json string
            String responseContents = response.getEntity(String.class);
            response.close();
            //unwrap from root elemen
            String unrootedJson = unrootJson(responseContents);
            //map json to pojo
			//need command to convert into target UpsourceDTO
            UpsourceDTO inbetween;
            try {
                inbetween =(UpsourceDTO) objectMapper.readValue(unrootedJson, dataType);
            } catch (Exception ex) {
                // !!! TODO CONSIDER ERROR HANDLING
                throw new UpsourceDeserializationException(ex);
            }
            result = new ResultDTO();
            result.setResult(inbetween);
            return result;
        }
    }
    
    /**
     * Method for looking up the UpsourceProjectId connected to the jira project
     * specified in the parameters. This method is responsible for the
     * automagical matching of jira and upsource projects when configuring a
     * connection. In essence, it is a simple search on projects by the jira
     * name on the upsource server.
     *
     * @throws UpsourceConnectivityException when an error occurs during the
     * call to the upsource server. Contains a throwable cause.
     * @throws UpsourceConnectionFailureException when returned httpresponse has
     * a statuscode other than 200 OK
     * @throws UpsourceDeserializationException when deserialization of an
     * unrooted response fails
     * @throws UpsourceConnectivityException when no projectIds are found
     * @see String unrootJson(String);
     * @param config the UpsourceConfig object representing the connection the
     * looked up upsourceId needs to be associated with
     * @param jiraProjectName a String acting as search query for finding the
     * UpsourceId. Represents the human readable name of the jira project
     * @return the best matching search result
     */
    public String getUpsourceProjectId(UpsourceConfig config, String jiraProjectName) {
        String result = "";
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("pattern", jiraProjectName);
        postBody.put("limit", 64);
        ShortProjectInfoListDTO container = 
                (ShortProjectInfoListDTO)callUpsourceRpc(config, "findProjects",
                        ShortProjectInfoListDTO.class, postBody).getResult();
        if (container.getProject() == null || container.getProject().isEmpty()) {
            throw new UpsourceConnectivityException("no Project under aligining to that name");
        }
        result = container.getProject().get(0).getProjectId();
        return result;
    }

    /**
     * Method for looking up details on a upsource project connected to the
     * given jira project.
     *
     * @throws UpsourceConnectivityException when loadConfig throws an exception
     * @throws UpsourceConnectivityException when an error occurs during the
     * call to the upsource server. Contains a throwable cause.
     * @throws UpsourceConnectionFailureException when returned httpresponse has
     * a statuscode other than 200 OK
     * @throws UpsourceDeserializationException when deserialization of an
     * unrooted response fails
     * @see String unrootJson(String); UpsourceConfig loadConfig(String)
     * @param p a Project object to define the targeted UpsourceConfig. Config
     * is loaded by p.Key
     * @return the best matching search result
     */
    @Override
    public ResultDTO getProjectInfo(Project p) {
        //prepare target for rest call
        UpsourceConfig config = loadConfig(p.getKey());
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("projectId", config.getUpsourceProjectId());
        return callUpsourceRpc(config, "getProjectInfo", ProjectInfoDTO.class, postBody);
    }

    /**
     * Method for looking up Upsource revisions that comply with the query
     * argument. Used to look up revisions with a certain issueKey in the
     * message that are not yet part of a review. Used during the creation of
     * new reviews.
     *
     * @throws UpsourceConnectivityException when loadConfig throws an exception
     * @throws UpsourceConnectivityException when an error occurs during the
     * call to the upsource server. Contains a throwable cause.
     * @throws UpsourceConnectionFailureException when returned httpresponse has
     * a statuscode other than 200 OK
     * @throws UpsourceDeserializationException when deserialization of an
     * unrooted response fails
     * @see String unrootJson(String); UpsourceConfig loadConfig(String);
     * @param projectKey a String representing the jira project key by which to
     * load an UpsourceConfig
     * @param query a String representing the search criteria
     * @return a ResultDTO containing RevisionDescriptorListDTO with all
     * revisions matching the search query
     */
    public ResultDTO getRevisions(String projectKey, String query) {
        UpsourceConfig config = loadConfig(projectKey);
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("projectId", config.getUpsourceProjectId());
        postBody.put("limit", config.getLookupReach());
        postBody.put("query", query);
        postBody.put("requestGraph", false);
        ResultDTO result = callUpsourceRpc(config, "getRevisionsListFiltered", RevisionDescriptorListDTO.class, postBody);
        return result;
    }

    public ResultDTO getRevisionReviewInfo(Project p, List<String> revisionIds){
        ResultDTO result = null;
        UpsourceConfig config = loadConfig(p.getKey());
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("projectId", config.getUpsourceProjectId());
        postBody.put("revisionId", revisionIds);
        result = callUpsourceRpc(config, "getRevisionReviewInfo", RevisionReviewInfoListDTO.class, postBody);
        return result;
    }
    
    /**
     * Method for creating a new review on the upsource server, containing all
     * revisions with the given issueKey that are not already part of another
     * review.
     *
     * @throws UpsourceConnectivityException when loadConfig throws an exception
     * @throws UpsourceConnectivityException when an error occurs during the
     * call to the upsource server. Contains a throwable cause.
     * @throws UpsourceConnectionFailureException when returned httpresponse has
     * a statuscode other than 200 OK
     * @throws UpsourceDeserializationException when deserialization of an
     * unrooted response fails
     * @see String unrootJson(String); UpsourceConfig loadConfig(String);
     * @param user a String representing the user creating the review
     * @param issueKey a String representing the issue to which all revisions in
     * the review need to relate.
     * @param projectKey a String representing the jira project key by which to
     * load an UpsourceConfig
     * @param authHeader a String representing the specific credentials of the
     * user creating the review
     * @return a String representing a success message in json format
     */
    @Override
    public Map<String, Object> createReview(String issueKey, String projectKey, String user, String authHeader) {
        Map<String, Object> returnObject = new HashMap<String, Object>();
        System.err.println("Extentions.createReview("
                + issueKey + ", "
                + projectKey + ", "
                + authHeader + ")");
        String query = "(" + issueKey + ") and not #review";
        //find all revisions that mention the query, eg contain issuekey and are not part of a review
        RevisionDescriptorListDTO revisions = (RevisionDescriptorListDTO) getRevisions(projectKey, query).getResult();
        if (revisions.getRevisions() == null || revisions.getRevisions().isEmpty()) {
            returnObject.put("status", "Could not resolve revisions.");
            return returnObject;
        }
        ArrayList<String> revisionArgs = new ArrayList<String>();
        for (RevisionInfoDTO r : revisions.getRevisions()) {
            revisionArgs.add(r.getRevisionId());
        }
        UpsourceConfig config = loadConfig(projectKey);
        String target = config.getHost() + "~rpc/createReview";
        WebResource r = this.client.resource(target);
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("projectId", config.getUpsourceProjectId());
        postBody.put("revisions", revisionArgs);
        ClientResponse response = null;
        try {
            response = r.accept(MediaType.APPLICATION_JSON)
                    .type(MediaType.APPLICATION_JSON)
                    .header("Authorization", authHeader)
                    .post(ClientResponse.class, postBody);
        } catch (Exception ex) {
            throw new UpsourceConnectivityException("error making call", ex);
        }
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        String responseContents = response.getEntity(String.class);
        response.close();
        String unrootedJson = unrootJson(responseContents);
        if (response.getStatus() != 200) {
            String message
                    = "A network error occurred: " + response.getStatus()
                    + " - " + response.getStatusInfo().getReasonPhrase();
            ErrorDTO er = null;
            try {
                er = mapper.readValue(unrootedJson, ErrorDTO.class);
            } catch (Exception ex) {
                // !!! TODO CONSIDER ERROR HANDLING
                throw new UpsourceDeserializationException(message, ex);
            }
            message += "Upsrc error message:\n CODE: " + er.getCode() + "\n MESSAGE: " + er.getMessage();
            throw new UpsourceConnectionFailureException(message);
        } else {
            ReviewDescriptorDTO rev = null;
            returnObject.put("status", "revision created");
            try {
                rev = mapper.readValue(unrootedJson, ReviewDescriptorDTO.class);
            } catch (Exception ex) {
                // !!! TODO CONSIDER ERROR HANDLING
                return returnObject;
            }
            boolean addSelf = false;
            FullUserInfoDTO upsourceUser = null;
            String[] userId = {rev.getParticipants().get(0).getUserId()};
            try {
                upsourceUser = (FullUserInfoDTO) ((UserInfoResponseDTO) getUserInfo(Arrays.asList(userId), projectKey).getResult())
                        .getInfos().get(0);
            } catch (Exception ex) {
            }
            if (upsourceUser != null) {
                addSelf = addUserToReview(projectKey, rev.getReviewId(),
                        new ParticipantInReviewDTO(upsourceUser.getUserId(), 2, 1),
                         authHeader);
            }
            returnObject.put("addedSelf", addSelf);
            return returnObject;
        }
    }

    /**
     * Method for looking up Upsource reviews for a given jira project.
     *
     * @throws UpsourceConnectivityException when loadConfig throws an exception
     * @throws UpsourceConnectivityException when an error occurs during the
     * call to the upsource server. Contains a throwable cause.
     * @throws UpsourceConnectionFailureException when returned httpresponse has
     * a statuscode other than 200 OK
     * @throws UpsourceDeserializationException when deserialization of an
     * unrooted response fails
     * @see String unrootJson(String); UpsourceConfig loadConfig(String); String
     * generateReviewLink(UpsourceIdDTO, UpsourceConfig)
     * @param p a Project object representing the jira project by which to load
     * an UpsourceConfig
     * @param query String object that can be used for filtering the retrieved 
     * reviews on the upsource side of the connection. Pass null of empty string
     * to execute without searchquery.
     * @return a ResultDTO containing up to 32 reviews for this jira/upsource
     * project in a ReviewListDTO
     */
    @Override
    public ResultDTO getReviews(Project p, String query) {
        //load the correct UpsourceConfig for this project by the project key provided in p
        UpsourceConfig config = loadConfig(p.getKey());
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("limit", config.getLookupReach());
        postBody.put("projectId", config.getUpsourceProjectId());
        if(query != null && !query.isEmpty()){
            postBody.put("query", query);
        }
        ResultDTO result = callUpsourceRpc(config, "getReviews", ReviewListDTO.class, postBody);
        ReviewListDTO reviewList = (ReviewListDTO)result.getResult();
        if (reviewList.getTotalCount() > 0) {
            for (ReviewDescriptorDTO rev : reviewList.getReviews()) {
                String link = generateReviewLink(rev.getReviewId(), config);
                rev.getAdditionalProperties().put("link", link);
            }
        }
        result.setResult(reviewList);
        return result;
    }

    /* Note: First build tried to recognise review-issue connection by scanning the review title of every retrieved review for the issue key.
     This design was made under the idea that it was convention to mention the issue key in the commit message, first line. This is however not
     fully guaranteed. Thusly this method was refactored to filter revision for the issue key and then check whether this revision belongs to a review.*/
    /**
     * Method for looking up Upsource reviews for a certain issue. 
     * @see String unrootJson(String); UpsourceConfig loadConfig(String);
     * ResultDTO getReviews(Project)
     * @param i an Issue object, nescessary to derive the target project and to
     * filter for
     * @return a ResultDTO containing a ReviewListDTO with all reviews for the
     * given issue
     */
    @Override
    public ResultDTO getReviewsForIssue(Issue i) {
        Project p = i.getProjectObject();
        ResultDTO response = new ResultDTO();
        response.setResult(new ReviewListDTO(new ArrayList<ReviewDescriptorDTO>(), false, 0));
        RevisionDescriptorListDTO relevantRevisions = (RevisionDescriptorListDTO) getRevisions(p.getKey(), i.getKey()).getResult();
        List<String> revisionIds = new ArrayList<String>();
        //get revisions that mention the issuekey
        if(relevantRevisions.getRevisions()==null || relevantRevisions.getRevisions().isEmpty()){
            return response;
        }
        for(RevisionInfoDTO revision : relevantRevisions.getRevisions()){
            revisionIds.add(revision.getRevisionId());
        }
        RevisionReviewInfoListDTO relevantReviewShorts = (RevisionReviewInfoListDTO) getRevisionReviewInfo(p, revisionIds).getResult();
        if(relevantReviewShorts == null || relevantReviewShorts.getReviewInfo()==null || relevantReviewShorts.getReviewInfo().isEmpty()){
            return response;
        }
        Set<String> reviewIds = new HashSet<String>();
        for(RevisionReviewInfoDTO wrapper : relevantReviewShorts.getReviewInfo()){
            if(wrapper.getReviewInfo() != null){
                reviewIds.add(wrapper.getReviewInfo().getReviewId().getReviewId());
            }
        }
        if(reviewIds.isEmpty()){
           return response; 
        }
            
        //form querystring for review lookup
        String query = "";
        for(String s : reviewIds){
            query = query + " or " + s;
        }
        //cut of leading ' or '
        query = query.substring(4);
        //get revisions
        response = getReviews(p,query);
        return response;
    }

    /**
     * Method for looking up Upsource reviews for a certain issue with their
     * comments. Uses getReviews to get all reviews for project, then filters
     * them on the given issue, then loads comments per review.
     *
     * @see String unrootJson(String); UpsourceConfig loadConfig(String);
     * ResultDTO getReviewsForIssue(Issue)
     * @param i an Issue object, nescessary to derive the target project and to
     * filter for
     * @return a ResultDTO containing a ReviewListDTO with all reviews and their
     * comments for the given issue
     */
    @Override
    public ResultDTO getReviewsForIssueWithComments(Issue i) {
        ResultDTO response = new ResultDTO();
        ReviewListDTO revs = (ReviewListDTO) getReviewsForIssue(i).getResult();
        response.setResult(revs);
        if (revs.getTotalCount() > 0) {
            ReviewListDTO revsWcomms = (ReviewListDTO) getCommentsForReviews(revs, i.getProjectObject()).getResult();
            response.setResult(revsWcomms);
            revsWcomms = (ReviewListDTO) getParticipantsForReviews(revsWcomms, i.getProjectObject().getKey()).getResult();
            response.setResult(revsWcomms);
        }
        return response;
    }

    /**
     * Method for looking up details on an upsource review. Currently has no
     * usages in the project
     *
     * @see String unrootJson(String); UpsourceConfig loadConfig(String)
     * @param id a ReviewIdDTO object to identify the specific review to look
     * for
     * @param projectKey a String identifying the jira project to look up the
     * correct upsource config
     * @return a ResultDTO containing a ReviewDescriptorDTO
     */
    @Override
    public ResultDTO getReviewDetails(ReviewIdDTO id, String projectKey) {
        UpsourceConfig config = loadConfig(projectKey);
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("projectId", id.getProjectId());
        postBody.put("reviewId", id.getReviewId());
        ResultDTO result = callUpsourceRpc(config, "getReviewDetails", ReviewDescriptorDTO.class, postBody);
        return result;
    }

    /**
     * Method generates direct link to the correct upsource page based on host,
     * upsource project and reviewId.
     *
     * @param id ReviewIdDTO containing the upsource project id and the review
     * id
     * @param config UpsourceConfig to load the upsource host url
     * @return a String representing the direct link url.
     */
    public String generateReviewLink(ReviewIdDTO id, UpsourceConfig config) {
        String link = config.getHost() + id.getProjectId() + "/review/" + id.getReviewId();
        return link;
    }

    /**
     * Method to add a user to a review.
     *
     * @param projectKey String to identify the upsourceConfig by jira project
     * key
     * @param review ReviewDTO object to identify the target review
     * @param part ParticipantInReviewDTO object to describe the upsource user
     * to be added
     * @param authHeader String to authorize the add action with
     * @return boolean describing success or failure
     */
    public boolean addUserToReview(String projectKey, ReviewIdDTO review, ParticipantInReviewDTO part, String authHeader) {
        UpsourceConfig config = loadConfig(projectKey);
        String target = config.getHost() + "~rpc/addParticipantToReview";
        WebResource r = this.client.resource(target);
        String auth = authHeader;
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("reviewId", review);
        postBody.put("participant", part);
        ClientResponse response = null;
        try {
            response = r.accept(MediaType.APPLICATION_JSON)
                    .type(MediaType.APPLICATION_JSON)
                    .header("Authorization", auth)
                    .post(ClientResponse.class, postBody);
        } catch (com.sun.jersey.api.client.ClientHandlerException ex) {
            return false;
        }
        if (response.getStatus() != 200) {
            return false;
        }
        return true;
    }

    /**
     * Method for looking up Upsource Feed for a project. Used in loading of the
     * comments for a review.
     *
     * @throws UpsourceConnectivityException when loadConfig throws an exception
     * @throws UpsourceConnectivityException when an error occurs during the
     * call to the upsource server. Contains a throwable cause.
     * @throws UpsourceConnectionFailureException when returned httpresponse has
     * a statuscode other than 200 OK
     * @throws UpsourceDeserializationException when deserialization of an
     * unrooted response fails
     * @see String unrootJson(String); UpsourceConfig loadConfig(String);
     * @param p a project object to look up the correct UpsourceConfig
     * @return a ResultDTO containing a FeedDTO for the requested project
     */
    @Override
    public ResultDTO getFeed(Project p) {
        UpsourceConfig config = loadConfig(p.getKey());
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("limit", config.getLookupReach());
        postBody.put("type", 1);
        postBody.put("projectId", config.getUpsourceProjectId());
        ResultDTO result = callUpsourceRpc(config, "getFeed", FeedDTO.class, postBody);
        return result;
    }

    /**
     * Method for looking up Upsource Feed for a specific review in the given
     * project. Used in loading of the comments for a review.
     *
     * @throws UpsourceConnectivityException when loadConfig throws an exception
     * @throws UpsourceConnectivityException when an error occurs during the
     * call to the upsource server. Contains a throwable cause.
     * @throws UpsourceConnectionFailureException when returned httpresponse has
     * a statuscode other than 200 OK
     * @throws UpsourceDeserializationException when deserialization of an
     * unrooted response fails
     * @see String unrootJson(String); UpsourceConfig loadConfig(String);
     * @param p a Project object to look up the correct UpsourceConfig
     * @param rev a ReviewDescriptorDTO specifying the target review.
     * @return a ResultDTO containing a FeedDTO for the requested project
     * filtered for the specified review
     */
    public ResultDTO getFeedForReview(Project p, ReviewDescriptorDTO rev) {
        UpsourceConfig config = loadConfig(p.getKey());        
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("limit", config.getLookupReach());
        postBody.put("type", 2);
        postBody.put("projectId", config.getUpsourceProjectId());
        postBody.put("reviewId", rev.getReviewId().getReviewId());
        ResultDTO result = callUpsourceRpc(config, "getFeed", FeedDTO.class, postBody);
        return result;
    }

    /**
     * Method for looking up comments for a list of reviews.
     *
     * @see String unrootJson(String); UpsourceConfig loadConfig(String);
     * ResultDTO getFeedForReview(Project, ReviewDescriptor); FeedDTO
     * filterForComments(FeedDTO, String)
     * @param reviews ReviewListDTO containing all the reviews to load the
     * comments for.
     * @param p a project object to look up the correct UpsourceConfig.
     * @return a ResultDTO containing a ReviewListDTO with comments for each
     * review, if any available
     */
    @Override
    public ResultDTO getCommentsForReviews(ReviewListDTO reviews, Project p) {
        for (ReviewDescriptorDTO review : reviews.getReviews()) {
            FeedDTO items = (FeedDTO) getFeedForReview(p, review).getResult();
            FeedDTO filtered = filterForComments(items, p.getKey());
            if (filtered.getFeedItems() == null /*|| filtered.getFeedItems().isEmpty()*/) {
                throw new UpsourceConnectivityException("Filtered is empty on filter");
            }
            review.getAdditionalProperties().put("commentContainers", filtered);
        }
        ResultDTO response = new ResultDTO();
        response.setResult(reviews);
        return response;
    }

    /**
     * Method for filtering a FeedDTO for specifically comments.
     *
     * @see DiscussionInFeedDTO convertDatesForComments(DiscussionInFeedDTO,
     * String)
     * @param unfilteredList the FeedDTO to be filtered.
     * @param projectKey String representing the jira project, necessary for
     * converting some metadata into human readable form
     * @return a FeedDTO only containing comment feedobjects
     */
    // TODO consider guaranteeing order by ordering on date
    private FeedDTO filterForComments(FeedDTO unfilteredList, String projectKey) {
        List<FeedItemDTO> commentItems = new ArrayList<FeedItemDTO>();
        for (FeedItemDTO item : unfilteredList.getFeedItems()) {
            Object d = item.getDiscussion();
            if (d != null) {
                DiscussionInFeedDTO dto = (DiscussionInFeedDTO) d;
                item.setDiscussion(convertDatesForComments(dto, projectKey));
                commentItems.add(item);
            }
        }
        FeedDTO response = new FeedDTO();
        response.setFeedItems(commentItems);
        return response;
    }

    /**
     * Method to convert the upsource author id and the unix timestamp on a
     * comment.
     *
     * @see UserInfoResponseDTO getUserInfo(ArrayList<String>, String)
     * @param d DiscussionInFeedDTO object containing the comments to convert
     * data for
     * @param projectKey String for identifying the correct UpsourceConfig
     * @return DiscussionInFeedDTO with comment metadata converted.
     */
    private DiscussionInFeedDTO convertDatesForComments(DiscussionInFeedDTO d, String projectKey) {
        for (CommentDTO c : d.getComments()) {
            long timestamp = c.getDate();
            Calendar convert = Calendar.getInstance();
            convert.setTimeInMillis(timestamp);
            c.getAdditionalProperties().put("timestamp", convert.getTime());

            if (c.getAuthorId() != null || !c.getAuthorId().isEmpty()) {
                ArrayList<String> args = new ArrayList<String>();
                args.add(c.getAuthorId());
                UserInfoResponseDTO wrap = (UserInfoResponseDTO) getUserInfo(args, projectKey)
                        .getResult();
                c.getAdditionalProperties().put("author", wrap.getInfos().get(0));
            }
        }
        return d;
    }

    /**
     * Method to load upsource account details for accounts matching the
     * provided pattern. used in the issueTransition process
     *
     * @throws UpsourceConnectivityException when loadConfig throws an exception
     * @throws UpsourceConnectivityException when an error occurs during the
     * call to the upsource server. Contains a throwable cause.
     * @throws UpsourceConnectionFailureException when returned httpresponse has
     * a statuscode other than 200 OK
     * @throws UpsourceDeserializationException when deserialization of an
     * unrooted response fails
     * @see String unrootJson(String); UpsourceConfig loadConfig(String);
     * @param projectKey String to load the correct UpsourceConfig
     * @param pattern String representing a search pattern
     * @return a ResultDTO containing a UserInfoResponseDTO according to the
     * pattern
     */
    public ResultDTO findUsers(String projectKey, String pattern) {
        UpsourceConfig config = loadConfig(projectKey);
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("limit", config.getLookupReach());
        postBody.put("pattern", pattern);
        ResultDTO result = callUpsourceRpc(config, "findUsers", UserInfoResponseDTO.class , postBody);
        return result;
    }

    /**
     * Method to load upsource account details for provided ids. Used in comment
     * metadata conversion
     *
     * @throws UpsourceConnectivityException when loadConfig throws an exception
     * @throws UpsourceConnectivityException when an error occurs during the
     * call to the upsource server. Contains a throwable cause.
     * @throws UpsourceConnectionFailureException when returned httpresponse has
     * a statuscode other than 200 OK
     * @throws UpsourceDeserializationException when deserialization of an
     * unrooted response fails
     * @see String unrootJson(String); UpsourceConfig loadConfig(String);
     * @param ids List<String> containing the ids of user to look up info on
     * @param projectKey String object to look up the correct UpsourceConfig
     * @return a ResultDTO containing UserInfoResponseDTO for the given ids
     */
    private ResultDTO getUserInfo(List<String> ids, String projectKey) {
        UpsourceConfig config = loadConfig(projectKey);
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("ids", ids);
        ResultDTO result = callUpsourceRpc(config, "getUserInfo", UserInfoResponseDTO.class, postBody);
        return result;
    }

    /**
     * Method to load and convert data for each participant in each review
     * specified in the reviews param. Result data will be stored in the
     * additionalProperties of each respective review. Extracted input data
     * contains the String upsourceUserId, the long role and the long state of a
     * participant in a review. Returned data contains the loaded String
     * upsourceUserName, the loaded String upsourceProfileUrl, the converted
     * String role names and the converted String state name.
     *
     * @param reviews ReviewListDTO containing all reviews to load data for.
     * @param projectKey String representing the jira project key, used to load
     * an upsourceConfig in getUserInfo().
     * @see ResultDTO getUserInfo(List&lt;String&gt;, String)
     * @return ResultDTO containing the ReviewsListDTO with the altered
     * ReviewDescriptorDTO objects.
     */
    public ResultDTO getParticipantsForReviews(ReviewListDTO reviews, String projectKey) {
        for (ReviewDescriptorDTO rev : reviews.getReviews()) {
            Map<String, Map<String, Object>> participants = new HashMap<String, Map<String, Object>>();
            for (ParticipantInReviewDTO participant : rev.getParticipants()) {
                Map<String, Object> p = null;
                if (participants.containsKey(participant.getUserId())) {
                    p = participants.get(participant.getUserId());
                    Map<Long, String> roles = (Map<Long, String>) p.get("roles");
                    long role = participant.getRole();
                    if (!roles.keySet().contains(role)) {
                        roles.put(role, UpsourceEnumValueConverter.convertRole(role));
                        p.put("roles", roles);
                    }
                    SimpleEntry<Long, String> storedState = (SimpleEntry<Long, String>) p.get("state");
                    long state = participant.getState();
                    if (storedState.getKey() < state) {
                        storedState = new SimpleEntry<Long, String>(state, UpsourceEnumValueConverter.convertReviewState(state));
                        p.put("state", storedState);
                    }
                } else {
                    p = new HashMap<String, Object>();
                    String[] ids = {(participant.getUserId())};
                    try {
                        UserInfoResponseDTO name = (UserInfoResponseDTO) getUserInfo(Arrays.asList(ids), projectKey).getResult();
                        FullUserInfoDTO userInfo = name.getInfos().get(0);
                        if (userInfo != null) {
                            String userName = userInfo.getName();
                            if (userName != null) {
                                p.put("userName", userName);
                            }
                        }
                    } catch (Exception ex) {
                        //Empty catch: failed loading of username/profile is not a major fault and should not
                        //further block execution
                    }
                    Map<Long, String> roles = new HashMap<Long, String>();
                    roles.put(participant.getRole(), UpsourceEnumValueConverter.convertRole(participant.getRole()));
                    p.put("roles", roles);
                    SimpleEntry<Long, String> state
                            = new SimpleEntry<Long, String>(participant.getState(),
                                    UpsourceEnumValueConverter.convertReviewState(participant.getState()));
                    p.put("state", state);
                }
                participants.put(participant.getUserId(), p);
            }
            rev.getAdditionalProperties().put("RolesAndStatesPerParticipant", participants);
        }
        ResultDTO response = new ResultDTO();
        response.setResult(reviews);
        return response;
    }

    /**
     * Method for changing an users state in a review (accept/raise concern).
     *
     * @param jiraProjectKey String to identify the correct upsourceConfig
     * @param upsourceProjectId String to identify the correct upsource project
     * @param reviewId String to identify the correct upsource review
     * @param state int representation of the state to switch to
     * @param authHeader String of encoded upsource user credentials to
     * authorize the statechange with the users account
     * @throws UpsourceConnectivityException when loadConfig throws an exception
     * @throws UpsourceConnectivityException when an error occurs during the
     * call to the upsource server. Contains a throwable cause.
     * @throws UpsourceConnectionFailureException when returned httpresponse has
     * a statuscode other than 200 OK
     * @throws UpsourceDeserializationException when deserialization of an
     * unrooted response fails
     * @see String unrootJson(String); UpsourceConfig loadConfig(String);
     */
    public void changeReviewState(String jiraProjectKey, String upsourceProjectId, String reviewId, String authHeader, int state) {
        UpsourceConfig config = loadConfig(jiraProjectKey);
        String target = config.getHost() + "~rpc/updateParticipantInReview";
        WebResource r = this.client.resource(target);
        Map<String, Object> postBody = new HashMap<String, Object>();
        ReviewIdDTO revId = new ReviewIdDTO();
        revId.setProjectId(upsourceProjectId);
        revId.setReviewId(reviewId);
        postBody.put("reviewId", revId);
        postBody.put("state", state);
        ClientResponse response = null;
        try {
            response = r.accept(MediaType.APPLICATION_JSON)
                    .type(MediaType.APPLICATION_JSON)
                    .header("Authorization", authHeader)
                    .post(ClientResponse.class, postBody);
        } catch (com.sun.jersey.api.client.ClientHandlerException ex) {
            throw new UpsourceConnectionFailureException("error making call", ex);
        }
        int responseCode = response.getStatus();
        if (responseCode == 401 || responseCode == 403) {
            JsonParser jp = new JsonParser();
            JsonObject node = jp.parse(response.getEntity(String.class)).getAsJsonObject();
            node = node.getAsJsonObject("error");
            String message = "Upsource reports: " + node.get("message").getAsString() + "; Code: "+ node.get("code").getAsString();            
            throw new UpsourceApiInternalException(message);
        } else if (responseCode != 200) {
            String message
                    = "A network error occurred: " + response.getStatus()
                    + " - " + response.getStatusInfo().getReasonPhrase();
            throw new UpsourceConnectionFailureException(message);
        }
    }

    /**
     * Method to save a map of IssueId to reviewId in the plugin settings, per
     * project.
     *
     * @param projectKey String identifying the project to associate the map
     * with
     * @param reviewIssueMap Map<String, String> containing issueId-reviewId kv
     * pairs.
     */
    @Deprecated
    public void saveReviewIssueMap(final String projectKey, final Map<String, String> reviewIssueMap) {
        transactionTemplate.execute(
                new TransactionCallback() {
            public Object doInTransaction() {
                PluginSettings settings = pluginSettingsFactory.createSettingsForKey(projectKey + "_UCPSettings");
                settings.put("ReviewIssueMap", reviewIssueMap);
                return null;
            }
        }
        );
    }

    /**
     * Method to load a map of IssueId to reviewId in the plugin settings, per
     * project.
     *
     * @param projectKey String identifying the project to associate the map
     * with
     * @return Map<String, String> containing issueId-reviewId kv pairs.
     */
    @Deprecated
    public Map<String, String> loadReviewIssueMap(final String projectKey) {
        Map<String, String> reviewIssueMap
                = (HashMap<String, String>) transactionTemplate.execute(
                        new TransactionCallback() {
                    public Object doInTransaction() {
                        PluginSettings settings = pluginSettingsFactory.createSettingsForKey(projectKey + "_UCPSettings");
                        HashMap<String, String> map = (HashMap<String, String>) settings.get("ReviewIssueMap");
                        return map;
                    }
                }
                );
        if (reviewIssueMap == null) {
            reviewIssueMap = new HashMap<String, String>();
        }
        return reviewIssueMap;
    }

    /**
     * Method for loading a list of diffed files for the specified review.
     *
     * @throws UpsourceConnectivityException when loadConfig throws an exception
     * @throws UpsourceConnectivityException when an error occurs during the
     * call to the upsource server. Contains a throwable cause.
     * @throws UpsourceConnectionFailureException when returned httpresponse has
     * a statuscode other than 200 OK
     * @throws UpsourceDeserializationException when deserialization of an
     * unrooted response fails
     * @see String unrootJson(String); UpsourceConfig loadConfig(String);
     * @param review ReviewDescriptorDTO specifying the review to load the diff
     * list for
     * @param p Project object to identify the correct upsourceConfig
     * @return a ResultDTO containing a ReviewSummaryChangesResponseDTO
     */
    @Override
    public ResultDTO getDiffForReview(ReviewDescriptorDTO review, Project p) {
        UpsourceConfig config = loadConfig(p.getKey());
        Map<String, Object> postBody = new HashMap<String, Object>();
        postBody.put("reviewId", review.getReviewId());
        ResultDTO result = callUpsourceRpc(config, "getReviewSummaryChanges", ReviewSummaryChangesResponseDTO.class, postBody);
        return result;
    }

    /**
     * Method for loading the diff of the specified file.
     *
     * @throws UpsourceConnectivityException when loadConfig throws an exception
     * @throws UpsourceConnectivityException when an error occurs during the
     * call to the upsource server. Contains a throwable cause.
     * @throws UpsourceConnectionFailureException when returned httpresponse has
     * a statuscode other than 200 OK
     * @throws UpsourceDeserializationException when deserialization of an
     * unrooted response fails
     * @see String unrootJson(String); UpsourceConfig loadConfig(String);
     * @param jiraProjectKey String to identify the correct upsourceConfig
     * @param reviewId String to specify the review in which the file is
     * included
     * @param upsourceProjectId String to specify the project in which the file
     * is found
     * @param revisionId String to specify the revision for which the diff
     * should be loaded
     * @param fileName String to specify which file's diff should be loaded
     * @return a ResultDTO containing a FileInlineDiffResponse
     */
    public ResultDTO getDiffForFile(
            String jiraProjectKey, String reviewId,
            String upsourceProjectId, String revisionId, String fileName) {
        UpsourceConfig config = loadConfig(jiraProjectKey);
        Map<String, Object> postBody = new HashMap<String, Object>();
        FileInRevisionDTO f = new FileInRevisionDTO(upsourceProjectId, revisionId, fileName);
        postBody.put("rightFile", f);
        postBody.put("ignoreWhitespace", true);
        ResultDTO result = callUpsourceRpc(config, "getFileInlineDiff", FileInlineDiffResponse.class, postBody);
        return result;
    }

    /**
     * Method to generate a list of html-ready Strings with css for displaying
     * the specified diff.
     *
     * @param data FileInlineDiffResponse object containing the diff and it's
     * formatting data
     * @return List&lt;String&gt; with html ready lines;
     */
    public Map<String, Object> parseDiffData(FileInlineDiffResponse data) {
        String response = data.getText();

        //saveguard diamond brackets for html
        response = response.replaceAll("<", "�");
        response = response.replaceAll(">", "�");
        
        List<TextMarkupDTO> markups = data.getSyntaxMarkup();
        
        if (markups != null && !markups.isEmpty()) {
            if(data.getAddedRanges() != null && !data.getAddedRanges().isEmpty()){
                for(RangeDTO r : data.getAddedRanges()){
                    TextMarkupDTO e = new TextMarkupDTO();
                    e.setRange(r);
                    TextAttributeDTO f = new TextAttributeDTO(null, "a6f3a6", null, null, null, null);
                    e.setTextAttribute(f);
                    markups.add(e);
                }
            }
            
            if(data.getRemovedRanges()!= null && !data.getRemovedRanges().isEmpty()){
                for(RangeDTO r : data.getRemovedRanges()){
                    TextMarkupDTO e = new TextMarkupDTO();
                    e.setRange(r);
                    TextAttributeDTO f = new TextAttributeDTO(null, "f8cbcb", null, null, null, null);
                    e.setTextAttribute(f);
                    markups.add(e);
                }
            }
            Collections.sort(markups, Collections.reverseOrder());
            SortedMap<Integer, List<String>> markupMap = new TreeMap<Integer, List<String>>(Collections.reverseOrder());
            for (TextMarkupDTO tm : markups) {
                //create style for opening span tag
                String style = "style=\"";
                TextAttributeDTO ta = tm.getTextAttribute();
                if (ta.getBgColor() != null && !ta.getBgColor().isEmpty()) {
                    style += "background:#" + ta.getBgColor() + "; ";
                }
                if (ta.getFgColor() != null && !ta.getFgColor().isEmpty()) {
                    style += "color:#" + ta.getFgColor() + "; ";
                }
                if (ta.getFontStyle() != null && !ta.getFontStyle().isEmpty()) {
                    if (ta.getFontStyle().contains("b")) {
                        style += "font-weight:bold; ";
                    }
                    if (ta.getFontStyle().contains("i")) {
                        style += "font-style:italic; ";
                    }
                }
                style += "\"";
                String openEnclosure = "<span " + style + ">";
                String closeEnclosure = "</span>";
                if (markupMap.get(tm.getRange().getStartOffset()) != null) {
                    markupMap.get(tm.getRange().getStartOffset()).add(openEnclosure);
                } else {
                    List<String> entry = new ArrayList<String>();
                    entry.add(openEnclosure);
                    markupMap.put(tm.getRange().getStartOffset(), entry);
                }
                if (markupMap.get(tm.getRange().getEndOffset()) != null) {
                    markupMap.get(tm.getRange().getEndOffset()).add(closeEnclosure);
                } else {
                    List<String> entry = new ArrayList<String>();
                    entry.add(closeEnclosure);
                    markupMap.put(tm.getRange().getEndOffset(), entry);
                }
            }
            for (Entry<Integer, List<String>> e : markupMap.entrySet()) {
                Collections.sort(e.getValue(), new SpanEnclosureComparator());
                String insertable = "";
                for (String s : e.getValue()) {
                    insertable += s;
                }
                response = response.substring(0, e.getKey()) + insertable + response.substring(e.getKey());
            }
        }
        
        //transform saveguarded diamond brackets for html
        response = response.replaceAll("�", "&lt;");
        response = response.replaceAll("�", "&gt;");

        //create list of Strings from single String for easy manipulation
        List<String> splitResponse = new ArrayList<String>(Arrays.asList(response.split("\n", -1)));

        //create list of linenumbers as strings
        List<String> oldLineNumbers = new ArrayList<String>();
        
        for (int i : data.getOldLineNumbers()) {
            oldLineNumbers.add(""+(i+1));
        } 
        List<String> newLineNumbers = new ArrayList<String>();
        for(int i : data.getNewLineNumbers()){
            newLineNumbers.add(""+(i+1));
        }
        //add markup for diff
        if (data.getAddedLines() != null) {
            String style = "style=\"background:#aaffbb\"";
            for (int i : data.getAddedLines()) {
                String target = splitResponse.get(i);
                target = "<span " + style + ">" + target + "</span>";
                splitResponse.set(i, target);
            }
        }
        if (data.getRemovedLines() != null) {
            String style = "style=\"background:#ffaaaa\"";
            for (int i : data.getRemovedLines()) {
                String target = splitResponse.get(i);
                target = "<span " + style + ">" + target + "</span>";
                splitResponse.set(i, target);
            }
        }
        if (data.getModifiedLines() != null) {
            String style = "style=\"background:#d7d4f0\"";
            for (int i : data.getModifiedLines()) {
                String target = splitResponse.get(i);
                target = "<span " + style + ">" + target + "</span>";
                splitResponse.set(i, target);
            }
        }
           
        Map<String, Object> responseMap = new HashMap<String, Object>();
        responseMap.put("text", splitResponse);
        responseMap.put("oldLineNumbers", oldLineNumbers);
        responseMap.put("newLineNumbers", newLineNumbers);
        return responseMap;
    }

    private class SpanEnclosureComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            if (o1.equals(o2)) {
                return 0;
            }
            int a = convertValue(o1);
            int b = convertValue(o2);
            if(a == 0 || b == 0){
                return 0;
            }else{
                return a - b;
            }
        }
        
        public int convertValue(String s){
        int response = 0;
        if(s.contains("</span>")){
            response = 1;
        }else if(s.contains("background")){
            response = 2;
        }else if(s.contains("bold")){
            response = 4;
        }
        return response;
        }
    }

    /**
     * Method to remove collapsed lines from a Diff file String List and its
     * lineNumber list.
     * @param input Map&lt;String, Object&gt; containing at least the list of
     * codelines in the pair &lt;"text", List&lt;String&gt;&gt; and possibly the
     * list of code linenumbers in the pair
     * &lt;"oldLineNumbers" ,List&lt;Integer&gt;&gt;.
     * &lt;"newLineNumbers", List&lt;Integer&gt;&gt;.
     * @param collapsedLines List&ltRangeDTO&gt; with the ranges of collapsed Lines.
     * @return Map&lt;String, Object&gt; containing the lists of textStrings and
     * line numbers with replacements for collapsed lines
     */
    public Map<String, Object> collapseLines(Map<String, Object> input, List<RangeDTO> collapsedLines) {
        List<String> uncollapsedLines = (List<String>) input.get("text");
        List<String> oldLineNumbers = (List<String>) input.get("oldLineNumbers");
        List<String> newLineNumbers = (List<String>) input.get("newLineNumbers");
        if (collapsedLines != null) {
            Collections.sort(collapsedLines, Collections.reverseOrder());
            for (RangeDTO r : collapsedLines) {
                if(r.getEndOffset()-r.getStartOffset() > 3){
                    for (int i = r.getEndOffset() - 1; i >= r.getStartOffset(); i--) {
                        uncollapsedLines.remove(i);
                        oldLineNumbers.remove(i);
                        newLineNumbers.remove(i);
                    }
                    oldLineNumbers.add(r.getStartOffset(), "\n"+(r.getStartOffset()+1)+"\n{...}\n"+r.getEndOffset());
                    newLineNumbers.add(r.getStartOffset(), "\n"+(r.getStartOffset()+1)+"\n{...}\n"+r.getEndOffset());
                    uncollapsedLines.add(r.getStartOffset(), "\n\t++++++++++++++++++++++++++++++++++++++++++++++\n\tLINES SUPPRESSED. VIEW UPSOURCE FOR SOURCE\n\t++++++++++++++++++++++++++++++++++++++++++++++\n");
                }
            }
        }
        input.put("oldLineNumbers", oldLineNumbers);
        input.put("newLineNumbers", newLineNumbers);
        input.put("text", uncollapsedLines);
        return input;
    }

    /**
     * aggregate method combining the loading and the formatting of a file's
     * diff.
     *
     * @param jiraProjectKey
     * @param reviewId
     * @param upsourceProjectId
     * @param revisionId
     * @param fileName
     * @see String generateDiffHtml(FileInlineDiffREsponse); ResultDTO
     * getDiffForFile(String, String, String, String, String)
     * @return a String containing the diff html
     */
    public Map<String, Object> displayDiff(
            String jiraProjectKey, String reviewId,
            String upsourceProjectId, String revisionId, String fileName) {
        Map<String, Object> result = null;
        //Load data from upsource
        FileInlineDiffResponse data = (FileInlineDiffResponse) getDiffForFile(jiraProjectKey, reviewId, upsourceProjectId,
                revisionId, fileName).getResult();
        //parse data to html ready state
        result = parseDiffData(data);
        //collapse unchanged lines
        result = collapseLines(result, data.getCollapsedLines());
        return result;
    }   
}
