/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.developmenttools.impl;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nikon.upsourcedtos.ProjectInfoDTO;
import dto.UpsourceConfig;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author RVanhuysse
 */
@Path("/")
@Scanned
public class ConfigResource {

    @ComponentImport
    private final ProjectManager projectManager;
    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;
    @ComponentImport
    private final TransactionTemplate transactionTemplate;
    
    private UpsourceConnectivityExtentionMethodsImp methodContainer;

    @Inject
    public ConfigResource(
            ProjectManager projectManager,
            UserManager userManager, 
            PluginSettingsFactory pluginSettingsFactory,
            TransactionTemplate transactionTemplate,
            UpsourceConnectivityExtentionMethodsImp methodContainer) {
        this.projectManager = projectManager;
        this.userManager = userManager;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;
        this.methodContainer = methodContainer;        
    }

    public UpsourceConnectivityExtentionMethodsImp getMethodContainer() {
        return methodContainer;
    }

    public void setMethodContainer(UpsourceConnectivityExtentionMethodsImp methodContainer) {
        this.methodContainer = methodContainer;
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(final UpsourceConfig config, @Context HttpServletRequest request) {
        UserKey userKey = userManager.getRemoteUserKey(request);
        //TODO set correct user group
        if (userKey == null || !(userManager.isAdmin(userKey))) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        config.setHost(checkHostFormat(config.getHost()));
        if(config.getUpsourceProjectId() == null || config.getUpsourceProjectId().isEmpty()){
            config.setUpsourceProjectId(getUpsourceProjectId(config,
                this.projectManager.getProjectByCurrentKey(config.getProjectKey())));
        }
        transactionTemplate.execute(
                new TransactionCallback() {
            public Object doInTransaction() {
                PluginSettings pluginSettings = pluginSettingsFactory.createSettingsForKey(config.getProjectKey()+"_UCPSettings");
                pluginSettings.put(UpsourceConfig.class.getName() + ".host", config.getHost());
                pluginSettings.put(UpsourceConfig.class.getName() + ".username", config.getUsername());
                pluginSettings.put(UpsourceConfig.class.getName() + ".password", config.getPassword());
                pluginSettings.put(UpsourceConfig.class.getName() + ".projectKey", config.getProjectKey());
                pluginSettings.put(UpsourceConfig.class.getName() + ".upsourceProjectId", config.getUpsourceProjectId());
                return null;
            }
        }
        );
        return Response.noContent().build();
    }
    
    @POST
    @Path("testConnection")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response testConnection(String data, @Context HttpServletRequest request){
        System.err.println(this.methodContainer.now()+" >>> Entered testConnection");
        UserKey userKey = userManager.getRemoteUserKey(request);
        //TODO set correct user group
        if (userKey == null || !(userManager.isAdmin(userKey))) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        System.err.println(this.methodContainer.now()+" >>> authorized");
        String projectKey = "";
        try{
            JsonParser p = new JsonParser();
            JsonObject root = p.parse(data).getAsJsonObject();
            projectKey = root.get("projectKey").getAsString();
        }catch(Exception ex){
            System.err.println(this.methodContainer.now()+" >>> parsing error: "+ ex.getMessage() + " StackTrace: "+ org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(ex));
            return Response.status(500).entity("Failed to parse data for connection test.").build();
        }
        if(projectKey == null || projectKey.isEmpty()){
            System.err.println(this.methodContainer.now()+" >>> projectKey error");
            return Response.status(500).entity("No projectKey received.").build();
        }
        ProjectInfoDTO p = null;
        try{
            p = (ProjectInfoDTO)this.methodContainer.getProjectInfo(
                                    projectManager.getProjectObjByKey(projectKey)
                                ).getResult();
        }catch(Exception ex){
            System.err.println(this.methodContainer.now()+" >>> methodContainer Error: "+ ex.getMessage() + " StackTrace: "+ org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(ex));
            return Response.status(500).entity("Connection Failed").build();
        }
        if(p != null){
            ObjectMapper mapper = new ObjectMapper();
            String response = "";
            try {
                response = mapper.writeValueAsString(p);
            } catch (IOException ex) {
                return Response.status(500).entity("Failed to parse data for connection test.").build();
            }
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        System.err.println(this.methodContainer.now()+" >>> unknown error");
        return Response.status(500).entity("execution failed").build();
    }
    
    @POST
    @Path("wipe")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response wipe(String data, @Context HttpServletRequest request){
        UserKey userKey = userManager.getRemoteUserKey(request);
        //TODO set correct user group
        if (userKey == null || !(userManager.isAdmin(userKey))) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        JsonParser p = new JsonParser();
        JsonObject root = p.parse(data).getAsJsonObject();
        final String projectKey = root.get("projectKey").getAsString();
        transactionTemplate.execute(
                new TransactionCallback() {
            public Object doInTransaction() {
                PluginSettings pluginSettings = pluginSettingsFactory.createSettingsForKey(projectKey+"_UCPSettings");
                pluginSettings.put(UpsourceConfig.class.getName() + ".host", "");
                pluginSettings.put(UpsourceConfig.class.getName() + ".username", "");
                pluginSettings.put(UpsourceConfig.class.getName() + ".password", "");
                pluginSettings.put(UpsourceConfig.class.getName() + ".projectKey", "");
                pluginSettings.put(UpsourceConfig.class.getName() + ".upsourceProjectId", "");
                return null;
            }
        }
        );
        return Response.noContent().build();
    }
    
    private String checkHostFormat(String input) {
        if(input == null){
            return "host field empty";
        }
        //check for protocol
        Pattern protocol = Pattern.compile("^.*://.*$");
        Matcher protocolMatcher = protocol.matcher(input);
        if(!protocolMatcher.matches()){
            input = "http://" + input;
        }
        //check for trailing /
        Pattern slashes = Pattern.compile(".*[^/]$");
        Matcher m = slashes.matcher(input);
        if(m.matches()){
            input = input + "/";
        }
        return input;
    }
    
    private String getUpsourceProjectId(UpsourceConfig config, Project jiraProject){
        String jiraProjectName = jiraProject.getName();
        return this.methodContainer.getUpsourceProjectId(config, jiraProjectName);
    }
    
    
}