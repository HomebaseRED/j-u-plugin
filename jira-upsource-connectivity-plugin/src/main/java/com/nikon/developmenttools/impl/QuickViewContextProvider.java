/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.developmenttools.impl;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.nikon.upsourcedtos.ReviewListDTO;
import dto.UpsourceConnectionFailureException;
import dto.UpsourceConnectivityException;
import dto.UpsourceUnsetConfigException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

/**
 *
 * @author RVanhuysse
 */
public class QuickViewContextProvider extends AbstractJiraContextProvider {

    @Inject
    private UpsourceConnectivityExtentionMethods methodContainer;
    
    public QuickViewContextProvider(){  
    }

    public UpsourceConnectivityExtentionMethods getMethodContainer() {
        return methodContainer;
    }

    public void setMethodContainer(UpsourceConnectivityExtentionMethods methodContainer) {
        this.methodContainer = methodContainer;
    }
    
    @Override
    public Map getContextMap(ApplicationUser au, JiraHelper jh) {  
        Map contextMap = new HashMap();
        List<String> exceptionMessages = new ArrayList<String>();
        try {
            Issue currentIssue= (Issue) jh.getContextParams().get("issue");
            ReviewListDTO r = (ReviewListDTO) this.methodContainer.getReviewsForIssue(currentIssue).getResult();
            contextMap.put("reviewsDTO", r);
        }catch (UpsourceConnectionFailureException ucfe) {
            System.err.println(this.methodContainer.now()+" - UPSOURCE CONNECTION EXCEPTION: " + ucfe.getMessage()+ " STACKTRACE: ");
            ucfe.printStackTrace();
            if (ucfe.getCause() != null && ucfe.getCause().getCause() instanceof java.net.UnknownHostException) {
                exceptionMessages.add("Error resolving host, please check the Upsource Connectivity Settings for this project and your network settings.");
            } else {
                exceptionMessages.add(ucfe.toString());
            }
        } catch (UpsourceUnsetConfigException uuce) {
            System.err.println(this.methodContainer.now()+" - UPSOURCE CONFIGRATION EXCEPTION: " + uuce.getMessage()+ " STACKTRACE: ");
            uuce.printStackTrace();
            exceptionMessages.add(uuce.getMessage());
        } catch (UpsourceConnectivityException uce) {
            System.err.println(this.methodContainer.now()+" - UPSOURCE EXCEPTION: " + uce.getMessage()+ " STACKTRACE: ");
            uce.printStackTrace();
            if (uce.getCause() != null) {
                exceptionMessages.add(uce.getCause().toString());
            } else {
                exceptionMessages.add(uce.toString());
            }
        } catch (Exception ex) {
            exceptionMessages.add(ex.toString());
            System.err.println(this.methodContainer.now()+" - EXCEPTION: " + ex.toString()+ " STACKTRACE: ");
            ex.printStackTrace();
        }
        contextMap.put("exceptionList", exceptionMessages);
        return contextMap;
    }
    
}
