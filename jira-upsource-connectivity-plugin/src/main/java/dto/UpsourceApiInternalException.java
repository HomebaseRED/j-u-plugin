/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author RVanhuysse
 */
public class UpsourceApiInternalException extends UpsourceConnectivityException{

    public UpsourceApiInternalException() {
        super();
    }

    public UpsourceApiInternalException(String message) {
        super(message);
    }

    public UpsourceApiInternalException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpsourceApiInternalException(Throwable cause) {
        super(cause);
    }

    public UpsourceApiInternalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
    
}
