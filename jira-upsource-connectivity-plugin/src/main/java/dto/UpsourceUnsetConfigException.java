/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author RVanhuysse
 */
public class UpsourceUnsetConfigException extends UpsourceConnectivityException {

    public UpsourceUnsetConfigException() {
        super();
    }

    public UpsourceUnsetConfigException(String message) {
        super(message);
    }

    public UpsourceUnsetConfigException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpsourceUnsetConfigException(Throwable cause) {
        super(cause);
    }

    public UpsourceUnsetConfigException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
    
}
