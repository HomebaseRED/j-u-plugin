/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author RVanhuysse
 */
public class UpsourceDeserializationException extends UpsourceConnectivityException{

    public UpsourceDeserializationException() {
        super();
    }

    public UpsourceDeserializationException(String message) {
        super(message);
    }

    public UpsourceDeserializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpsourceDeserializationException(Throwable cause) {
        super(cause);
    }

    public UpsourceDeserializationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
