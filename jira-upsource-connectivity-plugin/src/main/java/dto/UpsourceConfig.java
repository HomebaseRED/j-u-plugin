/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import com.sun.jersey.core.util.Base64;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RVanhuysse
 */
//DTO
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UpsourceConfig {

    @XmlElement
    private String host;
    @XmlElement
    private String username;
    @XmlElement
    private String password;
    @XmlElement 
    private int lookupReach = 32;
    @XmlElement
    private String projectKey;
    @XmlElement
    private String upsourceProjectId;
    
    
    public UpsourceConfig(){
        /*this.host = "host";
        this.username = "userName";
        this.password = "passWord";
        this.lookupReach = 64;
        this.projectKey = "key";*/
    }
    
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getLookupReach() {
        return lookupReach;
    }

    public void setLookupReach(int lookupReach) {
        this.lookupReach = lookupReach;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public String getUpsourceProjectId() {
        return upsourceProjectId;
    }

    public void setUpsourceProjectId(String upsourceProjectId) {
        this.upsourceProjectId = upsourceProjectId;
    }
    
    public String getAuthHeader() {
        String encoded = new String(
            Base64.encode(
                    this.username
                    +":"+
                    this.password
            )
        );
        return new String("Basic " + encoded);
    }

    @Override
    public String toString() {
        return "UpsourceConfig{" + "host=" + host + ", username=" + username + ", password=" + password + ", lookupReach=" + lookupReach + ", projectKey=" + projectKey + ", upsourceProjectId=" + upsourceProjectId + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (this.host != null ? this.host.hashCode() : 0);
        hash = 67 * hash + (this.username != null ? this.username.hashCode() : 0);
        hash = 67 * hash + (this.password != null ? this.password.hashCode() : 0);
        hash = 67 * hash + this.lookupReach;
        hash = 67 * hash + (this.projectKey != null ? this.projectKey.hashCode() : 0);
        hash = 67 * hash + (this.upsourceProjectId != null ? this.upsourceProjectId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UpsourceConfig other = (UpsourceConfig) obj;
        if (this.lookupReach != other.lookupReach) {
            return false;
        }
        if ((this.host == null) ? (other.host != null) : !this.host.equals(other.host)) {
            return false;
        }
        if ((this.username == null) ? (other.username != null) : !this.username.equals(other.username)) {
            return false;
        }
        if ((this.password == null) ? (other.password != null) : !this.password.equals(other.password)) {
            return false;
        }
        if ((this.projectKey == null) ? (other.projectKey != null) : !this.projectKey.equals(other.projectKey)) {
            return false;
        }
        if ((this.upsourceProjectId == null) ? (other.upsourceProjectId != null) : !this.upsourceProjectId.equals(other.upsourceProjectId)) {
            return false;
        }
        return true;
    }

    
     
}
