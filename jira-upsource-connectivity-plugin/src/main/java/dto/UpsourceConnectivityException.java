/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author RVanhuysse
 */
public class UpsourceConnectivityException extends RuntimeException{
 
    public UpsourceConnectivityException(){
        super();
    }

    public UpsourceConnectivityException(String message) {
        super(message);
    }

    public UpsourceConnectivityException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpsourceConnectivityException(Throwable cause) {
        super(cause);
    }

    public UpsourceConnectivityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
