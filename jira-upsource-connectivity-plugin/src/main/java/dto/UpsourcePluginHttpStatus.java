/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import javax.ws.rs.core.Response;

/**
 *
 * @author RVanhuysse
 */
public enum UpsourcePluginHttpStatus implements Response.StatusType{

    CRITICAL_ERROR(550, "Critical error"),
    
    DESERIALIZATION_ERROR(552, "Deserialization failed"),
    WEBHOOK_NOT_SUPPORTED(553, "Webhook not supported"),
    
    WEBHOOK_DATA_NULL(554, "Data in Webhook is null"),
    WEBHOOK_DATA_ACTOR_NULL(555, "Extracted Actor is null"),
    ACTOR_UNRESOLVED(556, "Actor was not resolved"),
    ISSUE_UNRESOLVED(557, "Issue was not resolved"),
    AUTHORIZING_USER_NULL(558, "Authorizing User is null"),
    TARGET_ISSUE_NULL(559, "Target Issue is null"),
    TRANSITION_VALIDATION_FAILED(560, "Transition validation failed"),
    TRANSITION_UNAVAILABLE(561, "Requested Transition not available in current Issue State")
    ;
    
    private int code;
    private String reason;
    private Response.Status.Family family;
    
    UpsourcePluginHttpStatus(int code, String reason){
        this.code = code;
        this.reason = reason;
        this.family = familyOf(code);
    }
    
    public static Response.Status.Family familyOf(final int statusCode){
        switch( (statusCode/100) ){
            case 1:
                return Response.Status.Family.INFORMATIONAL;
            case 2:
                return Response.Status.Family.SUCCESSFUL;
            case 3:
                return Response.Status.Family.REDIRECTION;
            case 4:
                return Response.Status.Family.CLIENT_ERROR;
            case 5:
                return Response.Status.Family.SERVER_ERROR;
            default:
                return Response.Status.Family.OTHER;
        }
    }
    
    @Override
    public int getStatusCode() {
        return this.code;
    }

    @Override
    public Response.Status.Family getFamily() {
        return this.family;
    }

    @Override
    public String getReasonPhrase() {
        return this.reason;
    }
    
}
