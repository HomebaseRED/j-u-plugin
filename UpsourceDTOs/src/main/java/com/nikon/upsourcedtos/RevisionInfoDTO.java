/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * @author RVanhuysse
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "projectId",
    "revisionId",
    "revisionDate",
    "effectiveRevisionDate",
    "revisionCommitMessage",
    "state",
    "vcsRevisionId",
    "shortRevisionId",
    "authorId",
    "reachability",
    "tags",
    "branchHeadLabel"
})
public class RevisionInfoDTO implements Serializable, UpsourceDTO {
   @JsonProperty("projectId")
    private String projectId;
    @JsonProperty("revisionId")
    private String revisionId;
    @JsonProperty("revisionDate")
    private long revisionDate;
    @JsonProperty("effectiveRevisionDate")
    private long effectiveRevisionDate;
    @JsonProperty("revisionCommitMessage")
    private String revisionCommitMessage;
    @JsonProperty("state")
    private long state;
    @JsonProperty("vcsRevisionId")
    private String vcsRevisionId;
    @JsonProperty("shortRevisionId")
    private String shortRevisionId;
    @JsonProperty("authorId")
    private String authorId;
    @JsonProperty("reachability")
    private long reachability;
    @JsonProperty("tags")
    private List<String> tags = null;
    @JsonProperty("branchHeadLabel")
    private List<String> branchHeadLabel = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -3618394300662429984L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public RevisionInfoDTO() {
    }

    /**
     * 
     * @param reachability
     * @param vcsRevisionId
     * @param state
     * @param revisionCommitMessage
     * @param revisionDate
     * @param projectId
     * @param shortRevisionId
     * @param effectiveRevisionDate
     * @param revisionId
     * @param authorId
     * @param tags
     * @param branchHeadLabel
     */
    public RevisionInfoDTO(String projectId, String revisionId, long revisionDate, long effectiveRevisionDate, String revisionCommitMessage, long state, String vcsRevisionId, String shortRevisionId, String authorId, long reachability, List<String> tags, List<String> branchHeadLabel) {
        super();
        this.projectId = projectId;
        this.revisionId = revisionId;
        this.revisionDate = revisionDate;
        this.effectiveRevisionDate = effectiveRevisionDate;
        this.revisionCommitMessage = revisionCommitMessage;
        this.state = state;
        this.vcsRevisionId = vcsRevisionId;
        this.shortRevisionId = shortRevisionId;
        this.authorId = authorId;
        this.reachability = reachability;
        this.tags = tags;
        this.branchHeadLabel = branchHeadLabel;
    }

    @JsonProperty("projectId")
    public String getProjectId() {
        return projectId;
    }

    @JsonProperty("projectId")
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @JsonProperty("revisionId")
    public String getRevisionId() {
        return revisionId;
    }

    @JsonProperty("revisionId")
    public void setRevisionId(String revisionId) {
        this.revisionId = revisionId;
    }

    @JsonProperty("revisionDate")
    public long getRevisionDate() {
        return revisionDate;
    }

    @JsonProperty("revisionDate")
    public void setRevisionDate(long revisionDate) {
        this.revisionDate = revisionDate;
    }

    @JsonProperty("effectiveRevisionDate")
    public long getEffectiveRevisionDate() {
        return effectiveRevisionDate;
    }

    @JsonProperty("effectiveRevisionDate")
    public void setEffectiveRevisionDate(long effectiveRevisionDate) {
        this.effectiveRevisionDate = effectiveRevisionDate;
    }

    @JsonProperty("revisionCommitMessage")
    public String getRevisionCommitMessage() {
        return revisionCommitMessage;
    }

    @JsonProperty("revisionCommitMessage")
    public void setRevisionCommitMessage(String revisionCommitMessage) {
        this.revisionCommitMessage = revisionCommitMessage;
    }

    @JsonProperty("state")
    public long getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(long state) {
        this.state = state;
    }

    @JsonProperty("vcsRevisionId")
    public String getVcsRevisionId() {
        return vcsRevisionId;
    }

    @JsonProperty("vcsRevisionId")
    public void setVcsRevisionId(String vcsRevisionId) {
        this.vcsRevisionId = vcsRevisionId;
    }

    @JsonProperty("shortRevisionId")
    public String getShortRevisionId() {
        return shortRevisionId;
    }

    @JsonProperty("shortRevisionId")
    public void setShortRevisionId(String shortRevisionId) {
        this.shortRevisionId = shortRevisionId;
    }

    @JsonProperty("authorId")
    public String getAuthorId() {
        return authorId;
    }

    @JsonProperty("authorId")
    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    @JsonProperty("reachability")
    public long getReachability() {
        return reachability;
    }

    @JsonProperty("reachability")
    public void setReachability(long reachability) {
        this.reachability = reachability;
    }

    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @JsonProperty("branchHeadLabel")
    public List<String> getBranchHeadLabel() {
        return branchHeadLabel;
    }

    @JsonProperty("branchHeadLabel")
    public void setBranchHeadLabel(List<String> branchHeadLabel) {
        this.branchHeadLabel = branchHeadLabel;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(projectId).append(revisionId).append(revisionDate).append(effectiveRevisionDate).append(revisionCommitMessage).append(state).append(vcsRevisionId).append(shortRevisionId).append(authorId).append(reachability).append(tags).append(branchHeadLabel).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RevisionInfoDTO) == false) {
            return false;
        }
        RevisionInfoDTO rhs = ((RevisionInfoDTO) other);
        return new EqualsBuilder().append(projectId, rhs.getProjectId()).append(revisionId, rhs.getRevisionId()).append(revisionDate, rhs.getRevisionDate()).append(effectiveRevisionDate, rhs.getEffectiveRevisionDate()).append(revisionCommitMessage, rhs.getRevisionCommitMessage()).append(state, rhs.getState()).append(vcsRevisionId, rhs.getVcsRevisionId()).append(shortRevisionId, rhs.getShortRevisionId()).append(authorId, rhs.getAuthorId()).append(reachability, rhs.getReachability()).append(tags, rhs.getTags()).append(branchHeadLabel, rhs.getBranchHeadLabel()).append(additionalProperties, rhs.getAdditionalProperties()).isEquals();
    } 
}
