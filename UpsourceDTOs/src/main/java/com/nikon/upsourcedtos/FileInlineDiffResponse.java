/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "isIdentical",
    "text",
    "oldFile",
    "newFile",
    "contentType",
    "addedLines",
    "removedLines",
    "modifiedLines",
    "collapsedLines",
    "addedRanges",
    "removedRanges",
    "syntaxMarkup",
    "diffToOldDocument",
    "diffToNewDocument",
    "oldLineNumbers",
    "newLineNumbers",
    "annotation",
    "hasUnrelatedChanges"
})
public class FileInlineDiffResponse implements Serializable, UpsourceDTO{
    
    @JsonProperty("isIdentical")
    private boolean isIdentical;
    @JsonProperty("text")
    private String text;
    @JsonProperty("oldFile")
    private FileInRevisionDTO oldFile;
    @JsonProperty("newFile")
    private FileInRevisionDTO newFile;
    @JsonProperty("contentType")
    private FileContentTypeDTO contentType;
    @JsonProperty("addedLines")
    private List<Integer> addedLines;
    @JsonProperty("removedLines")
    private List<Integer> removedLines;
    @JsonProperty("modifiedLines")
    private List<Integer> modifiedLines;
    @JsonProperty("collapsedLines")
    private List<RangeDTO> collapsedLines;
    @JsonProperty("addedRanges")
    private List<RangeDTO> addedRanges;
    @JsonProperty("removedRanges")
    private List<RangeDTO> removedRanges;
    @JsonProperty("syntaxMarkup")
    private List<TextMarkupDTO> syntaxMarkup;
    @JsonProperty("diffToOldDocument")
    private List<RangeMappingDTO> diffToOldDocument;
    @JsonProperty("diffToNewDocument")
    private List<RangeMappingDTO> diffToNewDocument;
    @JsonProperty("oldLineNumbers")
    private List<Integer> oldLineNumbers;
    @JsonProperty("newLineNumbers")
    private List<Integer> newLineNumbers;
    @JsonProperty("annotation")
    private List<FileAnnotationSectionDTO> annotation;
    @JsonProperty("hasUnrelatedChanges")
    private boolean hasUnrelatedChanges;

    public FileInlineDiffResponse() {
    }

    public FileInlineDiffResponse(boolean isIdentical, String text, FileInRevisionDTO oldFile, FileInRevisionDTO newFile, FileContentTypeDTO contentType, List<Integer> addedLines, List<Integer> removedLines, List<Integer> modifiedLines, List<RangeDTO> collapsedLines, List<RangeDTO> addedRanges, List<RangeDTO> removedRanges, List<TextMarkupDTO> syntaxMarkup, List<RangeMappingDTO> diffToOldDocument, List<RangeMappingDTO> diffToNewDocument, List<Integer> oldLineNumbers, List<Integer> newLineNumbers, List<FileAnnotationSectionDTO> annotation, boolean hasUnrelatedChanges) {
        this.isIdentical = isIdentical;
        this.text = text;
        this.oldFile = oldFile;
        this.newFile = newFile;
        this.contentType = contentType;
        this.addedLines = addedLines;
        this.removedLines = removedLines;
        this.modifiedLines = modifiedLines;
        this.collapsedLines = collapsedLines;
        this.addedRanges = addedRanges;
        this.removedRanges = removedRanges;
        this.syntaxMarkup = syntaxMarkup;
        this.diffToOldDocument = diffToOldDocument;
        this.diffToNewDocument = diffToNewDocument;
        this.oldLineNumbers = oldLineNumbers;
        this.newLineNumbers = newLineNumbers;
        this.annotation = annotation;
        this.hasUnrelatedChanges = hasUnrelatedChanges;
    }

    @JsonProperty("isIdentical")
    public boolean isIsIdentical() {
        return isIdentical;
    }

    @JsonProperty("isIdentical")
    public void setIsIdentical(boolean isIdentical) {
        this.isIdentical = isIdentical;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("oldFile")
    public FileInRevisionDTO getOldFile() {
        return oldFile;
    }

    @JsonProperty("oldFile")
    public void setOldFile(FileInRevisionDTO oldFile) {
        this.oldFile = oldFile;
    }

    @JsonProperty("newFile")
    public FileInRevisionDTO getNewFile() {
        return newFile;
    }

    @JsonProperty("newFile")
    public void setNewFile(FileInRevisionDTO newFile) {
        this.newFile = newFile;
    }

    @JsonProperty("contentType")
    public FileContentTypeDTO getContentType() {
        return contentType;
    }

    @JsonProperty("contentType")
    public void setContentType(FileContentTypeDTO contentType) {
        this.contentType = contentType;
    }

    @JsonProperty("addedLines")
    public List<Integer> getAddedLines() {
        return addedLines;
    }

    @JsonProperty("addedLines")
    public void setAddedLines(List<Integer> addedLines) {
        this.addedLines = addedLines;
    }

    @JsonProperty("removedLines")
    public List<Integer> getRemovedLines() {
        return removedLines;
    }

    @JsonProperty("removedLines")
    public void setRemovedLines(List<Integer> removedLines) {
        this.removedLines = removedLines;
    }

    @JsonProperty("modifiedLines")
    public List<Integer> getModifiedLines() {
        return modifiedLines;
    }

    @JsonProperty("modifiedLines")
    public void setModifiedLines(List<Integer> modifiedLines) {
        this.modifiedLines = modifiedLines;
    }

    @JsonProperty("collapsedLines")
    public List<RangeDTO> getCollapsedLines() {
        return collapsedLines;
    }

    @JsonProperty("collapsedLines")
    public void setCollapsedLines(List<RangeDTO> collapsedLines) {
        this.collapsedLines = collapsedLines;
    }

    @JsonProperty("addedRanges")
    public List<RangeDTO> getAddedRanges() {
        return addedRanges;
    }

    @JsonProperty("addedRanges")
    public void setAddedRanges(List<RangeDTO> addedRanges) {
        this.addedRanges = addedRanges;
    }

    @JsonProperty("removedRanges")
    public List<RangeDTO> getRemovedRanges() {
        return removedRanges;
    }

    @JsonProperty("removedRanges")
    public void setRemovedRanges(List<RangeDTO> removedRanges) {
        this.removedRanges = removedRanges;
    }

    @JsonProperty("syntaxMarkup")
    public List<TextMarkupDTO> getSyntaxMarkup() {
        return syntaxMarkup;
    }

    @JsonProperty("syntaxMarkup")
    public void setSyntaxMarkup(List<TextMarkupDTO> syntaxMarkup) {
        this.syntaxMarkup = syntaxMarkup;
    }

    @JsonProperty("diffToOldDocument")
    public List<RangeMappingDTO> getDiffToOldDocument() {
        return diffToOldDocument;
    }

    @JsonProperty("diffToOldDocument")
    public void setDiffToOldDocument(List<RangeMappingDTO> diffToOldDocument) {
        this.diffToOldDocument = diffToOldDocument;
    }

    @JsonProperty("diffToNewDocument")
    public List<RangeMappingDTO> getDiffToNewDocument() {
        return diffToNewDocument;
    }

    @JsonProperty("diffToNewDocument")
    public void setDiffToNewDocument(List<RangeMappingDTO> diffToNewDocument) {
        this.diffToNewDocument = diffToNewDocument;
    }

    @JsonProperty("oldLineNumbers")
    public List<Integer> getOldLineNumbers() {
        return oldLineNumbers;
    }

    @JsonProperty("oldLineNumbers")
    public void setOldLineNumbers(List<Integer> oldLineNumbers) {
        this.oldLineNumbers = oldLineNumbers;
    }

    @JsonProperty("newLineNumbers")
    public List<Integer> getNewLineNumbers() {
        return newLineNumbers;
    }

    @JsonProperty("newLineNumbers")
    public void setNewLineNumbers(List<Integer> newLineNumbers) {
        this.newLineNumbers = newLineNumbers;
    }

    @JsonProperty("annotation")
    public List<FileAnnotationSectionDTO> getAnnotation() {
        return annotation;
    }

    @JsonProperty("annotation")
    public void setAnnotation(List<FileAnnotationSectionDTO> annotation) {
        this.annotation = annotation;
    }

    @JsonProperty("hasUnrelatedChanges")
    public boolean isHasUnrelatedChanges() {
        return hasUnrelatedChanges;
    }

    @JsonProperty("hasUnrelatedChanges")
    public void setHasUnrelatedChanges(boolean hasUnrelatedChanges) {
        this.hasUnrelatedChanges = hasUnrelatedChanges;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (this.isIdentical ? 1 : 0);
        hash = 89 * hash + Objects.hashCode(this.text);
        hash = 89 * hash + Objects.hashCode(this.oldFile);
        hash = 89 * hash + Objects.hashCode(this.newFile);
        hash = 89 * hash + Objects.hashCode(this.contentType);
        hash = 89 * hash + Objects.hashCode(this.addedLines);
        hash = 89 * hash + Objects.hashCode(this.removedLines);
        hash = 89 * hash + Objects.hashCode(this.modifiedLines);
        hash = 89 * hash + Objects.hashCode(this.collapsedLines);
        hash = 89 * hash + Objects.hashCode(this.addedRanges);
        hash = 89 * hash + Objects.hashCode(this.removedRanges);
        hash = 89 * hash + Objects.hashCode(this.syntaxMarkup);
        hash = 89 * hash + Objects.hashCode(this.diffToOldDocument);
        hash = 89 * hash + Objects.hashCode(this.diffToNewDocument);
        hash = 89 * hash + Objects.hashCode(this.oldLineNumbers);
        hash = 89 * hash + Objects.hashCode(this.newLineNumbers);
        hash = 89 * hash + Objects.hashCode(this.annotation);
        hash = 89 * hash + (this.hasUnrelatedChanges ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileInlineDiffResponse other = (FileInlineDiffResponse) obj;
        if (this.isIdentical != other.isIdentical) {
            return false;
        }
        if (this.hasUnrelatedChanges != other.hasUnrelatedChanges) {
            return false;
        }
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (!Objects.equals(this.oldFile, other.oldFile)) {
            return false;
        }
        if (!Objects.equals(this.newFile, other.newFile)) {
            return false;
        }
        if (!Objects.equals(this.contentType, other.contentType)) {
            return false;
        }
        if (!Objects.equals(this.addedLines, other.addedLines)) {
            return false;
        }
        if (!Objects.equals(this.removedLines, other.removedLines)) {
            return false;
        }
        if (!Objects.equals(this.modifiedLines, other.modifiedLines)) {
            return false;
        }
        if (!Objects.equals(this.collapsedLines, other.collapsedLines)) {
            return false;
        }
        if (!Objects.equals(this.addedRanges, other.addedRanges)) {
            return false;
        }
        if (!Objects.equals(this.removedRanges, other.removedRanges)) {
            return false;
        }
        if (!Objects.equals(this.syntaxMarkup, other.syntaxMarkup)) {
            return false;
        }
        if (!Objects.equals(this.diffToOldDocument, other.diffToOldDocument)) {
            return false;
        }
        if (!Objects.equals(this.diffToNewDocument, other.diffToNewDocument)) {
            return false;
        }
        if (!Objects.equals(this.oldLineNumbers, other.oldLineNumbers)) {
            return false;
        }
        if (!Objects.equals(this.newLineNumbers, other.newLineNumbers)) {
            return false;
        }
        if (!Objects.equals(this.annotation, other.annotation)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FileInlineDiffResponse{" + "isIdentical=" + isIdentical + ", text=" + text + ", oldFile=" + oldFile + ", newFile=" + newFile + ", contentType=" + contentType + ", addedLines=" + addedLines + ", removedLines=" + removedLines + ", modifiedLines=" + modifiedLines + ", collapsedLines=" + collapsedLines + ", addedRanges=" + addedRanges + ", removedRanges=" + removedRanges + ", syntaxMarkup=" + syntaxMarkup + ", diffToOldDocument=" + diffToOldDocument + ", diffToNewDocument=" + diffToNewDocument + ", oldLineNumbers=" + oldLineNumbers + ", newLineNumbers=" + newLineNumbers + ", annotation=" + annotation + ", hasUnrelatedChanges=" + hasUnrelatedChanges + '}';
    }
}
