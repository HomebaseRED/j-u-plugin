/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "base",
    "oldState",
    "newState"
})
public class ReviewStateChangedFeedEventBean implements Serializable, UpsourceDTO {
    
    @JsonProperty("base")
    private FeedEventBean base;
    @JsonProperty("oldState")
    private long oldState;
    @JsonProperty("newState")
    private long newState;

    public ReviewStateChangedFeedEventBean() {
    }

    public ReviewStateChangedFeedEventBean(FeedEventBean base, long oldState, long newState) {
        this.base = base;
        this.oldState = oldState;
        this.newState = newState;
    }

    @JsonProperty("base")
    public FeedEventBean getBase() {
        return base;
    }

    @JsonProperty("base")
    public void setBase(FeedEventBean base) {
        this.base = base;
    }
    
    @JsonProperty("oldState")
    public long getOldState() {
        return oldState;
    }
    
    @JsonProperty("oldState")
    public void setOldState(long oldState) {
        this.oldState = oldState;
    }

    @JsonProperty("newState")
    public long getNewState() {
        return newState;
    }

    @JsonProperty("newState")
    public void setNewState(long newState) {
        this.newState = newState;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.base);
        hash = 67 * hash + (int) (this.oldState ^ (this.oldState >>> 32));
        hash = 67 * hash + (int) (this.newState ^ (this.newState >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReviewStateChangedFeedEventBean other = (ReviewStateChangedFeedEventBean) obj;
        if (this.oldState != other.oldState) {
            return false;
        }
        if (this.newState != other.newState) {
            return false;
        }
        if (!Objects.equals(this.base, other.base)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ReviewStateChangedFeedEventBean{" + "base=" + base + ", oldState=" + oldState + ", newState=" + newState + '}';
    }
    
    
}