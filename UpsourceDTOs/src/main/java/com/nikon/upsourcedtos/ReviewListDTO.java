
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonTypeName("reviews")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "reviews",
    "hasMore",
    "totalCount"
})
public class ReviewListDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("reviews")
    private List<ReviewDescriptorDTO> reviews = null;
    @JsonProperty("hasMore")
    private boolean hasMore;
    @JsonProperty("totalCount")
    private long totalCount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 6000753385296923503L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ReviewListDTO() {
    }

    /**
     * 
     * @param hasMore
     * @param reviews
     * @param totalCount
     */
    public ReviewListDTO(List<ReviewDescriptorDTO> reviews, boolean hasMore, long totalCount) {
        super();
        this.reviews = reviews;
        this.hasMore = hasMore;
        this.totalCount = totalCount;
    }

    @JsonProperty("reviews")
    public List<ReviewDescriptorDTO> getReviews() {
        return reviews;
    }

    @JsonProperty("reviews")
    public void setReviews(List<ReviewDescriptorDTO> reviews) {
        this.reviews = reviews;
    }

    @JsonProperty("hasMore")
    public boolean isHasMore() {
        return hasMore;
    }

    @JsonProperty("hasMore")
    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }

    @JsonProperty("totalCount")
    public long getTotalCount() {
        return totalCount;
    }

    @JsonProperty("totalCount")
    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(reviews).append(hasMore).append(totalCount).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ReviewListDTO) == false) {
            return false;
        }
        ReviewListDTO rhs = ((ReviewListDTO) other);
        return new EqualsBuilder().append(reviews, rhs.reviews).append(hasMore, rhs.hasMore).append(totalCount, rhs.totalCount).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
