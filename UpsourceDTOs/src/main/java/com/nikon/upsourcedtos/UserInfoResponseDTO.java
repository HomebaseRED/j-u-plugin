/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserInfoResponseDTO implements Serializable, UpsourceDTO {
    
    private List<FullUserInfoDTO> infos = null;
    
    public UserInfoResponseDTO(){}

    public UserInfoResponseDTO(List<FullUserInfoDTO> infos) {
        this.infos = infos;
    }

    public List<FullUserInfoDTO> getInfos() {
        return infos;
    }

    public void setInfos(List<FullUserInfoDTO> infos) {
        this.infos = infos;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.infos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserInfoResponseDTO other = (UserInfoResponseDTO) obj;
        if (!Objects.equals(this.infos, other.infos)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserInfoResponseDTO{" + "infos=" + infos + '}';
    }
    
    
}
