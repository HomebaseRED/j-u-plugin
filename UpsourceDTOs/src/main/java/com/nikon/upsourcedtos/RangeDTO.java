/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "startOffset",
    "endOffset"
})
public class RangeDTO implements Serializable, Comparable, UpsourceDTO {

    @JsonProperty("startOffset")
    private int startOffset;
    @JsonProperty("endOffset")
    private int endOffset;

    public RangeDTO() {
    }

    public RangeDTO(int startOffset, int endOffset) {
        this.startOffset = startOffset;
        this.endOffset = endOffset;
    }

    @JsonProperty("startOffset")
    public int getStartOffset() {
        return startOffset;
    }

    @JsonProperty("startOffset")
    public void setStartOffset(int startOffset) {
        this.startOffset = startOffset;
    }

    @JsonProperty("endOffset")
    public int getEndOffset() {
        return endOffset;
    }

    @JsonProperty("endOffset")
    public void setEndOffset(int endOffset) {
        this.endOffset = endOffset;
    }

    public int size(){
        return this.endOffset - this.startOffset;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + this.startOffset;
        hash = 19 * hash + this.endOffset;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RangeDTO other = (RangeDTO) obj;
        if (this.startOffset != other.startOffset) {
            return false;
        }
        if (this.endOffset != other.endOffset) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RangeDTO{" + "startOffset=" + startOffset + ", endOffset=" + endOffset + '}';
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof RangeDTO){
            RangeDTO other = (RangeDTO) o;
            if(this.equals(other)){
                return 0;
            }
            int startOffsetDiff = this.startOffset - other.startOffset;
            return startOffsetDiff;
        }
        return -1;
    }
    
}
