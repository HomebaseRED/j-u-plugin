
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonTypeName("discussionCounter")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "counter",
    "hasUnresolved"
})
public class SimpleDiscussionCounterDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("counter")
    private long counter;
    @JsonProperty("hasUnresolved")
    private boolean hasUnresolved;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -9207898928170524414L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public SimpleDiscussionCounterDTO() {
    }

    /**
     * 
     * @param hasUnresolved
     * @param counter
     */
    public SimpleDiscussionCounterDTO(long counter, boolean hasUnresolved) {
        super();
        this.counter = counter;
        this.hasUnresolved = hasUnresolved;
    }

    @JsonProperty("counter")
    public long getCounter() {
        return counter;
    }

    @JsonProperty("counter")
    public void setCounter(long counter) {
        this.counter = counter;
    }

    @JsonProperty("hasUnresolved")
    public boolean isHasUnresolved() {
        return hasUnresolved;
    }

    @JsonProperty("hasUnresolved")
    public void setHasUnresolved(boolean hasUnresolved) {
        this.hasUnresolved = hasUnresolved;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(counter).append(hasUnresolved).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SimpleDiscussionCounterDTO) == false) {
            return false;
        }
        SimpleDiscussionCounterDTO rhs = ((SimpleDiscussionCounterDTO) other);
        return new EqualsBuilder().append(counter, rhs.counter).append(hasUnresolved, rhs.hasUnresolved).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
