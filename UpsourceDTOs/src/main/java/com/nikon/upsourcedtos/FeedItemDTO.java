
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "feedItemId",
    "projectId",
    "discussion",
    "addedRevisions",
    "removedRevisions",
    "newParticipantInReview",
    "removedParticipantFromReview",
    "participantStateChanged",
    "createdReview",
    "modifiedReview",
    "removedReview",
    "reviewStateChanged",
    "branchTrackingStopped",
    "pullRequest",
    "date",
    "actorId",
    "squashedToRevision"
})
public class FeedItemDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("feedItemId")
    private String feedItemId;
    @JsonProperty("projectId")
    private String projectId;
    @JsonProperty("discussion")
    private DiscussionInFeedDTO discussion;
    @JsonProperty("addedRevisions")
    private List<RevisionInfoDTO> addedRevisions = null;
    @JsonProperty("removedRevisions")
    private List<RevisionInfoDTO> removedRevisions = null;
    @JsonProperty("newParticipantInReview")
    private List<ParticipantInReviewDTO> newParticipantInReview = null;
    @JsonProperty("removedParticipantFromReview")
    private List<String> removedParticipantFromReview = null;
    @JsonProperty("participantStateChanged")
    private ParticipantStateChangedDTO participantStateChanged;
    @JsonProperty("createdReview")
    private ShortReviewInfoDTO createdReview;
    @JsonProperty("modifiedReview")
    private ShortReviewInfoDTO modifiedReview;
    @JsonProperty("removedReview")
    private ReviewIdDTO removedReview;
    @JsonProperty("reviewStateChanged")
    private ReviewStateChangedDTO reviewStateChanged;
    @JsonProperty("branchTrackingStopped")
    private String branchTrackingStopped;
    @JsonProperty("pullRequest")
    private String pullRequest;
    @JsonProperty("date")
    private long date;
    @JsonProperty("actorId")
    private String actorId;
    @JsonProperty("squashedToRevision")
    private RevisionInfoDTO squashedToRevision;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 8301990862616512654L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public FeedItemDTO() {
    }

    /**
     * 
     * @param addedRevisions
     * @param removedRevisions
     * @param participantInReview
     * @param discussion
     * @param reviewStateChanged
     * @param actorId
     * @param projectId
     * @param createdReview
     * @param removedParticipantFromReview
     * @param participantStateChanged
     * @param modifiedReview
     * @param date
     * @param removedReview
     * @param feedItemId
     * @param branchTrackingStopped
     * @param pullrequest
     */
    public FeedItemDTO(String feedItemId, 
            String projectId, 
            DiscussionInFeedDTO discussion,
            long date,
            String actorId,
            List<RevisionInfoDTO> addedRevisions,
            List<RevisionInfoDTO> removedRevisions,
            List<ParticipantInReviewDTO> participantInReview,
            List<String> removedParticipantFromReview,
            ShortReviewInfoDTO createdReview,
            ShortReviewInfoDTO modifiedReview,
            ReviewIdDTO removedReview,
            ReviewStateChangedDTO reviewStateChanged,
            String branchTrackingStopped,
            String pullrequest,
            ParticipantStateChangedDTO participantStateChanged,
            RevisionInfoDTO squashedToRevision) {
        super();
        this.feedItemId = feedItemId;
        this.projectId = projectId;
        this.discussion = discussion;
        this.addedRevisions = addedRevisions;
        this.removedRevisions = removedRevisions;
        this.newParticipantInReview = participantInReview;
        this.removedParticipantFromReview = removedParticipantFromReview;
        this.modifiedReview = modifiedReview;
        this.reviewStateChanged = reviewStateChanged;
        this.participantStateChanged = participantStateChanged;
        this.createdReview = createdReview;
        this.removedReview = removedReview;
        this.branchTrackingStopped = branchTrackingStopped;
        this.pullRequest = pullrequest;
        this.date = date;
        this.actorId = actorId;
        this.squashedToRevision = squashedToRevision;
    }

    @JsonProperty("feedItemId")
    public String getFeedItemId() {
        return feedItemId;
    }

    @JsonProperty("feedItemId")
    public void setFeedItemId(String feedItemId) {
        this.feedItemId = feedItemId;
    }

    @JsonProperty("projectId")
    public String getProjectId() {
        return projectId;
    }

    @JsonProperty("projectId")
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @JsonProperty("discussion")
    public DiscussionInFeedDTO getDiscussion() {
        return discussion;
    }

    @JsonProperty("discussion")
    public void setDiscussion(DiscussionInFeedDTO discussion) {
        this.discussion = discussion;
    }

    @JsonProperty("date")
    public long getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(long date) {
        this.date = date;
    }

    @JsonProperty("actorId")
    public String getActorId() {
        return actorId;
    }

    @JsonProperty("actorId")
    public void setActorId(String actorId) {
        this.actorId = actorId;
    }

    @JsonProperty("addedRevisions")
    public List<RevisionInfoDTO> getAddedRevisions() {
        return addedRevisions;
    }

    @JsonProperty("addedRevisions")
    public void setAddedRevisions(List<RevisionInfoDTO> addedRevisions) {
        this.addedRevisions = addedRevisions;
    }

    @JsonProperty("removedRevisions")
    public List<RevisionInfoDTO> getRemovedRevisions() {
        return removedRevisions;
    }

    @JsonProperty("removedRevisions")
    public void setRemovedRevisions(List<RevisionInfoDTO> removedRevisions) {
        this.removedRevisions = removedRevisions;
    }

    @JsonProperty("newParticipantInReview")
    public List<ParticipantInReviewDTO> getNewParticipantInReview() {
        return newParticipantInReview;
    }

    @JsonProperty("newParticipantInReview")
    public void setNewParticipantInReview(List<ParticipantInReviewDTO> newParticipantInReview) {
        this.newParticipantInReview = newParticipantInReview;
    }

    @JsonProperty("removedParticipantFromReview")
    public List<String> getRemovedParticipantFromReview() {
        return removedParticipantFromReview;
    }

    @JsonProperty("removedParticipantFromReview")
    public void setRemovedParticipantFromReview(List<String> removedParticipantFromReview) {
        this.removedParticipantFromReview = removedParticipantFromReview;
    }

    @JsonProperty("participantStateChanged")
    public ParticipantStateChangedDTO getParticipantStateChanged() {
        return participantStateChanged;
    }

    @JsonProperty("participantStateChanged")
    public void setParticipantStateChanged(ParticipantStateChangedDTO participantStateChanged) {
        this.participantStateChanged = participantStateChanged;
    }

    @JsonProperty("createdReview")
    public ShortReviewInfoDTO getCreatedReview() {
        return createdReview;
    }

    @JsonProperty("createdReview")
    public void setCreatedReview(ShortReviewInfoDTO createdReview) {
        this.createdReview = createdReview;
    }

    @JsonProperty("modifiedReview")
    public ShortReviewInfoDTO getModifiedReview() {
        return modifiedReview;
    }

    @JsonProperty("modifiedReview")
    public void setModifiedReview(ShortReviewInfoDTO modifiedReview) {
        this.modifiedReview = modifiedReview;
    }

    @JsonProperty("removedReview")
    public ReviewIdDTO getRemovedReview() {
        return removedReview;
    }

    @JsonProperty("removedReview")
    public void setRemovedReview(ReviewIdDTO removedReview) {
        this.removedReview = removedReview;
    }

    @JsonProperty("reviewStateChanged")
    public ReviewStateChangedDTO getReviewStateChanged() {
        return reviewStateChanged;
    }

    @JsonProperty("reviewStateChanged")
    public void setReviewStateChanged(ReviewStateChangedDTO reviewStateChanged) {
        this.reviewStateChanged = reviewStateChanged;
    }

    @JsonProperty("branchTrackingStopped")
    public String getBranchTrackingStopped() {
        return branchTrackingStopped;
    }

    @JsonProperty("branchTrackingStopped")
    public void setBranchTrackingStopped(String branchTrackingStopped) {
        this.branchTrackingStopped = branchTrackingStopped;
    }

    @JsonProperty("pullRequest")
    public String getPullRequest() {
        return pullRequest;
    }

    @JsonProperty("pullRequest")
    public void setPullRequest(String pullRequest) {
        this.pullRequest = pullRequest;
    }

    @JsonProperty("squashedToRevision")
    public RevisionInfoDTO getSquashedToRevision() {
        return squashedToRevision;
    }

    @JsonProperty("squashedToRevision")
    public void setSquashedToRevision(RevisionInfoDTO squashedToRevision) {
        this.squashedToRevision = squashedToRevision;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(feedItemId)
                .append(projectId)
                .append(discussion)
                .append(addedRevisions)
                .append(removedRevisions)
                .append(newParticipantInReview)
                .append(removedParticipantFromReview)
                .append(participantStateChanged)
                .append(createdReview)
                .append(modifiedReview)
                .append(removedReview)
                .append(reviewStateChanged)
                .append(branchTrackingStopped)
                .append(pullRequest)
                .append(date)
                .append(actorId)
                .append(squashedToRevision)
                .append(additionalProperties)
                .toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof FeedItemDTO) == false) {
            return false;
        }
        FeedItemDTO rhs = ((FeedItemDTO) other);
        return new EqualsBuilder()
                .append(feedItemId, rhs.feedItemId)
                .append(projectId, rhs.projectId)
                .append(discussion, rhs.discussion)
                .append(addedRevisions, rhs.addedRevisions)
                .append(removedRevisions, rhs.removedRevisions)
                .append(newParticipantInReview, rhs.newParticipantInReview)
                .append(removedParticipantFromReview, rhs.removedParticipantFromReview)
                .append(participantStateChanged, rhs.participantStateChanged)
                .append(createdReview, rhs.createdReview)
                .append(modifiedReview, rhs.modifiedReview)
                .append(removedReview, rhs.removedReview)
                .append(reviewStateChanged, rhs.reviewStateChanged)
                .append(branchTrackingStopped, rhs.branchTrackingStopped)
                .append(pullRequest, rhs.pullRequest)
                .append(date, rhs.date)
                .append(actorId, rhs.actorId)
                .append(squashedToRevision, rhs.squashedToRevision)
                .append(additionalProperties, rhs.additionalProperties)
                .isEquals();
    }

}
