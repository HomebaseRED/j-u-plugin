/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "startLine",
    "lineCount",
    "revision",
    "filePath"
})
public class FileAnnotationSectionDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("startLine")
    private int startLine;
    @JsonProperty("lineCount")
    private int lineCount;
    @JsonProperty("revision")
    private RevisionInfoDTO revision;
    @JsonProperty("filePath")
    private String filePath;

    public FileAnnotationSectionDTO() {
    }

    public FileAnnotationSectionDTO(int startLine, int lineCount, RevisionInfoDTO revision, String filePath) {
        this.startLine = startLine;
        this.lineCount = lineCount;
        this.revision = revision;
        this.filePath = filePath;
    }

    @JsonProperty("startLine")
    public int getStartLine() {
        return startLine;
    }

    @JsonProperty("startLine")
    public void setStartLine(int startLine) {
        this.startLine = startLine;
    }

    @JsonProperty("lineCount")
    public int getLineCount() {
        return lineCount;
    }

    @JsonProperty("lineCount")
    public void setLineCount(int lineCount) {
        this.lineCount = lineCount;
    }

    @JsonProperty("revision")
    public RevisionInfoDTO getRevision() {
        return revision;
    }

    @JsonProperty("revision")
    public void setRevision(RevisionInfoDTO revision) {
        this.revision = revision;
    }

    @JsonProperty("filePath")
    public String getFilePath() {
        return filePath;
    }

    @JsonProperty("filePath")
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + this.startLine;
        hash = 47 * hash + this.lineCount;
        hash = 47 * hash + Objects.hashCode(this.revision);
        hash = 47 * hash + Objects.hashCode(this.filePath);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileAnnotationSectionDTO other = (FileAnnotationSectionDTO) obj;
        if (this.startLine != other.startLine) {
            return false;
        }
        if (this.lineCount != other.lineCount) {
            return false;
        }
        if (!Objects.equals(this.filePath, other.filePath)) {
            return false;
        }
        if (!Objects.equals(this.revision, other.revision)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FileAnnotationSectionDTO{" + "startLine=" + startLine + ", lineCount=" + lineCount + ", revision=" + revision + ", filePath=" + filePath + '}';
    }
    
    
}
