/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "userId",
    "userName",
    "userEmail"
})
public class UserIdBean implements Serializable, UpsourceDTO{
    
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("userName")
    private String userName;
    @JsonProperty("userEmail")
    private String userEmail;

    public UserIdBean() {
    }

    public UserIdBean(String userId, String userName, String userEmail) {
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
    }

    @JsonProperty("")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("")
    public String getUserName() {
        return userName;
    }

    @JsonProperty("")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("")
    public String getUserEmail() {
        return userEmail;
    }

    @JsonProperty("")
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.userId);
        hash = 53 * hash + Objects.hashCode(this.userName);
        hash = 53 * hash + Objects.hashCode(this.userEmail);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserIdBean other = (UserIdBean) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.userName, other.userName)) {
            return false;
        }
        if (!Objects.equals(this.userEmail, other.userEmail)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserIdBean{" + "userId=" + userId + ", userName=" + userName + ", userEmail=" + userEmail + '}';
    }

    
}
