/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

//for some obscure reason the newer fasterxml annotations do not work, making the class unrecognisable to an objectmapper.
//HOWEVER, in other classes in this package, said annotations work just fine.
//Not gonna waste my time on this right now, but should there ever be json parsing errors, this is a good place to start investigating.

//import com.fasterxml.jackson.annotation.JsonProperty;
//import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "revision",
    "graph",
    "headHash",
    "query"    
})
public class RevisionDescriptorListDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("revision")
    private List<RevisionInfoDTO> revision = null;
    @JsonProperty("graph")
    private RevisionListGraphDTO graph;
    @JsonProperty("headHash")
    private String headHash;
    @JsonProperty("query")
    private String query;
    
    public RevisionDescriptorListDTO(){}

    public RevisionDescriptorListDTO(List<RevisionInfoDTO> revision, RevisionListGraphDTO graph, String headHash, String query) {
        this.revision = revision;
        this.graph = graph;
        this.headHash = headHash;
        this.query = query;
    }

    @JsonProperty("revision")
    public List<RevisionInfoDTO> getRevisions() {
        return revision;
    }

    @JsonProperty("revision")
    public void setRevisions(List<RevisionInfoDTO> revision) {
        this.revision = revision;
    }

    @JsonProperty("graph")
    public RevisionListGraphDTO getGraph() {
        return graph;
    }

    @JsonProperty("graph")
    public void setGraph(RevisionListGraphDTO graph) {
        this.graph = graph;
    }

    @JsonProperty("headHash")
    public String getHeadHash() {
        return headHash;
    }

    @JsonProperty("headHash")
    public void setHeadHash(String headHash) {
        this.headHash = headHash;
    }

    @JsonProperty("query")
    public String getQuery() {
        return query;
    }

    @JsonProperty("query")
    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.revision);
        hash = 97 * hash + Objects.hashCode(this.graph);
        hash = 97 * hash + Objects.hashCode(this.headHash);
        hash = 97 * hash + Objects.hashCode(this.query);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RevisionDescriptorListDTO other = (RevisionDescriptorListDTO) obj;
        if (!Objects.equals(this.headHash, other.headHash)) {
            return false;
        }
        if (!Objects.equals(this.query, other.query)) {
            return false;
        }
        if (!Objects.equals(this.revision, other.revision)) {
            return false;
        }
        if (!Objects.equals(this.graph, other.graph)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RevisionDescriptorListDTO{" + "revisions=" + revision + ", graph=" + graph + ", headHash=" + headHash + ", query=" + query + '}';
    }
    
    
}
