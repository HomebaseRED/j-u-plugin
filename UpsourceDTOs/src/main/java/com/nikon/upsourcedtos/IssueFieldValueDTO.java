
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonTypeName("value")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "valueId",
    "name",
    "html"
})
public class IssueFieldValueDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("valueId")
    private String valueId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("html")
    private String html;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -3491430560742408468L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public IssueFieldValueDTO() {
    }

    /**
     * 
     * @param name
     * @param valueId
     * @param html
     */
    public IssueFieldValueDTO(String valueId, String name, String html) {
        super();
        this.valueId = valueId;
        this.name = name;
        this.html = html;
    }

    @JsonProperty("valueId")
    public String getValueId() {
        return valueId;
    }

    @JsonProperty("valueId")
    public void setValueId(String valueId) {
        this.valueId = valueId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("html")
    public String getHtml() {
        return html;
    }

    @JsonProperty("html")
    public void setHtml(String html) {
        this.html = html;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(valueId).append(name).append(html).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof IssueFieldValueDTO) == false) {
            return false;
        }
        IssueFieldValueDTO rhs = ((IssueFieldValueDTO) other);
        return new EqualsBuilder().append(valueId, rhs.valueId).append(name, rhs.name).append(html, rhs.html).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
