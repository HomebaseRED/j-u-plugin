/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "from",
    "to"
})
public class RangeMappingDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("from")
    private RangeDTO from;
    @JsonProperty("to")
    private RangeDTO to;

    public RangeMappingDTO() {
    }

    public RangeMappingDTO(RangeDTO from, RangeDTO to) {
        this.from = from;
        this.to = to;
    }

    @JsonProperty("from")
    public RangeDTO getFrom() {
        return from;
    }

    @JsonProperty("from")
    public void setFrom(RangeDTO from) {
        this.from = from;
    }

    @JsonProperty("to")
    public RangeDTO getTo() {
        return to;
    }

    @JsonProperty("to")
    public void setTo(RangeDTO to) {
        this.to = to;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.from);
        hash = 97 * hash + Objects.hashCode(this.to);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RangeMappingDTO other = (RangeMappingDTO) obj;
        if (!Objects.equals(this.from, other.from)) {
            return false;
        }
        if (!Objects.equals(this.to, other.to)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RangeMappingDTO{" + "from=" + from + ", to=" + to + '}';
    }
    
    
}
