/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "position",
    "color"    
})
public class RevisionListGraphNodeDTO implements Serializable, UpsourceDTO{
 
    @JsonProperty("position")
    private int position;
    @JsonProperty("nodes")
    private int color;
    
    public RevisionListGraphNodeDTO(){}

    public RevisionListGraphNodeDTO(int position, int color) {
        this.position = position;
        this.color = color;
    }

    @JsonProperty("position")
    public int getPosition() {
        return position;
    }

    @JsonProperty("position")
    public void setPosition(int position) {
        this.position = position;
    }

    @JsonProperty("color")
    public int getColor() {
        return color;
    }

    @JsonProperty("color")
    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.position;
        hash = 97 * hash + this.color;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RevisionListGraphNodeDTO other = (RevisionListGraphNodeDTO) obj;
        if (this.position != other.position) {
            return false;
        }
        if (this.color != other.color) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RevisionListGraphNodeDTO{" + "position=" + position + ", color=" + color + '}';
    }     
}
