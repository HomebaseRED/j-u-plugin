/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "file",
    "addedLines",
    "removedLines"
})
public class FileDiffSummaryDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("file")
    private FileInRevisionDTO file;
    @JsonProperty("addedLines")
    private int addedLines;
    @JsonProperty("removedLines")
    private int removedLines;

    public FileDiffSummaryDTO() {
    }

    public FileDiffSummaryDTO(FileInRevisionDTO file, int addedLines, int removedLines) {
        this.file = file;
        this.addedLines = addedLines;
        this.removedLines = removedLines;
    }

    @JsonProperty("file")
    public FileInRevisionDTO getFile() {
        return file;
    }

    @JsonProperty("file")
    public void setFile(FileInRevisionDTO file) {
        this.file = file;
    }

    @JsonProperty("addedLines")
    public int getAddedLines() {
        return addedLines;
    }

    @JsonProperty("addedLines")
    public void setAddedLines(int addedLines) {
        this.addedLines = addedLines;
    }

    @JsonProperty("removedLines")
    public int getRemovedLines() {
        return removedLines;
    }

    @JsonProperty("removedLines")
    public void setRemovedLines(int removedLines) {
        this.removedLines = removedLines;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.file);
        hash = 59 * hash + this.addedLines;
        hash = 59 * hash + this.removedLines;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileDiffSummaryDTO other = (FileDiffSummaryDTO) obj;
        if (this.addedLines != other.addedLines) {
            return false;
        }
        if (this.removedLines != other.removedLines) {
            return false;
        }
        if (!Objects.equals(this.file, other.file)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FileDiffSummaryDTO{" + "file=" + file + ", addedLines=" + addedLines + ", removedLines=" + removedLines + '}';
    }
    
    public String hello(){
        return "hello";
    }
}
