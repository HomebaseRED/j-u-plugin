/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RevisionReviewInfoDTO implements Serializable, UpsourceDTO{
    
   @JsonProperty("reviewInfo")
    private ShortReviewInfoDTO reviewInfo;
    
    public RevisionReviewInfoDTO(){}
    
    public RevisionReviewInfoDTO(ShortReviewInfoDTO reviewInfo){
        this.reviewInfo = reviewInfo;
    }

    @JsonProperty("reviewInfo")
    public ShortReviewInfoDTO getReviewInfo() {
        return reviewInfo;
    }

    @JsonProperty("reviewInfo")
    public void setReviewInfo(ShortReviewInfoDTO reviewInfo) {
        this.reviewInfo = reviewInfo;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.reviewInfo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RevisionReviewInfoDTO other = (RevisionReviewInfoDTO) obj;
        if (!Objects.equals(this.reviewInfo, other.reviewInfo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RevisionReviewInfoDTO{" + "reviewInfo=" + reviewInfo + '}';
    }
}
