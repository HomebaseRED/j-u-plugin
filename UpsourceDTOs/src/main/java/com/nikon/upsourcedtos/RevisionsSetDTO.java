/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({    
        "revisions"        
})
public class RevisionsSetDTO {
    
    @JsonProperty("revisions")
    private List<String> revisions = null;

    public RevisionsSetDTO() {
    }

    public RevisionsSetDTO(List<String> revisions) {
        this.revisions = revisions;
    }

    @JsonProperty("revisions")
    public List<String> getRevisions() {
        return revisions;
    }

    @JsonProperty("revisions")
    public void setRevisions(List<String> revisions) {
        this.revisions = revisions;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.revisions);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RevisionsSetDTO other = (RevisionsSetDTO) obj;
        if (!Objects.equals(this.revisions, other.revisions)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RevisionsSetDTO{" + "revisions=" + revisions + '}';
    }
    
    
}
