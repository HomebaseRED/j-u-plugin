/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FullUserInfoDTO implements Serializable, UpsourceDTO {
    
    private String userId;
    private String name;
    private boolean isResolved;
    private boolean isMe;
    private String avatarUrl;
    private String profileUrl;
    private String email;
    private String login;
    
    public FullUserInfoDTO(){}

    public FullUserInfoDTO(String userId, String name, boolean isResolved, boolean isMe, String avatarUrl, String profileUrl, String email, String login) {
        this.userId = userId;
        this.name = name;
        this.isResolved = isResolved;
        this.isMe = isMe;
        this.avatarUrl = avatarUrl;
        this.profileUrl = profileUrl;
        this.email = email;
        this.login = login;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIsResolved() {
        return isResolved;
    }

    public void setIsResolved(boolean isResolved) {
        this.isResolved = isResolved;
    }

    public boolean isIsMe() {
        return isMe;
    }

    public void setIsMe(boolean isMe) {
        this.isMe = isMe;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.userId);
        hash = 37 * hash + Objects.hashCode(this.name);
        hash = 37 * hash + (this.isResolved ? 1 : 0);
        hash = 37 * hash + (this.isMe ? 1 : 0);
        hash = 37 * hash + Objects.hashCode(this.avatarUrl);
        hash = 37 * hash + Objects.hashCode(this.profileUrl);
        hash = 37 * hash + Objects.hashCode(this.email);
        hash = 37 * hash + Objects.hashCode(this.login);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FullUserInfoDTO other = (FullUserInfoDTO) obj;
        if (this.isResolved != other.isResolved) {
            return false;
        }
        if (this.isMe != other.isMe) {
            return false;
        }
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.avatarUrl, other.avatarUrl)) {
            return false;
        }
        if (!Objects.equals(this.profileUrl, other.profileUrl)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.login, other.login)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FullUserInfoDTO{" + "userId=" + userId + ", name=" + name + ", isResolved=" + isResolved + ", isMe=" + isMe + ", avatarUrl=" + avatarUrl + ", profileUrl=" + profileUrl + ", email=" + email + ", login=" + login + '}';
    }
    
    
    
}
