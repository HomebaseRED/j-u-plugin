
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeName("projectInfo")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "projectName",
    "projectId",
    "headHash",
    "codeReviewIdPattern",
    "externalLinks",
    "issueTrackerConnections",
    "projectModelType",
    "defaultEffectiveCharset",
    "defaultBranch",
    "issueTrackerDetails",
    "isConnectedToGithub",
    "iconUrl",
    "group"
})
public class ProjectInfoDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("projectName")
    private String projectName;
    @JsonProperty("projectId")
    private String projectId;
    @JsonProperty("headHash")
    private String headHash;
    @JsonProperty("codeReviewIdPattern")
    private String codeReviewIdPattern;
    @JsonProperty("externalLinks")
    private List<ExternalLinkDTO> externalLinks = null;
    @JsonProperty("issueTrackerConnections")
    private List<ExternalLinkDTO> issueTrackerConnections = null;
    @JsonProperty("projectModelType")
    private String projectModelType;
    @JsonProperty("defaultEffectiveCharset")
    private String defaultEffectiveCharset;
    @JsonProperty("defaultBranch")
    private String defaultBranch;
    @JsonProperty("issueTrackerDetails")
    private IssueTrackerProjectDetailsDTO issueTrackerDetails;
    @JsonProperty("isConnectedToGithub")
    private boolean isConnectedToGithub;
    @JsonProperty("iconUrl")
    private String iconUrl;
    @JsonProperty("group")
    private ProjectGroupDTO group;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 6109467517581331548L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ProjectInfoDTO() {
    }

    /**
     * 
     * @param issueTrackerConnections
     * @param headHash
     * @param isConnectedToGithub
     * @param codeReviewIdPattern
     * @param externalLinks
     * @param projectModelType
     * @param defaultEffectiveCharset
     * @param projectId
     * @param group
     * @param projectName
     * @param iconUrl
     * @param issueTrackerDetails
     * @param defaultBranch
     */
    public ProjectInfoDTO(String projectName, 
            String projectId, 
            String headHash, 
            String codeReviewIdPattern, 
            List<ExternalLinkDTO> externalLinks,
            List<ExternalLinkDTO> issueTrackerConnections,
            String projectModelType, 
            String defaultEffectiveCharset, 
            String defaultBranch, 
            IssueTrackerProjectDetailsDTO issueTrackerDetails, 
            boolean isConnectedToGithub, 
            String iconUrl, 
            ProjectGroupDTO group) {
        this.projectName = projectName;
        this.projectId = projectId;
        this.headHash = headHash;
        this.codeReviewIdPattern = codeReviewIdPattern;
        this.externalLinks = externalLinks;
        this.issueTrackerConnections = issueTrackerConnections;
        this.projectModelType = projectModelType;
        this.defaultEffectiveCharset = defaultEffectiveCharset;
        this.defaultBranch = defaultBranch;
        this.issueTrackerDetails = issueTrackerDetails;
        this.isConnectedToGithub = isConnectedToGithub;
        this.iconUrl = iconUrl;
        this.group = group;
    }

    @JsonProperty(value = "projectName")
    public String getProjectName() {
        return projectName;
    }

    @JsonProperty("projectName")
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @JsonProperty("projectId")
    public String getProjectId() {
        return projectId;
    }

    @JsonProperty("projectId")
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @JsonProperty("headHash")
    public String getHeadHash() {
        return headHash;
    }

    @JsonProperty("headHash")
    public void setHeadHash(String headHash) {
        this.headHash = headHash;
    }

    @JsonProperty("codeReviewIdPattern")
    public String getCodeReviewIdPattern() {
        return codeReviewIdPattern;
    }

    @JsonProperty("codeReviewIdPattern")
    public void setCodeReviewIdPattern(String codeReviewIdPattern) {
        this.codeReviewIdPattern = codeReviewIdPattern;
    }

    @JsonProperty("externalLinks")
    public List<ExternalLinkDTO> getExternalLinks() {
        return externalLinks;
    }

    @JsonProperty("externalLinks")
    public void setExternalLinks(List<ExternalLinkDTO> externalLinks) {
        this.externalLinks = externalLinks;
    }

    @JsonProperty("issueTrackerConnections")
    public List<ExternalLinkDTO> getIssueTrackerConnections() {
        return issueTrackerConnections;
    }

    @JsonProperty("issueTrackerConnections")
    public void setIssueTrackerConnections(List<ExternalLinkDTO> issueTrackerConnections) {
        this.issueTrackerConnections = issueTrackerConnections;
    }

    @JsonProperty("projectModelType")
    public String getProjectModelType() {
        return projectModelType;
    }

    @JsonProperty("projectModelType")
    public void setProjectModelType(String projectModelType) {
        this.projectModelType = projectModelType;
    }

    @JsonProperty("defaultEffectiveCharset")
    public String getDefaultEffectiveCharset() {
        return defaultEffectiveCharset;
    }

    @JsonProperty("defaultEffectiveCharset")
    public void setDefaultEffectiveCharset(String defaultEffectiveCharset) {
        this.defaultEffectiveCharset = defaultEffectiveCharset;
    }

    @JsonProperty("defaultBranch")
    public String getDefaultBranch() {
        return defaultBranch;
    }

    @JsonProperty("defaultBranch")
    public void setDefaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
    }

    @JsonProperty("issueTrackerDetails")
    public IssueTrackerProjectDetailsDTO getIssueTrackerDetails() {
        return issueTrackerDetails;
    }

    @JsonProperty("issueTrackerDetails")
    public void setIssueTrackerDetails(IssueTrackerProjectDetailsDTO issueTrackerDetails) {
        this.issueTrackerDetails = issueTrackerDetails;
    }

    @JsonProperty("isConnectedToGithub")
    public boolean isIsConnectedToGithub() {
        return isConnectedToGithub;
    }

    @JsonProperty("isConnectedToGithub")
    public void setIsConnectedToGithub(boolean isConnectedToGithub) {
        this.isConnectedToGithub = isConnectedToGithub;
    }

    @JsonProperty("iconUrl")
    public String getIconUrl() {
        return iconUrl;
    }

    @JsonProperty("iconUrl")
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    @JsonProperty("group")
    public ProjectGroupDTO getGroup() {
        return group;
    }

    @JsonProperty("group")
    public void setGroup(ProjectGroupDTO group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(projectName)
                .append(projectId)
                .append(headHash)
                .append(codeReviewIdPattern)
                .append(externalLinks)
                .append(issueTrackerConnections)
                .append(projectModelType)
                .append(defaultEffectiveCharset)
                .append(defaultBranch)
                .append(issueTrackerDetails)
                .append(isConnectedToGithub)
                .append(iconUrl)
                .append(group)
                .append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ProjectInfoDTO) == false) {
            return false;
        }
        ProjectInfoDTO rhs = ((ProjectInfoDTO) other);
        return new EqualsBuilder()
                .append(projectName, rhs.projectName)
                .append(projectId, rhs.projectId)
                .append(headHash, rhs.headHash)
                .append(codeReviewIdPattern, rhs.codeReviewIdPattern)
                .append(externalLinks, rhs.externalLinks)
                .append(issueTrackerConnections, rhs.issueTrackerConnections)
                .append(projectModelType, rhs.projectModelType)
                .append(defaultEffectiveCharset, rhs.defaultEffectiveCharset)
                .append(defaultBranch, rhs.defaultBranch)
                .append(issueTrackerDetails, rhs.issueTrackerDetails)
                .append(isConnectedToGithub, rhs.isConnectedToGithub)
                .append(iconUrl, rhs.iconUrl)
                .append(group, rhs.group)
                .append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
