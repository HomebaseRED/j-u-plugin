/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "width",
    "rows"    
})
public class RevisionListGraphDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("width")
    private int width;
    @JsonProperty("rows")
    private List<RevisionListGraphRowDTO> rows;
    
    public RevisionListGraphDTO(){}

    public RevisionListGraphDTO(int width, List<RevisionListGraphRowDTO> rows) {
        this.width = width;
        this.rows = rows;
    }

    @JsonProperty("width")
    public int getWidth() {
        return width;
    }

    @JsonProperty("width")
    public void setWidth(int width) {
        this.width = width;
    }

    @JsonProperty("rows")
    public List<RevisionListGraphRowDTO> getRows() {
        return rows;
    }

    @JsonProperty("rows")
    public void setRows(List<RevisionListGraphRowDTO> rows) {
        this.rows = rows;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + this.width;
        hash = 47 * hash + Objects.hashCode(this.rows);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RevisionListGraphDTO other = (RevisionListGraphDTO) obj;
        if (this.width != other.width) {
            return false;
        }
        if (!Objects.equals(this.rows, other.rows)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RevisionListGraphDTO{" + "width=" + width + ", rows=" + rows + '}';
    }
    
    
    
}
