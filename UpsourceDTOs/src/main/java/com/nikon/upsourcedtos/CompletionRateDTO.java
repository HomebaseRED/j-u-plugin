
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonTypeName("completionRate")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "completedCount",
    "reviewersCount"
})
public class CompletionRateDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("completedCount")
    private long completedCount;
    @JsonProperty("reviewersCount")
    private long reviewersCount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -3381846746010400125L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CompletionRateDTO() {
    }

    /**
     * 
     * @param completedCount
     * @param reviewersCount
     */
    public CompletionRateDTO(long completedCount, long reviewersCount) {
        super();
        this.completedCount = completedCount;
        this.reviewersCount = reviewersCount;
    }

    @JsonProperty("completedCount")
    public long getCompletedCount() {
        return completedCount;
    }

    @JsonProperty("completedCount")
    public void setCompletedCount(long completedCount) {
        this.completedCount = completedCount;
    }

    @JsonProperty("reviewersCount")
    public long getReviewersCount() {
        return reviewersCount;
    }

    @JsonProperty("reviewersCount")
    public void setReviewersCount(long reviewersCount) {
        this.reviewersCount = reviewersCount;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(completedCount).append(reviewersCount).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CompletionRateDTO) == false) {
            return false;
        }
        CompletionRateDTO rhs = ((CompletionRateDTO) other);
        return new EqualsBuilder().append(completedCount, rhs.completedCount).append(reviewersCount, rhs.reviewersCount).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
