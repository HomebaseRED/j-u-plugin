
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonTypeName("issue")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "issueId",
    "issueLink"
})
public class IssueIdDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("issueId")
    private String issueId;
    @JsonProperty("issueLink")
    private String issueLink;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 787526130884876483L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public IssueIdDTO() {
    }

    /**
     * 
     * @param issueId
     */
    public IssueIdDTO(String issueId, String issueLink) {
        super();
        this.issueId = issueId;
        this.issueLink = issueLink;
    }

    @JsonProperty("issueId")
    public String getIssueId() {
        return issueId;
    }

    @JsonProperty("issueId")
    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    @JsonProperty("issueLink")
    public String getIssueLink() {
        return issueLink;
    }

    @JsonProperty("issueLink")
    public void setIssueLink(String issueLink) {
        this.issueLink = issueLink;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(issueId).append(issueLink).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof IssueIdDTO) == false) {
            return false;
        }
        IssueIdDTO rhs = ((IssueIdDTO) other);
        return new EqualsBuilder().append(issueId, rhs.issueId).append(issueLink, rhs.issueLink).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
