/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "diff"
})
public class RevisionsDiffDTO implements Serializable, UpsourceDTO {
    
    @JsonProperty("diff")
    private List<RevisionDiffItemDTO> diff = null;

    public RevisionsDiffDTO() {
    }
    
    public RevisionsDiffDTO(List<RevisionDiffItemDTO> items){
        this.diff = items;
    }

    @JsonProperty("diff")
    public List<RevisionDiffItemDTO> getDiff() {
        return diff;
    }

    @JsonProperty("diff")
    public void setDiff(List<RevisionDiffItemDTO> diff) {
        this.diff = diff;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.diff);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RevisionsDiffDTO other = (RevisionsDiffDTO) obj;
        if (!Objects.equals(this.diff, other.diff)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RevisionsDiffDTO{" + "diff=" + diff + '}';
    }
    
    
}
