/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "reviewId",
    "file"
})
public class FileInReviewDTO implements Serializable, UpsourceDTO {
    
    @JsonProperty("reviewId")
    private ReviewIdDTO reviewId;
    @JsonProperty("file")
    private FileInRevisionDTO file;

    public FileInReviewDTO(ReviewIdDTO reviewId, FileInRevisionDTO file) {
        this.reviewId = reviewId;
        this.file = file;
    }

    public FileInReviewDTO() {
    }

    @JsonProperty("reviewId")
    public ReviewIdDTO getReviewId() {
        return reviewId;
    }

    @JsonProperty("reviewId")
    public void setReviewId(ReviewIdDTO reviewId) {
        this.reviewId = reviewId;
    }

    @JsonProperty("file")
    public FileInRevisionDTO getFile() {
        return file;
    }

    @JsonProperty("file")
    public void setFile(FileInRevisionDTO file) {
        this.file = file;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.reviewId);
        hash = 41 * hash + Objects.hashCode(this.file);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileInReviewDTO other = (FileInReviewDTO) obj;
        if (!Objects.equals(this.reviewId, other.reviewId)) {
            return false;
        }
        if (!Objects.equals(this.file, other.file)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FileInReviewDTO{" + "reviewId=" + reviewId + ", file=" + file + '}';
    }
}
