
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "project"
})
public class ShortProjectInfoListDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("project")
    private List<ShortProjectInfoDTO> project = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 160425366202693764L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ShortProjectInfoListDTO() {
    }

    /**
     * 
     * @param project
     */
    public ShortProjectInfoListDTO(List<ShortProjectInfoDTO> project) {
        super();
        this.project = project;
    }

    @JsonProperty("project")
    public List<ShortProjectInfoDTO> getProject() {
        return project;
    }

    @JsonProperty("project")
    public void setProject(List<ShortProjectInfoDTO> project) {
        this.project = project;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(project).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ShortProjectInfoListDTO) == false) {
            return false;
        }
        ShortProjectInfoListDTO rhs = ((ShortProjectInfoListDTO) other);
        return new EqualsBuilder().append(project, rhs.project).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
