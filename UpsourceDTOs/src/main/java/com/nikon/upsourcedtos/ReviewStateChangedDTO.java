
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "oldState",
    "newState"
})
public class ReviewStateChangedDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("oldState")
    private long oldState;
    @JsonProperty("newState")
    private long newState;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 3726348483248185183L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ReviewStateChangedDTO() {
    }

    /**
     * 
     * @param oldState
     * @param newState
     */
    public ReviewStateChangedDTO(long oldState, long newState) {
        super();
        this.oldState = oldState;
        this.newState = newState;
    }

    @JsonProperty("oldState")
    public long getOldState() {
        return oldState;
    }

    @JsonProperty("oldState")
    public void setOldState(long oldState) {
        this.oldState = oldState;
    }

    @JsonProperty("newState")
    public long getNewState() {
        return newState;
    }

    @JsonProperty("newState")
    public void setNewState(long newState) {
        this.newState = newState;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(oldState).append(newState).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ReviewStateChangedDTO) == false) {
            return false;
        }
        ReviewStateChangedDTO rhs = ((ReviewStateChangedDTO) other);
        return new EqualsBuilder().append(oldState, rhs.oldState).append(newState, rhs.newState).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
