/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.nikon.upsourcedtos.io.UpsourceWebhookResponseDeserializer;
import java.io.Serializable;
import java.util.Objects;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "majorVersion",
    "minorVersion",
    "projectId",
    "dataType",
    "data"
})
@JsonDeserialize(using = UpsourceWebhookResponseDeserializer.class)
public class UpsourceWebhookResponse implements Serializable, UpsourceDTO{
    
    @JsonProperty("majorVersion")
    private long majorVerion;
    @JsonProperty("minorVersion")
    private long minorVersion;
    @JsonProperty("projectId")
    private String projectId;
    @JsonProperty("dataType")
    private String dataType;
    @JsonProperty("data")
    private UpsourceDTO data;
    
    public UpsourceWebhookResponse(){}

    public UpsourceWebhookResponse(
            long majorVerion, long minorVersion, 
            String projectId, String dataType, 
            UpsourceDTO data) {
        this.majorVerion = majorVerion;
        this.minorVersion = minorVersion;
        this.projectId = projectId;
        this.dataType = dataType;
        this.data = data;
    }

    @JsonProperty("majorVerion")
    public long getMajorVerion() {
        return majorVerion;
    }

    @JsonProperty("majorVerion")
    public void setMajorVerion(long majorVerion) {
        this.majorVerion = majorVerion;
    }

    @JsonProperty("minorVersion")
    public long getMinorVersion() {
        return minorVersion;
    }

    @JsonProperty("minorVersion")
    public void setMinorVersion(long minorVersion) {
        this.minorVersion = minorVersion;
    }

    @JsonProperty("projectId")
    public String getProjectId() {
        return projectId;
    }

    @JsonProperty("projectId")
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @JsonProperty("dataType")
    public String getDataType() {
        return dataType;
    }

    @JsonProperty("dataType")
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    @JsonProperty("data")
    public UpsourceDTO getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(UpsourceDTO data) {
        this.data = data;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (int) (this.majorVerion ^ (this.majorVerion >>> 32));
        hash = 41 * hash + (int) (this.minorVersion ^ (this.minorVersion >>> 32));
        hash = 41 * hash + Objects.hashCode(this.projectId);
        hash = 41 * hash + Objects.hashCode(this.dataType);
        hash = 41 * hash + Objects.hashCode(this.data);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UpsourceWebhookResponse other = (UpsourceWebhookResponse) obj;
        if (this.majorVerion != other.majorVerion) {
            return false;
        }
        if (this.minorVersion != other.minorVersion) {
            return false;
        }
        if (!Objects.equals(this.projectId, other.projectId)) {
            return false;
        }
        if (!Objects.equals(this.dataType, other.dataType)) {
            return false;
        }
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UpsourceWebhookResponse{" + "majorVerion=" + majorVerion + ", minorVersion=" + minorVersion + ", projectId=" + projectId + ", dataType=" + dataType + ", data=" + data + '}';
    }
    
    
    
}
