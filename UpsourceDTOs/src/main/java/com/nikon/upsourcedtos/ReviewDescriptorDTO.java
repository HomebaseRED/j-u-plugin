
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonTypeName("review")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "reviewId",
    "title",
    "participants",
    "state",
    "isUnread",
    "isReadyToClose",
    "branch",
    "issue",
    "isRemoved",
    "updatedAt",
    "completionRate",
    "discussionCounter"
})
public class ReviewDescriptorDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("reviewId")
    private ReviewIdDTO reviewId;
    @JsonProperty("title")
    private String title;
    @JsonProperty("participants")
    private List<ParticipantInReviewDTO> participants = null;
    @JsonProperty("state")
    private long state;
    @JsonProperty("isUnread")
    private boolean isUnread;
    @JsonProperty("isReadyToClose")
    private boolean isReadyToClose;
    @JsonProperty("branch")
    private List<String> branch = null;
    @JsonProperty("issue")
    private List<IssueIdDTO> issue = null;
    @JsonProperty("isRemoved")
    private boolean isRemoved;
    @JsonProperty("updatedAt")
    private long updatedAt;
    @JsonProperty("completionRate")
    private CompletionRateDTO completionRate;
    @JsonProperty("discussionCounter")
    private SimpleDiscussionCounterDTO discussionCounter;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -1049339819597390901L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ReviewDescriptorDTO() {
    }

    /**
     * 
     * @param updatedAt
     * @param isRemoved
     * @param title
     * @param isReadyToClose
     * @param discussionCounter
     * @param issue
     * @param branch
     * @param state
     * @param reviewId
     * @param completionRate
     * @param participants
     * @param isUnread
     */
    public ReviewDescriptorDTO(ReviewIdDTO reviewId, String title, List<ParticipantInReviewDTO> participants, long state, boolean isUnread, boolean isReadyToClose, List<String> branch, List<IssueIdDTO> issue, boolean isRemoved, long updatedAt, CompletionRateDTO completionRate, SimpleDiscussionCounterDTO discussionCounter) {
        super();
        this.reviewId = reviewId;
        this.title = title;
        this.participants = participants;
        this.state = state;
        this.isUnread = isUnread;
        this.isReadyToClose = isReadyToClose;
        this.branch = branch;
        this.issue = issue;
        this.isRemoved = isRemoved;
        this.updatedAt = updatedAt;
        this.completionRate = completionRate;
        this.discussionCounter = discussionCounter;
    }

    @JsonProperty("reviewId")
    public ReviewIdDTO getReviewId() {
        return reviewId;
    }

    @JsonProperty("reviewId")
    public void setReviewId(ReviewIdDTO reviewId) {
        this.reviewId = reviewId;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("participants")
    public List<ParticipantInReviewDTO> getParticipants() {
        return participants;
    }

    @JsonProperty("participants")
    public void setParticipants(List<ParticipantInReviewDTO> participants) {
        this.participants = participants;
    }

    @JsonProperty("state")
    public long getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(long state) {
        this.state = state;
    }

    @JsonProperty("isUnread")
    public boolean isIsUnread() {
        return isUnread;
    }

    @JsonProperty("isUnread")
    public void setIsUnread(boolean isUnread) {
        this.isUnread = isUnread;
    }

    @JsonProperty("isReadyToClose")
    public boolean isIsReadyToClose() {
        return isReadyToClose;
    }

    @JsonProperty("isReadyToClose")
    public void setIsReadyToClose(boolean isReadyToClose) {
        this.isReadyToClose = isReadyToClose;
    }

    @JsonProperty("branch")
    public List<String> getBranch(){
        return branch;
    }
    
    @JsonProperty("branch")
    public void setBranch(List<String> branch){
        this.branch = branch;
    }
    
    @JsonProperty("issue")
    public List<IssueIdDTO> getIssue() {
        return issue;
    }

    @JsonProperty("issue")
    public void setIssue(List<IssueIdDTO> issue) {
        this.issue = issue;
    }

    @JsonProperty("isRemoved")
    public boolean isIsRemoved() {
        return isRemoved;
    }

    @JsonProperty("isRemoved")
    public void setIsRemoved(boolean isRemoved) {
        this.isRemoved = isRemoved;
    }

    @JsonProperty("updatedAt")
    public long getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updatedAt")
    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("completionRate")
    public CompletionRateDTO getCompletionRate() {
        return completionRate;
    }

    @JsonProperty("completionRate")
    public void setCompletionRate(CompletionRateDTO completionRate) {
        this.completionRate = completionRate;
    }

    @JsonProperty("discussionCounter")
    public SimpleDiscussionCounterDTO getDiscussionCounter() {
        return discussionCounter;
    }

    @JsonProperty("discussionCounter")
    public void setDiscussionCounter(SimpleDiscussionCounterDTO discussionCounter) {
        this.discussionCounter = discussionCounter;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(reviewId)
                .append(title)
                .append(participants)
                .append(state)
                .append(isUnread)
                .append(isReadyToClose)
                .append(branch)
                .append(issue)
                .append(isRemoved)
                .append(updatedAt)
                .append(completionRate)
                .append(discussionCounter)
                .append(additionalProperties)
                .toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ReviewDescriptorDTO) == false) {
            return false;
        }
        ReviewDescriptorDTO rhs = ((ReviewDescriptorDTO) other);
        return new EqualsBuilder()
                .append(reviewId, rhs.reviewId)
                .append(title, rhs.title)
                .append(participants, rhs.participants)
                .append(state, rhs.state)
                .append(isUnread, rhs.isUnread)
                .append(isReadyToClose, rhs.isReadyToClose)
                .append(branch, rhs.branch)
                .append(issue, rhs.issue)
                .append(isRemoved, rhs.isRemoved)
                .append(updatedAt, rhs.updatedAt)
                .append(completionRate, rhs.completionRate)
                .append(discussionCounter, rhs.discussionCounter)
                .append(additionalProperties, rhs.additionalProperties)
                .isEquals();
    }

}
