/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "position",
    "toPosition",
    "isUp",
    "isSolid",
    "hasArrow",
    "color"
})
public class RevisionListGraphEdgeDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("position")
    private int position;    
    @JsonProperty("toPosition")
    private int toPosition;    
    @JsonProperty("isUp")
    private boolean isUp;    
    @JsonProperty("isSolid")
    private boolean isSolid;    
    @JsonProperty("hasArrow")
    private boolean hasArrow;    
    @JsonProperty("color")
    private int color;
    
    public RevisionListGraphEdgeDTO(){}

    public RevisionListGraphEdgeDTO(int position, int toPosition, boolean isUp, boolean isSolid, boolean hasArrow, int color) {
        this.position = position;
        this.toPosition = toPosition;
        this.isUp = isUp;
        this.isSolid = isSolid;
        this.hasArrow = hasArrow;
        this.color = color;
    }
    
    @JsonProperty("position")
    public int getPosition() {
        return position;
    }
    
    @JsonProperty("position")
    public void setPosition(int position) {
        this.position = position;
    }
    
    @JsonProperty("toPosition")
    public int getToPosition() {
        return toPosition;
    }
    
    @JsonProperty("toPosition")
    public void setToPosition(int toPosition) {
        this.toPosition = toPosition;
    }
    
    @JsonProperty("isUp")
    public boolean isIsUp() {
        return isUp;
    }
    
    @JsonProperty("isUp")
    public void setIsUp(boolean isUp) {
        this.isUp = isUp;
    }
    
    @JsonProperty("isSolid")
    public boolean isIsSolid() {
        return isSolid;
    }
    
    @JsonProperty("isSolid")
    public void setIsSolid(boolean isSolid) {
        this.isSolid = isSolid;
    }
    
    @JsonProperty("hasArrow")
    public boolean isHasArrow() {
        return hasArrow;
    }
    
    @JsonProperty("hasArrow")
    public void setHasArrow(boolean hasArrow) {
        this.hasArrow = hasArrow;
    }
    
    @JsonProperty("color")
    public int getColor() {
        return color;
    }
    
    @JsonProperty("color")
    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.position;
        hash = 41 * hash + this.toPosition;
        hash = 41 * hash + (this.isUp ? 1 : 0);
        hash = 41 * hash + (this.isSolid ? 1 : 0);
        hash = 41 * hash + (this.hasArrow ? 1 : 0);
        hash = 41 * hash + this.color;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RevisionListGraphEdgeDTO other = (RevisionListGraphEdgeDTO) obj;
        if (this.position != other.position) {
            return false;
        }
        if (this.toPosition != other.toPosition) {
            return false;
        }
        if (this.isUp != other.isUp) {
            return false;
        }
        if (this.isSolid != other.isSolid) {
            return false;
        }
        if (this.hasArrow != other.hasArrow) {
            return false;
        }
        if (this.color != other.color) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RevisionListGraphEdgeDTO{" + "position=" + position + ", toPosition=" + toPosition + ", isUp=" + isUp + ", isSolid=" + isSolid + ", hasArrow=" + hasArrow + ", color=" + color + '}';
    }
    
    
}
