/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "range",
    "textAttribute"
})
public class TextMarkupDTO implements Serializable, Comparable, UpsourceDTO{
    
    @JsonProperty("range")
    private RangeDTO range;
    @JsonProperty("textAttribute")
    private TextAttributeDTO textAttribute;

    public TextMarkupDTO() {
    }

    public TextMarkupDTO(RangeDTO range, TextAttributeDTO textAttribute) {
        this.range = range;
        this.textAttribute = textAttribute;
    }

    @JsonProperty("range")
    public RangeDTO getRange() {
        return range;
    }

    @JsonProperty("range")
    public void setRange(RangeDTO range) {
        this.range = range;
    }

    @JsonProperty("textAttribute")
    public TextAttributeDTO getTextAttribute() {
        return textAttribute;
    }

    @JsonProperty("textAttribute")
    public void setTextAttribute(TextAttributeDTO textAttribute) {
        this.textAttribute = textAttribute;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.range);
        hash = 37 * hash + Objects.hashCode(this.textAttribute);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TextMarkupDTO other = (TextMarkupDTO) obj;
        if (!Objects.equals(this.range, other.range)) {
            return false;
        }
        if (!Objects.equals(this.textAttribute, other.textAttribute)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TextMarkupDTO{" + "range=" + range + ", textAttribute=" + textAttribute + '}';
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof TextMarkupDTO){
            TextMarkupDTO other = (TextMarkupDTO) o;
            if(this.equals(other)){
                return 0;
            }
            return this.range.compareTo(other.getRange());
        }
        return -1;
    }
    
}
