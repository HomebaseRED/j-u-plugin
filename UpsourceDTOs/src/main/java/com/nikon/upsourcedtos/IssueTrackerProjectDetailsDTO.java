
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonTypeName("issueTrackerDetails")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "providerKey",
    "projectUrl",
    "field"
})
public class IssueTrackerProjectDetailsDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("providerKey")
    private String providerKey;
    @JsonProperty("projectUrl")
    private String projectUrl;
    @JsonProperty("field")
    private List<IssueFieldDTO> field = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 4994737592146961798L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public IssueTrackerProjectDetailsDTO() {
    }

    /**
     * 
     * @param field
     * @param projectUrl
     * @param providerKey
     */
    public IssueTrackerProjectDetailsDTO(String providerKey, String projectUrl, List<IssueFieldDTO> field) {
        super();
        this.providerKey = providerKey;
        this.projectUrl = projectUrl;
        this.field = field;
    }

    @JsonProperty("providerKey")
    public String getProviderKey() {
        return providerKey;
    }

    @JsonProperty("providerKey")
    public void setProviderKey(String providerKey) {
        this.providerKey = providerKey;
    }

    @JsonProperty("projectUrl")
    public String getProjectUrl() {
        return projectUrl;
    }

    @JsonProperty("projectUrl")
    public void setProjectUrl(String projectUrl) {
        this.projectUrl = projectUrl;
    }

    @JsonProperty("field")
    public List<IssueFieldDTO> getField() {
        return field;
    }

    @JsonProperty("field")
    public void setField(List<IssueFieldDTO> field) {
        this.field = field;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(providerKey).append(projectUrl).append(field).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof IssueTrackerProjectDetailsDTO) == false) {
            return false;
        }
        IssueTrackerProjectDetailsDTO rhs = ((IssueTrackerProjectDetailsDTO) other);
        return new EqualsBuilder().append(providerKey, rhs.providerKey).append(projectUrl, rhs.projectUrl).append(field, rhs.field).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
