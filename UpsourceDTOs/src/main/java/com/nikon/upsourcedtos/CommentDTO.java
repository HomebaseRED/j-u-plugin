
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "discussionId",
    "commentId",
    "text",
    "authorId",
    "date",
    "parentId",
    "isEditable",
    "markupType",
    "isSynchronized",
    "syncResult"
})
public class CommentDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("discussionId")
    private String discussionId;
    @JsonProperty("commentId")
    private String commentId;
    @JsonProperty("text")
    private String text;
    @JsonProperty("authorId")
    private String authorId;
    @JsonProperty("date")
    private long date;
    @JsonProperty("isEditable")
    private boolean isEditable;
    @JsonProperty("isSynchronized")
    private boolean isSynchronized;
    @JsonProperty("parentId")
    private String parentId;
    @JsonProperty("markupType")
    private String markupType;
    @JsonProperty("syncResult")
    private long syncResult;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -3633765249289862816L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CommentDTO() {
    }

    /**
     * 
     * @param parentId
     * @param text
     * @param isSynchronized
     * @param date
     * @param discussionId
     * @param isEditable
     * @param commentId
     * @param authorId
     * @param markupType
     * @param syncResult
     */
    public CommentDTO(String discussionId, String commentId, String text, String authorId, long date, boolean isEditable, boolean isSynchronized, String parentId, String markupType, long syncResult) {
        this.discussionId = discussionId;
        this.commentId = commentId;
        this.text = text;
        this.authorId = authorId;
        this.date = date;
        this.isEditable = isEditable;
        this.isSynchronized = isSynchronized;
        this.parentId = parentId;
        this.markupType = markupType;
        this.syncResult = syncResult;
    }

    @JsonProperty(value = "discussionId")
    public String getDiscussionId() {
        return discussionId;
    }

    @JsonProperty("discussionId")
    public void setDiscussionId(String discussionId) {
        this.discussionId = discussionId;
    }

    @JsonProperty("commentId")
    public String getCommentId() {
        return commentId;
    }

    @JsonProperty("commentId")
    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("authorId")
    public String getAuthorId() {
        return authorId;
    }

    @JsonProperty("authorId")
    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    @JsonProperty("date")
    public long getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(long date) {
        this.date = date;
    }

    @JsonProperty("isEditable")
    public boolean isIsEditable() {
        return isEditable;
    }

    @JsonProperty("isEditable")
    public void setIsEditable(boolean isEditable) {
        this.isEditable = isEditable;
    }

    @JsonProperty("isSynchronized")
    public boolean isIsSynchronized() {
        return isSynchronized;
    }

    @JsonProperty("isSynchronized")
    public void setIsSynchronized(boolean isSynchronized) {
        this.isSynchronized = isSynchronized;
    }

    @JsonProperty("parentId")
    public String getParentId() {
        return parentId;
    }

    @JsonProperty("parentId")
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @JsonProperty("markupType")
    public String getMarkupType() {
        return markupType;
    }

    @JsonProperty("markupType")
    public void setMarkupType(String markupType) {
        this.markupType = markupType;
    }

    @JsonProperty("syncResult")
    public long getSyncResult() {
        return syncResult;
    }

    @JsonProperty("syncResult")
    public void setSyncResult(long syncResult) {
        this.syncResult = syncResult;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(discussionId).append(commentId).append(text).append(authorId).append(date).append(isEditable).append(isSynchronized).append(parentId).append(markupType).append(syncResult).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CommentDTO) == false) {
            return false;
        }
        CommentDTO rhs = ((CommentDTO) other);
        return new EqualsBuilder().append(discussionId, rhs.discussionId).append(commentId, rhs.commentId).append(text, rhs.text).append(authorId, rhs.authorId).append(date, rhs.date).append(isEditable, rhs.isEditable).append(isSynchronized, rhs.isSynchronized).append(parentId, rhs.parentId).append(markupType, rhs.markupType).append(syncResult, rhs.syncResult).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
