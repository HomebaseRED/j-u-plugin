
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "projectId",
    "projectName",
    "headHash",
    "isReady",
    "lastCommitDate",
    "lastCommitAuthorName",
    "iconUrl",
    "group",
    "founder",
    "lastDayCommits",
    "lastMonthCommits"
})
public class ShortProjectInfoDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("projectId")
    private String projectId;
    @JsonProperty("projectName")
    private String projectName;
    @JsonProperty("headHash")
    private String headHash;
    @JsonProperty("isReady")
    private boolean isReady;
    @JsonProperty("lastCommitDate")
    private long lastCommitDate;
    @JsonProperty("lastCommitAuthorName")
    private String lastCommitAuthorName;
    @JsonProperty("iconUrl")
    private String iconUrl;
    @JsonProperty("group")
    private ProjectGroupDTO group;
    @JsonProperty("founder")
    private ProjectFounderDTO founder;
    @JsonProperty("lastDayCommits")
    private int lastDayCommits;
    @JsonProperty("lastMonthCommits")
    private int lastMonthCommits;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -3220229173707737782L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ShortProjectInfoDTO() {
    }

    /**
     * 
     * @param isReady
     * @param headHash
     * @param lastCommitDate
     * @param iconUrl
     * @param lastCommitAuthorName
     * @param projectId
     * @param group
     * @param projectName
     * @param founder
     * @param lastDayCommits
     * @param lastMonthCommits
     */
    public ShortProjectInfoDTO(String projectId, String projectName, String headHash, boolean isReady, long lastCommitDate, String lastCommitAuthorName, String iconUrl, ProjectGroupDTO group, ProjectFounderDTO founder, int lastDayCommits, int lastMonthCommits) {
        this.projectId = projectId;
        this.projectName = projectName;
        this.headHash = headHash;
        this.isReady = isReady;
        this.lastCommitDate = lastCommitDate;
        this.lastCommitAuthorName = lastCommitAuthorName;
        this.iconUrl = iconUrl;
        this.group = group;
        this.founder = founder;
        this.lastDayCommits = lastDayCommits;
        this.lastMonthCommits = lastMonthCommits;
    }

    @JsonProperty(value = "projectId")
    public String getProjectId() {
        return projectId;
    }

    @JsonProperty("projectId")
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @JsonProperty("projectName")
    public String getProjectName() {
        return projectName;
    }

    @JsonProperty("projectName")
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @JsonProperty("headHash")
    public String getHeadHash() {
        return headHash;
    }

    @JsonProperty("headHash")
    public void setHeadHash(String headHash) {
        this.headHash = headHash;
    }

    @JsonProperty("isReady")
    public boolean isIsReady() {
        return isReady;
    }

    @JsonProperty("isReady")
    public void setIsReady(boolean isReady) {
        this.isReady = isReady;
    }

    @JsonProperty("lastCommitDate")
    public long getLastCommitDate() {
        return lastCommitDate;
    }

    @JsonProperty("lastCommitDate")
    public void setLastCommitDate(long lastCommitDate) {
        this.lastCommitDate = lastCommitDate;
    }

    @JsonProperty("lastCommitAuthorName")
    public String getLastCommitAuthorName() {
        return lastCommitAuthorName;
    }

    @JsonProperty("lastCommitAuthorName")
    public void setLastCommitAuthorName(String lastCommitAuthorName) {
        this.lastCommitAuthorName = lastCommitAuthorName;
    }

    @JsonProperty("iconUrl")
    public String getIconUrl() {
        return iconUrl;
    }

    @JsonProperty("iconUrl")
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    @JsonProperty("group")
    public ProjectGroupDTO getGroup() {
        return group;
    }

    @JsonProperty("group")
    public void setGroup(ProjectGroupDTO group) {
        this.group = group;
    }

    @JsonProperty("founder")
    public ProjectFounderDTO getFounder() {
        return founder;
    }

    @JsonProperty("founder")
    public void setFounder(ProjectFounderDTO founder) {
        this.founder = founder;
    }

    @JsonProperty("lastDayCommits")
    public int getLastDayCommits() {
        return lastDayCommits;
    }

    @JsonProperty("lastDayCommits")
    public void setLastDayCommits(int lastDayCommits) {
        this.lastDayCommits = lastDayCommits;
    }

    @JsonProperty("lastMonthCommits")
    public int getLastMonthCommits() {
        return lastMonthCommits;
    }

    @JsonProperty("lastMonthCommits")
    public void setLastMonthCommits(int lastMonthCommits) {
        this.lastMonthCommits = lastMonthCommits;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(projectId)
                .append(projectName)
                .append(headHash)
                .append(isReady)
                .append(lastCommitDate)
                .append(lastCommitAuthorName)
                .append(iconUrl)
                .append(group)
                .append(founder)
                .append(lastDayCommits)
                .append(lastMonthCommits)
                .append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ShortProjectInfoDTO) == false) {
            return false;
        }
        ShortProjectInfoDTO rhs = ((ShortProjectInfoDTO) other);
        return new EqualsBuilder()
                .append(projectId, rhs.projectId)
                .append(projectName, rhs.projectName)
                .append(headHash, rhs.headHash)
                .append(isReady, rhs.isReady)
                .append(lastCommitDate, rhs.lastCommitDate)
                .append(lastCommitAuthorName, rhs.lastCommitAuthorName)
                .append(iconUrl, rhs.iconUrl)
                .append(group, rhs.group)
                .append(founder, rhs.founder)
                .append(lastDayCommits, rhs.lastDayCommits)
                .append(lastMonthCommits, rhs.lastMonthCommits)
                .append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
