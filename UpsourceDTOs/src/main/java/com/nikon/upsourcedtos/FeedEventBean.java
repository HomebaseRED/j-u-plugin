/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "userId",
    "userIds",
    "reviewNumber",
    "reviewId",
    "date",
    "actor",
    "feedEventId"
})
public class FeedEventBean implements Serializable, UpsourceDTO {
    
    @JsonProperty("userId")
    private UserIdBean userId;
    @JsonProperty("userIds")
    private List<UserIdBean> userIds = null;
    @JsonProperty("reviewNumber")
    private int reviewNumber;
    @JsonProperty("reviewId")
    private String reviewId;
    @JsonProperty("date")
    private long date;
    @JsonProperty("actor")
    private UserIdBean actor;
    @JsonProperty("feedEventId")
    private String feedEventId;

    public FeedEventBean() {
    }

    public FeedEventBean(UserIdBean userId, List<UserIdBean> userIds, int reviewNumber, String reviewId, long date, UserIdBean actor, String feedEventId) {
        this.userId = userId;
        this.userIds = userIds;
        this.reviewNumber = reviewNumber;
        this.reviewId = reviewId;
        this.date = date;
        this.actor = actor;
        this.feedEventId = feedEventId;
    }

    @JsonProperty("userId")
    public UserIdBean getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(UserIdBean userId) {
        this.userId = userId;
    }

    @JsonProperty("userIds")
    public List<UserIdBean> getUserIds() {
        return userIds;
    }

    @JsonProperty("userIds")
    public void setUserIds(List<UserIdBean> userIds) {
        this.userIds = userIds;
    }

    @JsonProperty("reviewNumber")
    public int getReviewNumber() {
        return reviewNumber;
    }

    @JsonProperty("reviewNumber")
    public void setReviewNumber(int reviewNumber) {
        this.reviewNumber = reviewNumber;
    }

    @JsonProperty("reviewId")
    public String getReviewId() {
        return reviewId;
    }

    @JsonProperty("reviewId")
    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    @JsonProperty("date")
    public long getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(long date) {
        this.date = date;
    }

    @JsonProperty("actor")
    public UserIdBean getActor() {
        return actor;
    }

    @JsonProperty("actor")
    public void setActor(UserIdBean actor) {
        this.actor = actor;
    }

    @JsonProperty("feedEventId")
    public String getFeedEventId() {
        return feedEventId;
    }

    @JsonProperty("feedEventId")
    public void setFeedEventId(String feedEventId) {
        this.feedEventId = feedEventId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.userId);
        hash = 53 * hash + Objects.hashCode(this.userIds);
        hash = 53 * hash + this.reviewNumber;
        hash = 53 * hash + Objects.hashCode(this.reviewId);
        hash = 53 * hash + (int) (this.date ^ (this.date >>> 32));
        hash = 53 * hash + Objects.hashCode(this.actor);
        hash = 53 * hash + Objects.hashCode(this.feedEventId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FeedEventBean other = (FeedEventBean) obj;
        if (this.reviewNumber != other.reviewNumber) {
            return false;
        }
        if (this.date != other.date) {
            return false;
        }
        if (!Objects.equals(this.reviewId, other.reviewId)) {
            return false;
        }
        if (!Objects.equals(this.feedEventId, other.feedEventId)) {
            return false;
        }
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        if (!Objects.equals(this.userIds, other.userIds)) {
            return false;
        }
        if (!Objects.equals(this.actor, other.actor)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FeedEventBean{" + "userId=" + userId + ", userIds=" + userIds + ", reviewNumber=" + reviewNumber + ", reviewId=" + reviewId + ", date=" + date + ", actor=" + actor + ", feedEventId=" + feedEventId + '}';
    }

    
}
