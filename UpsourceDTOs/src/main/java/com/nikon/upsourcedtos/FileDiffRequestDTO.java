/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "leftFile",
    "rightFile",
    "ignoreWhitespace"
})
public class FileDiffRequestDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("leftFile")
    private FileInRevisionDTO leftFile;
    @JsonProperty("rightFile")
    private FileInRevisionDTO rightFile;
    @JsonProperty("ignoreWhitespace")
    private boolean ignoreWhitespace;

    public FileDiffRequestDTO() {
    }

    public FileDiffRequestDTO(FileInRevisionDTO rightFile){
        this.leftFile = null;
        this.rightFile = rightFile;
        this.ignoreWhitespace = true;
    }
    
    public FileDiffRequestDTO(FileInRevisionDTO leftFile, FileInRevisionDTO rightFile, boolean ignoreWhitespace) {
        this.leftFile = leftFile;
        this.rightFile = rightFile;
        this.ignoreWhitespace = ignoreWhitespace;
    }

    @JsonProperty("leftFile")
    public FileInRevisionDTO getLeftFile() {
        return leftFile;
    }

    @JsonProperty("leftFile")
    public void setLeftFile(FileInRevisionDTO leftFile) {
        this.leftFile = leftFile;
    }

    @JsonProperty("rightFile")
    public FileInRevisionDTO getRightFile() {
        return rightFile;
    }

    @JsonProperty("rightFile")
    public void setRightFile(FileInRevisionDTO rightFile) {
        this.rightFile = rightFile;
    }

    @JsonProperty("ignoreWhitespace")
    public boolean isIgnoreWhitespace() {
        return ignoreWhitespace;
    }

    @JsonProperty("ignoreWhitespace")
    public void setIgnoreWhitespace(boolean ignoreWhitespace) {
        this.ignoreWhitespace = ignoreWhitespace;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.leftFile);
        hash = 59 * hash + Objects.hashCode(this.rightFile);
        hash = 59 * hash + (this.ignoreWhitespace ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileDiffRequestDTO other = (FileDiffRequestDTO) obj;
        if (this.ignoreWhitespace != other.ignoreWhitespace) {
            return false;
        }
        if (!Objects.equals(this.leftFile, other.leftFile)) {
            return false;
        }
        if (!Objects.equals(this.rightFile, other.rightFile)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FileDiffRequestDTO{" + "leftFile=" + leftFile + ", rightFile=" + rightFile + ", ignoreWhitespace=" + ignoreWhitespace + '}';
    }
            
}
