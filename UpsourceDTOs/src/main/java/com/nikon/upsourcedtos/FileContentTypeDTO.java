/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "isText",
    "isDirectory",
    "isGenerated",
    "canDownload",
    "fileType"
})
public class FileContentTypeDTO implements Serializable, UpsourceDTO {
    
    @JsonProperty("isText")
    private boolean isText;
    @JsonProperty("isDirectory")
    private boolean isDirectory;
    @JsonProperty("isGenerated")
    private boolean isGenerated;
    @JsonProperty("canDownload")
    private boolean canDownload;
    @JsonProperty("fileType")
    private String fileType;

    public FileContentTypeDTO() {
    }

    public FileContentTypeDTO(boolean isText, boolean isDirectory, boolean isGenerated, boolean canDownload, String fileType) {
        this.isText = isText;
        this.isDirectory = isDirectory;
        this.isGenerated = isGenerated;
        this.canDownload = canDownload;
        this.fileType = fileType;
    }

    @JsonProperty("isText")
    public boolean isIsText() {
        return isText;
    }

    @JsonProperty("isText")
    public void setIsText(boolean isText) {
        this.isText = isText;
    }

    @JsonProperty("isDirectory")
    public boolean isIsDirectory() {
        return isDirectory;
    }

    @JsonProperty("isDirectory")
    public void setIsDirectory(boolean isDirectory) {
        this.isDirectory = isDirectory;
    }

    @JsonProperty("isGenerated")
    public boolean isIsGenerated() {
        return isGenerated;
    }

    @JsonProperty("isGenerated")
    public void setIsGenerated(boolean isGenerated) {
        this.isGenerated = isGenerated;
    }

    @JsonProperty("canDownload")
    public boolean isCanDownload() {
        return canDownload;
    }

    @JsonProperty("canDownload")
    public void setCanDownload(boolean canDownload) {
        this.canDownload = canDownload;
    }

    @JsonProperty("fileType")
    public String getFileType() {
        return fileType;
    }

    @JsonProperty("fileType")
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.isText ? 1 : 0);
        hash = 97 * hash + (this.isDirectory ? 1 : 0);
        hash = 97 * hash + (this.isGenerated ? 1 : 0);
        hash = 97 * hash + (this.canDownload ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.fileType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileContentTypeDTO other = (FileContentTypeDTO) obj;
        if (this.isText != other.isText) {
            return false;
        }
        if (this.isDirectory != other.isDirectory) {
            return false;
        }
        if (this.isGenerated != other.isGenerated) {
            return false;
        }
        if (this.canDownload != other.canDownload) {
            return false;
        }
        if (!Objects.equals(this.fileType, other.fileType)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FileContentTypeDTO{" + "isText=" + isText + ", isDirectory=" + isDirectory + ", isGenerated=" + isGenerated + ", canDownload=" + canDownload + ", fileType=" + fileType + '}';
    }
    
    
}
