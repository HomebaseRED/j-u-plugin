
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonTypeName("field")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "defaultValueId",
    "value"
})
public class IssueFieldDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("defaultValueId")
    private String defaultValueId;
    @JsonProperty("value")
    private List<IssueFieldValueDTO> value = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 4864467924739688786L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public IssueFieldDTO() {
    }

    /**
     * 
     * @param id
     * @param name
     * @param value
     */
    public IssueFieldDTO(String id, String name, String defaultValueId, List<IssueFieldValueDTO> value) {
        super();
        this.id = id;
        this.name = name;
        this.defaultValueId = defaultValueId;
        this.value = value;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("defaultValueId")
    public String getDefaultValueId() {
        return defaultValueId;
    }

    @JsonProperty("defaultValueId")
    public void setDefaultValueId(String defaultValueId) {
        this.defaultValueId = defaultValueId;
    }

    @JsonProperty("value")
    public List<IssueFieldValueDTO> getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(List<IssueFieldValueDTO> value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(id)
                .append(name)
                .append(defaultValueId)
                .append(value)
                .append(additionalProperties)
                .toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof IssueFieldDTO) == false) {
            return false;
        }
        IssueFieldDTO rhs = ((IssueFieldDTO) other);
        return new EqualsBuilder()
                .append(id, rhs.id)
                .append(name, rhs.name)
                .append(defaultValueId, rhs.defaultValueId)
                .append(value, rhs.value)
                .append(additionalProperties, rhs.additionalProperties)
                .isEquals();
    }

}
