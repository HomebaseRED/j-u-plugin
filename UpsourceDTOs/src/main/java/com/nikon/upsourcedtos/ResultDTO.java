
package com.nikon.upsourcedtos;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonTypeInfo;

/**
 *
 * @author RVanhuysse
 */


public class ResultDTO implements UpsourceDTO {
    
    @JsonTypeInfo(
            use = JsonTypeInfo.Id.NAME,
            include = JsonTypeInfo.As.PROPERTY,
            property = "type"
    )
    /*
    @JsonSubTypes({        
        @JsonSubTypes.Type(value = CompletionRateDTO.class, name="completionRate"),
        @JsonSubTypes.Type(value = DiscussionCounterDTO.class, name = "discussionCounter"),
        @JsonSubTypes.Type(value = ErrorDTO.class, name = "error"),
        @JsonSubTypes.Type(value = FieldDTO.class, name = "field"),
        @JsonSubTypes.Type(value = ProjectGroupDTO.class, name = "group"),
        @JsonSubTypes.Type(value = IssueDTO.class, name = "issue"),
        @JsonSubTypes.Type(value = ExternalLinkDTO.class, name = "externalLinks"),
        @JsonSubTypes.Type(value = IssueTrackerDetailsDTO.class, name = "issueTrackerDetails"),        
        @JsonSubTypes.Type(value = ParticipantDTO.class, name = "participant"),
        @JsonSubTypes.Type(value = ProjectInfoDTO.class, name = "projectInfo"),
        @JsonSubTypes.Type(value = ReviewDTO.class, name= "review"),
        @JsonSubTypes.Type(value = ReviewIdDTO.class, name = "reviewId"),
        @JsonSubTypes.Type(value = ReviewListDTO.class, name= "reviews"),
        @JsonSubTypes.Type(value = ValueDTO.class, name = "value")
    })
    */
    private UpsourceDTO result;
    
    public ResultDTO(){}

    public UpsourceDTO getResult() {
        return result;
    }

    public void setResult(UpsourceDTO result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
    
    
    
}
