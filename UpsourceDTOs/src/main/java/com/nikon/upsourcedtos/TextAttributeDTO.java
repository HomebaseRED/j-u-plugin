/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "fgColor",
    "bgColor",
    "fontStyle",
    "effectStyle",
    "effectColor",
    "errorStripeColor"
})
public class TextAttributeDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("fgColor")
    private String fgColor;
    @JsonProperty("bgColor")
    private String bgColor;
    @JsonProperty("fontStyle")
    private String fontStyle;
    @JsonProperty("effectStyle")
    private String effectStyle;
    @JsonProperty("effectColor")
    private String effectColor;
    @JsonProperty("errorStripeColor")
    private String errorStripeColor;

    public TextAttributeDTO() {
    }

    public TextAttributeDTO(String fgColor, String bgColor, String fontStyle, String effectStyle, String effectColor, String errorStripeColor) {
        this.fgColor = fgColor;
        this.bgColor = bgColor;
        this.fontStyle = fontStyle;
        this.effectStyle = effectStyle;
        this.effectColor = effectColor;
        this.errorStripeColor = errorStripeColor;
    }

    @JsonProperty("fgColor")
    public String getFgColor() {
        return fgColor;
    }

    @JsonProperty("fgColor")
    public void setFgColor(String fgColor) {
        this.fgColor = fgColor;
    }

    @JsonProperty("bgColor")
    public String getBgColor() {
        return bgColor;
    }

    @JsonProperty("bgColor")
    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    @JsonProperty("fontStyle")
    public String getFontStyle() {
        return fontStyle;
    }

    @JsonProperty("fontStyle")
    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }

    @JsonProperty("effectStyle")
    public String getEffectStyle() {
        return effectStyle;
    }

    @JsonProperty("effectStyle")
    public void setEffectStyle(String effectStyle) {
        this.effectStyle = effectStyle;
    }

    @JsonProperty("effectColor")
    public String getEffectColor() {
        return effectColor;
    }

    @JsonProperty("effectColor")
    public void setEffectColor(String effectColor) {
        this.effectColor = effectColor;
    }

    @JsonProperty("errorStripeColor")
    public String getErrorStripeColor() {
        return errorStripeColor;
    }

    @JsonProperty("errorStripeColor")
    public void setErrorStripeColor(String errorStripeColor) {
        this.errorStripeColor = errorStripeColor;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.fgColor);
        hash = 29 * hash + Objects.hashCode(this.bgColor);
        hash = 29 * hash + Objects.hashCode(this.fontStyle);
        hash = 29 * hash + Objects.hashCode(this.effectStyle);
        hash = 29 * hash + Objects.hashCode(this.effectColor);
        hash = 29 * hash + Objects.hashCode(this.errorStripeColor);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TextAttributeDTO other = (TextAttributeDTO) obj;
        if (!Objects.equals(this.fgColor, other.fgColor)) {
            return false;
        }
        if (!Objects.equals(this.bgColor, other.bgColor)) {
            return false;
        }
        if (!Objects.equals(this.fontStyle, other.fontStyle)) {
            return false;
        }
        if (!Objects.equals(this.effectStyle, other.effectStyle)) {
            return false;
        }
        if (!Objects.equals(this.effectColor, other.effectColor)) {
            return false;
        }
        if (!Objects.equals(this.errorStripeColor, other.errorStripeColor)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TextAttributeDTO{" + "fgColor=" + fgColor + ", bgColor=" + bgColor + ", fontStyle=" + fontStyle + ", effectStyle=" + effectStyle + ", effectColor=" + effectColor + ", errorStripeColor=" + errorStripeColor + '}';
    }
    
    
}
