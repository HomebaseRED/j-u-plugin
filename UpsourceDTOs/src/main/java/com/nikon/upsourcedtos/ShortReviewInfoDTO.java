/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "reviewId",
    "title",
    "state",
    "branch",
    "completionRate"
})
public class ShortReviewInfoDTO implements Serializable, UpsourceDTO {
    
    @JsonProperty("reviewId")
    private ReviewIdDTO reviewId;
    @JsonProperty("title")
    private String title;
    @JsonProperty("state")
    private long state;
    @JsonProperty("branch")
    private List<String> branch = null;
    @JsonProperty("completionRate")
    private CompletionRateDTO completionRate;

    public ShortReviewInfoDTO() {
    }

    public ShortReviewInfoDTO(ReviewIdDTO reviewId, String title, long state, List<String> branch, CompletionRateDTO completionRate) {
        this.reviewId = reviewId;
        this.title = title;
        this.state = state;
        this.branch = branch;
        this.completionRate = completionRate;
    }

    @JsonProperty("reviewId")
    public ReviewIdDTO getReviewId() {
        return reviewId;
    }

    @JsonProperty("reviewId")
    public void setReviewId(ReviewIdDTO reviewId) {
        this.reviewId = reviewId;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("state")
    public long getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(long state) {
        this.state = state;
    }

    @JsonProperty("branch")
    public List<String> getBranch() {
        return branch;
    }

    @JsonProperty("branch")
    public void setBranch(List<String> branch) {
        this.branch = branch;
    }

    @JsonProperty("completionRate")
    public CompletionRateDTO getCompletionRate() {
        return completionRate;
    }

    @JsonProperty("completionRate")
    public void setCompletionRate(CompletionRateDTO completionRate) {
        this.completionRate = completionRate;
    }
    
    
    
}
