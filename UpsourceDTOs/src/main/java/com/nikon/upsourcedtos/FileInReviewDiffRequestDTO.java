/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "file",
        "ignoreWhitespace",
        "revisions",
        "showUnrelatedChanges"
})
public class FileInReviewDiffRequestDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("file")
    private FileInReviewDTO file;
    @JsonProperty("ignoreWhitespace")
    private boolean ignoreWhitespace;
    @JsonProperty("revisions")
    private RevisionsSetDTO revisions;
    @JsonProperty("showUnrelatedChanges")
    private boolean showUnrelatedChanges;

    public FileInReviewDiffRequestDTO() {
    }

    public FileInReviewDiffRequestDTO(FileInReviewDTO file, boolean ignoreWhitespace, RevisionsSetDTO revisions, boolean showUnrelatedChanges) {
        this.file = file;
        this.ignoreWhitespace = ignoreWhitespace;
        this.revisions = revisions;
        this.showUnrelatedChanges = showUnrelatedChanges;
    }

    @JsonProperty("file")
    public FileInReviewDTO getFile() {
        return file;
    }

    @JsonProperty("file")
    public void setFile(FileInReviewDTO file) {
        this.file = file;
    }

    @JsonProperty("ignoreWhitespace")
    public boolean isIgnoreWhitespace() {
        return ignoreWhitespace;
    }

    @JsonProperty("ignoreWhitespace")
    public void setIgnoreWhitespace(boolean ignoreWhitespace) {
        this.ignoreWhitespace = ignoreWhitespace;
    }

    @JsonProperty("revisions")
    public RevisionsSetDTO getRevisions() {
        return revisions;
    }

    @JsonProperty("revisions")
    public void setRevisions(RevisionsSetDTO revisions) {
        this.revisions = revisions;
    }

    @JsonProperty("showUnrelatedChanges")
    public boolean isShowUnrelatedChanges() {
        return showUnrelatedChanges;
    }

    @JsonProperty("showUnrelatedChanges")
    public void setShowUnrelatedChanges(boolean showUnrelatedChanges) {
        this.showUnrelatedChanges = showUnrelatedChanges;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.file);
        hash = 53 * hash + (this.ignoreWhitespace ? 1 : 0);
        hash = 53 * hash + Objects.hashCode(this.revisions);
        hash = 53 * hash + (this.showUnrelatedChanges ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileInReviewDiffRequestDTO other = (FileInReviewDiffRequestDTO) obj;
        if (this.ignoreWhitespace != other.ignoreWhitespace) {
            return false;
        }
        if (this.showUnrelatedChanges != other.showUnrelatedChanges) {
            return false;
        }
        if (!Objects.equals(this.file, other.file)) {
            return false;
        }
        if (!Objects.equals(this.revisions, other.revisions)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FileInReviewDiffRequestDTO{" + "file=" + file + ", ignoreWhitespace=" + ignoreWhitespace + ", revisions=" + revisions + ", showUnrelatedChanges=" + showUnrelatedChanges + '}';
    }
}
