
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "range",
    "fileId",
    "revisionId",
    "inlineInRevision"
})
public class AnchorDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("range")
    private RangeDTO range;
    @JsonProperty("fileId")
    private String fileId;
    @JsonProperty("revisionId")
    private String revisionId;
    @JsonProperty("inlineInRevision")
    private String inlineInRevision;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 1489508234321726799L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public AnchorDTO() {
    }

    /**
     * 
     * @param revisionId
     */
    public AnchorDTO(
            RangeDTO range,
            String fileId,
            String revisionId,
            String inlineInRevision) {
        super();
        this.range = range;
        this.fileId = fileId;
        this.revisionId = revisionId;
        this.inlineInRevision = inlineInRevision;
    }

    @JsonProperty("range")
    public RangeDTO getRange() {
        return range;
    }

    @JsonProperty("range")
    public void setRange(RangeDTO range) {
        this.range = range;
    }

    @JsonProperty("fileId")
    public String getFileId() {
        return fileId;
    }

    @JsonProperty("fileId")
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    @JsonProperty("revisionId")
    public String getRevisionId() {
        return revisionId;
    }

    @JsonProperty("revisionId")
    public void setRevisionId(String revisionId) {
        this.revisionId = revisionId;
    }

    @JsonProperty("inlineInRevision")
    public String getinlineInRevision() {
        return inlineInRevision;
    }

    @JsonProperty("inlineInRevision")
    public void setinlineInRevision(String inlineInRevision) {
        this.inlineInRevision = inlineInRevision;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(range).append(fileId).append(revisionId).append(inlineInRevision).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AnchorDTO) == false) {
            return false;
        }
        AnchorDTO rhs = ((AnchorDTO) other);
        return new EqualsBuilder().append(range, rhs.range).append(fileId, rhs.fileId).append(revisionId, rhs.revisionId).append(inlineInRevision, rhs.inlineInRevision).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
