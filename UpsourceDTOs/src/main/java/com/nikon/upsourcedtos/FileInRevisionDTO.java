/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "projectId",
    "revisionId",
    "fileName"
})
public class FileInRevisionDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("projectId")
    private String projectId;
    @JsonProperty("revisionId")
    private String revisionId;
    @JsonProperty("fileName")
    private String fileName;

    public FileInRevisionDTO() {
    }

    public FileInRevisionDTO(String projectId, String revisionId, String fileName) {
        this.projectId = projectId;
        this.revisionId = revisionId;
        this.fileName = fileName;
    }

    @JsonProperty("projectId")
    public String getProjectId() {
        return projectId;
    }

    @JsonProperty("projectId")
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @JsonProperty("revisionId")
    public String getRevisionId() {
        return revisionId;
    }

    @JsonProperty("revisionId")
    public void setRevisionId(String revisionId) {
        this.revisionId = revisionId;
    }

    @JsonProperty("fileName")
    public String getFileName() {
        return fileName;
    }
    
    @JsonProperty("fileName")
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.projectId);
        hash = 37 * hash + Objects.hashCode(this.revisionId);
        hash = 37 * hash + Objects.hashCode(this.fileName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FileInRevisionDTO other = (FileInRevisionDTO) obj;
        if (!Objects.equals(this.projectId, other.projectId)) {
            return false;
        }
        if (!Objects.equals(this.revisionId, other.revisionId)) {
            return false;
        }
        if (!Objects.equals(this.fileName, other.fileName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FileInRevisionDTO{" + "projectId=" + projectId + ", revisionId=" + revisionId + ", fileName=" + fileName + '}';
    }   
}
