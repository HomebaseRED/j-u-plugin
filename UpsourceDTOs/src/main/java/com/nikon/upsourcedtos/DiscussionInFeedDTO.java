
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "projectId",
    "discussionId",
    "anchor",
    "comments",
    "review",
    "labels",
    "read",
    "isStarred",
    "firstUnreadCommentId",
    "issue",
    "isResolved"
})
public class DiscussionInFeedDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("projectId")
    private String projectId;
    @JsonProperty("discussionId")
    private String discussionId;
    @JsonProperty("anchor")
    private AnchorDTO anchor;
    @JsonProperty("comments")
    private List<CommentDTO> comments = null;
    @JsonProperty("review")
    private ShortReviewInfoDTO review;
    @JsonProperty("labels")
    private List<LabelDTO> labels;
    @JsonProperty("read")
    private long read;
    @JsonProperty("isStarred")
    private boolean isStarred;
    @JsonProperty("firstUnreadCommentId")
    private String firstUnreadCommentId;
    @JsonProperty("issue")
    private String issue;
    @JsonProperty("isResolved")
    private boolean isResolved;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -6342784057200937306L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DiscussionInFeedDTO() {
    }

    /**
     * 
     * @param firstUnreadCommentId
     * @param labels
     * @param isStarred
     * @param read
     * @param projectId
     * @param isResolved
     * @param discussionId
     * @param review
     * @param comments
     * @param anchor
     * @param issue
     */
    public DiscussionInFeedDTO(String projectId, String discussionId, AnchorDTO anchor, List<CommentDTO> comments, ShortReviewInfoDTO review, long read, boolean isStarred, String firstUnreadCommentId, boolean isResolved, List<LabelDTO> labels, String issue) {
        super();
        this.projectId = projectId;
        this.discussionId = discussionId;
        this.anchor = anchor;
        this.comments = comments;
        this.review = review;
        this.read = read;
        this.isStarred = isStarred;
        this.firstUnreadCommentId = firstUnreadCommentId;
        this.isResolved = isResolved;
        this.labels = labels;
        this.issue = issue;
    }

    @JsonProperty("projectId")
    public String getProjectId() {
        return projectId;
    }

    @JsonProperty("projectId")
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @JsonProperty("discussionId")
    public String getDiscussionId() {
        return discussionId;
    }

    @JsonProperty("discussionId")
    public void setDiscussionId(String discussionId) {
        this.discussionId = discussionId;
    }

    @JsonProperty("anchor")
    public AnchorDTO getAnchor() {
        return anchor;
    }

    @JsonProperty("anchor")
    public void setAnchor(AnchorDTO anchor) {
        this.anchor = anchor;
    }

    @JsonProperty("comments")
    public List<CommentDTO> getComments() {
        return comments;
    }

    @JsonProperty("comments")
    public void setComments(List<CommentDTO> comments) {
        this.comments = comments;
    }

    @JsonProperty("review")
    public ShortReviewInfoDTO getReview() {
        return review;
    }

    @JsonProperty("review")
    public void setReview(ShortReviewInfoDTO review) {
        this.review = review;
    }

    @JsonProperty("read")
    public long getRead() {
        return read;
    }

    @JsonProperty("read")
    public void setRead(long read) {
        this.read = read;
    }

    @JsonProperty("isStarred")
    public boolean isIsStarred() {
        return isStarred;
    }

    @JsonProperty("isStarred")
    public void setIsStarred(boolean isStarred) {
        this.isStarred = isStarred;
    }

    @JsonProperty("firstUnreadCommentId")
    public String getFirstUnreadCommentId() {
        return firstUnreadCommentId;
    }

    @JsonProperty("firstUnreadCommentId")
    public void setFirstUnreadCommentId(String firstUnreadCommentId) {
        this.firstUnreadCommentId = firstUnreadCommentId;
    }

    @JsonProperty("isResolved")
    public boolean isIsResolved() {
        return isResolved;
    }

    @JsonProperty("isResolved")
    public void setIsResolved(boolean isResolved) {
        this.isResolved = isResolved;
    }

    @JsonProperty("labels")
    public List<LabelDTO> getLabels() {
        return labels;
    }

    @JsonProperty("labels")
    public void setLabels(List<LabelDTO> labels) {
        this.labels = labels;
    }
    
    @JsonProperty("issue")
    public String getIssue() {
        return issue;
    }

    @JsonProperty("issue")
    public void setIssue(String issue) {
        this.issue = issue;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(projectId).append(discussionId).append(anchor).append(comments).append(review).append(read).append(isStarred).append(firstUnreadCommentId).append(isResolved).append(labels).append(issue).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DiscussionInFeedDTO) == false) {
            return false;
        }
        DiscussionInFeedDTO rhs = ((DiscussionInFeedDTO) other);
        return new EqualsBuilder().append(projectId, rhs.projectId).append(discussionId, rhs.discussionId).append(anchor, rhs.anchor).append(comments, rhs.comments).append(review, rhs.review).append(read, rhs.read).append(isStarred, rhs.isStarred).append(firstUnreadCommentId, rhs.firstUnreadCommentId).append(isResolved, rhs.isResolved).append(labels, rhs.labels).append(issue, rhs.issue).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
