/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

//import com.fasterxml.jackson.annotation.JsonProperty;
//import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "projectId",
    "diffType",
    "newFile",
    "oldFile",
    "fileIcon",
    "isRead"
})
public class RevisionDiffItemDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("projectId")
    private String projectId;
    @JsonProperty("diffType")
    private long diffType;
    @JsonProperty("newFile")
    private FileInRevisionDTO newFile;
    @JsonProperty("oldFile")
    private FileInRevisionDTO oldFile;
    @JsonProperty("fileIcon")
    private String fileIcon;
    @JsonProperty("isRead")
    private boolean read;

    public RevisionDiffItemDTO() {
    }

    public RevisionDiffItemDTO(String projectId, long diffType, FileInRevisionDTO newFile, FileInRevisionDTO oldFile, String fileIcon, boolean read) {
        this.projectId = projectId;
        this.diffType = diffType;
        this.newFile = newFile;
        this.oldFile = oldFile;
        this.fileIcon = fileIcon;
        this.read = read;
    }

    @JsonProperty("projectId")
    public String getProjectId() {
        return projectId;
    }

    @JsonProperty("projectId")
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @JsonProperty("diffType")
    public long getDiffType() {
        return diffType;
    }

    @JsonProperty("diffType")
    public void setDiffType(long diffType) {
        this.diffType = diffType;
    }

    @JsonProperty("newFile")
    public FileInRevisionDTO getNewFile() {
        return newFile;
    }

    @JsonProperty("newFile")
    public void setNewFile(FileInRevisionDTO newFile) {
        this.newFile = newFile;
    }

    @JsonProperty("oldFile")
    public FileInRevisionDTO getOldFile() {
        return oldFile;
    }

    @JsonProperty("oldFile")
    public void setOldFile(FileInRevisionDTO oldFile) {
        this.oldFile = oldFile;
    }

    @JsonProperty("fileIcon")
    public String getFileIcon() {
        return fileIcon;
    }

    @JsonProperty("fileIcon")
    public void setFileIcon(String fileIcon) {
        this.fileIcon = fileIcon;
    }

    @JsonProperty("isRead")
    public boolean isRead() {
        return read;
    }

    @JsonProperty("isRead")
    public void setRead(boolean read) {
        this.read = read;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.projectId);
        hash = 59 * hash + (int) (this.diffType ^ (this.diffType >>> 32));
        hash = 59 * hash + Objects.hashCode(this.newFile);
        hash = 59 * hash + Objects.hashCode(this.oldFile);
        hash = 59 * hash + Objects.hashCode(this.fileIcon);
        hash = 59 * hash + (this.read ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RevisionDiffItemDTO other = (RevisionDiffItemDTO) obj;
        if (this.diffType != other.diffType) {
            return false;
        }
        if (this.read != other.read) {
            return false;
        }
        if (!Objects.equals(this.projectId, other.projectId)) {
            return false;
        }
        if (!Objects.equals(this.fileIcon, other.fileIcon)) {
            return false;
        }
        if (!Objects.equals(this.newFile, other.newFile)) {
            return false;
        }
        if (!Objects.equals(this.oldFile, other.oldFile)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RevisionDiffItemDTO{" + "projectId=" + projectId + ", diffType=" + diffType + ", newFile=" + newFile + ", oldFile=" + oldFile + ", fileIcon=" + fileIcon + ", read=" + read + '}';
    }   
}
