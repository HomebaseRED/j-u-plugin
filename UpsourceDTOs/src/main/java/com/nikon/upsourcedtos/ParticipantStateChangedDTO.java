
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "participant",
    "oldState",
    "newState"
})
public class ParticipantStateChangedDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("participant")
    private String participant;
    @JsonProperty("oldState")
    private long oldState;
    @JsonProperty("newState")
    private long newState;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -993942663447122316L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ParticipantStateChangedDTO() {
    }

    /**
     * 
     * @param oldState
     * @param participant
     * @param newState
     */
    public ParticipantStateChangedDTO(String participant, long oldState, long newState) {
        super();
        this.participant = participant;
        this.oldState = oldState;
        this.newState = newState;
    }

    @JsonProperty("participant")
    public String getParticipant() {
        return participant;
    }

    @JsonProperty("participant")
    public void setParticipant(String participant) {
        this.participant = participant;
    }

    @JsonProperty("oldState")
    public long getOldState() {
        return oldState;
    }

    @JsonProperty("oldState")
    public void setOldState(long oldState) {
        this.oldState = oldState;
    }

    @JsonProperty("newState")
    public long getNewState() {
        return newState;
    }

    @JsonProperty("newState")
    public void setNewState(long newState) {
        this.newState = newState;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(participant).append(oldState).append(newState).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ParticipantStateChangedDTO) == false) {
            return false;
        }
        ParticipantStateChangedDTO rhs = ((ParticipantStateChangedDTO) other);
        return new EqualsBuilder().append(participant, rhs.participant).append(oldState, rhs.oldState).append(newState, rhs.newState).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
