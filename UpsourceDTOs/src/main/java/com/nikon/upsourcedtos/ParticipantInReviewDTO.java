
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonTypeName("participant")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "userId",
    "role",
    "state"
})
public class ParticipantInReviewDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("userId")
    private String userId;
    @JsonProperty("role")
    private long role;
    @JsonProperty("state")
    private long state;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 8829694783432242059L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ParticipantInReviewDTO() {       
    }

    /**
     * 
     * @param userId
     * @param state
     * @param role
     */
    public ParticipantInReviewDTO(String userId, long role, long state) {
        super();
        this.userId = userId;
        this.role = role;
        this.state = state;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("role")
    public long getRole() {
        return role;
    }

    @JsonProperty("role")
    public void setRole(long role) {
        this.role = role;
    }

    @JsonProperty("state")
    public long getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(long state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(userId).append(role).append(state).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ParticipantInReviewDTO) == false) {
            return false;
        }
        ParticipantInReviewDTO rhs = ((ParticipantInReviewDTO) other);
        return new EqualsBuilder().append(userId, rhs.userId).append(role, rhs.role).append(state, rhs.state).append(additionalProperties, rhs.additionalProperties).isEquals();
    }
    
}
