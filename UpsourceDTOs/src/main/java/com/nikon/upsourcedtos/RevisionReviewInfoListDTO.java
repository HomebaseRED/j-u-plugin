/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RevisionReviewInfoListDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("reviewInfo")
    private List<RevisionReviewInfoDTO> reviewInfo = null;
    
    public RevisionReviewInfoListDTO(){}
    
    public RevisionReviewInfoListDTO(List<RevisionReviewInfoDTO> revisionReviewInfoDTO){
        this.reviewInfo = revisionReviewInfoDTO;
    }

    @JsonProperty("reviewInfo")
    public List<RevisionReviewInfoDTO> getReviewInfo() {
        return reviewInfo;
    }

    @JsonProperty("reviewInfo")
    public void setReviewInfo(List<RevisionReviewInfoDTO> reviewInfo) {
        this.reviewInfo = reviewInfo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.reviewInfo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RevisionReviewInfoListDTO other = (RevisionReviewInfoListDTO) obj;
        if (!Objects.equals(this.reviewInfo, other.reviewInfo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RevisionReviewInfoListDTO{" + "reviewInfo=" + reviewInfo + '}';
    }    
}
