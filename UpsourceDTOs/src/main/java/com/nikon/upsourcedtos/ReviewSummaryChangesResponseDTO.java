/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "diff",
    "annotation",
    "ignoredFiles",
    "fileDiffSummary",
    "resultKind"
})
public class ReviewSummaryChangesResponseDTO implements Serializable, UpsourceDTO{
    
    @JsonProperty("diff")
    private RevisionsDiffDTO diff;
    @JsonProperty("annotation")
    private String annotation;
    @JsonProperty("ignoredFiles")
    private List<String> ignoredFiles = null;
    @JsonProperty("fileDiffSummary")
    private List<FileDiffSummaryDTO> fileDiffSummary = null;
    @JsonProperty("resultKind")
    private long resultKind;

    public ReviewSummaryChangesResponseDTO(){}

    public ReviewSummaryChangesResponseDTO(RevisionsDiffDTO diff, String annotation, List<String> ignoredFiles, List<FileDiffSummaryDTO> fileDiffSummary, long resultKind) {
        this.diff = diff;
        this.annotation = annotation;
        this.ignoredFiles = ignoredFiles;
        this.fileDiffSummary = fileDiffSummary;
        this.resultKind = resultKind;
    }

    @JsonProperty("diff")
    public RevisionsDiffDTO getDiff() {
        return diff;
    }

    @JsonProperty("diff")
    public void setDiff(RevisionsDiffDTO diff) {
        this.diff = diff;
    }

    @JsonProperty("annotation")
    public String getAnnotation() {
        return annotation;
    }

    @JsonProperty("annotation")
    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    @JsonProperty("ignoredFiles")
    public List<String> getIgnoredFiles() {
        return ignoredFiles;
    }

    @JsonProperty("ignoredFiles")
    public void setIgnoredFiles(List<String> ignoredFiles) {
        this.ignoredFiles = ignoredFiles;
    }

    @JsonProperty("fileDiffSummary")
    public List<FileDiffSummaryDTO> getFileDiffSummary() {
        return fileDiffSummary;
    }

    @JsonProperty("fileDiffSummary")
    public void setFileDiffSummary(List<FileDiffSummaryDTO> fileDiffSummary) {
        this.fileDiffSummary = fileDiffSummary;
    }

    @JsonProperty("resultKind")
    public long getResultKind() {
        return resultKind;
    }

    @JsonProperty("resultKind")
    public void setResultKind(long resultKind) {
        this.resultKind = resultKind;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.diff);
        hash = 47 * hash + Objects.hashCode(this.annotation);
        hash = 47 * hash + Objects.hashCode(this.ignoredFiles);
        hash = 47 * hash + Objects.hashCode(this.fileDiffSummary);
        hash = 47 * hash + (int) (this.resultKind ^ (this.resultKind >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReviewSummaryChangesResponseDTO other = (ReviewSummaryChangesResponseDTO) obj;
        if (this.resultKind != other.resultKind) {
            return false;
        }
        if (!Objects.equals(this.annotation, other.annotation)) {
            return false;
        }
        if (!Objects.equals(this.diff, other.diff)) {
            return false;
        }
        if (!Objects.equals(this.ignoredFiles, other.ignoredFiles)) {
            return false;
        }
        if (!Objects.equals(this.fileDiffSummary, other.fileDiffSummary)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ReviewSummaryChangesResponseDTO{" + "diff=" + diff + ", annotation=" + annotation + ", ignoredFiles=" + ignoredFiles + ", fileDiffSummary=" + fileDiffSummary + ", resultKind=" + resultKind + '}';
    }
    
    
}
