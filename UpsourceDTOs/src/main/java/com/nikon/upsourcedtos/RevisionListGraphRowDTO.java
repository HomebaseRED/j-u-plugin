/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author RVanhuysse
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "nodes",
    "edges"    
})
public class RevisionListGraphRowDTO implements Serializable, UpsourceDTO {
   
    @JsonProperty("nodes")
    private List<RevisionListGraphNodeDTO> nodes;
    @JsonProperty("edges")
    private List<RevisionListGraphEdgeDTO> edges;
    
    public RevisionListGraphRowDTO(){}

    public RevisionListGraphRowDTO(List<RevisionListGraphNodeDTO> nodes, List<RevisionListGraphEdgeDTO> edges) {
        this.nodes = nodes;
        this.edges = edges;
    }

    @JsonProperty("nodes")
    public List<RevisionListGraphNodeDTO> getNodes() {
        return nodes;
    }

    @JsonProperty("nodes")
    public void setNodes(List<RevisionListGraphNodeDTO> nodes) {
        this.nodes = nodes;
    }

    @JsonProperty("edges")
    public List<RevisionListGraphEdgeDTO> getEdges() {
        return edges;
    }

    @JsonProperty("edges")
    public void setEdges(List<RevisionListGraphEdgeDTO> edges) {
        this.edges = edges;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.nodes);
        hash = 19 * hash + Objects.hashCode(this.edges);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RevisionListGraphRowDTO other = (RevisionListGraphRowDTO) obj;
        if (!Objects.equals(this.nodes, other.nodes)) {
            return false;
        }
        if (!Objects.equals(this.edges, other.edges)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RevisionListGraphRowDTO{" + "nodes=" + nodes + ", edges=" + edges + '}';
    }
}
