
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "feedItems",
    "hasMore"
})
public class FeedDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("feedItems")
    private List<FeedItemDTO> feedItems = null;
    @JsonProperty("hasMore")
    private boolean hasMore;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -3927166649730684928L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public FeedDTO() {
    }

    /**
     * 
     * @param hasMore
     * @param feedItems
     */
    public FeedDTO(List<FeedItemDTO> feedItems, boolean hasMore) {
        super();
        this.feedItems = feedItems;
        this.hasMore = hasMore;
    }

    @JsonProperty("feedItems")
    public List<FeedItemDTO> getFeedItems() {
        return feedItems;
    }

    @JsonProperty("feedItems")
    public void setFeedItems(List<FeedItemDTO> feedItems) {
        this.feedItems = feedItems;
    }

    @JsonProperty("hasMore")
    public boolean isHasMore() {
        return hasMore;
    }

    @JsonProperty("hasMore")
    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(feedItems).append(hasMore).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof FeedDTO) == false) {
            return false;
        }
        FeedDTO rhs = ((FeedDTO) other);
        return new EqualsBuilder().append(feedItems, rhs.feedItems).append(hasMore, rhs.hasMore).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
