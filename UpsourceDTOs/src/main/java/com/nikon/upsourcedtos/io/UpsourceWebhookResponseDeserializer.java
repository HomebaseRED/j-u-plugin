/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos.io;

import com.nikon.upsourcedtos.UpsourceDTO;
import com.nikon.upsourcedtos.UpsourceWebhookResponse;
import java.io.IOException;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.ObjectCodec;
import static org.codehaus.jackson.map.DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author RVanhuysse
 */
public class UpsourceWebhookResponseDeserializer extends JsonDeserializer<UpsourceWebhookResponse> {

    @Override
    public UpsourceWebhookResponse deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        ObjectCodec oc = jp.getCodec();
        JsonNode node = oc.readTree(jp);
        long majorVersion = node.get("majorVersion").asLong();
        long minorVersion = node.get("minorVersion").asLong();
        String projectId = node.get("projectId").asText();
        String dataType = node.get("dataType").asText();
        String packageName = "com.nikon.upsourcedtos";
        JsonNode unparsedData = node.get("data");
        Class dataClass = null;
        try{
            String fullyQualifiedName = packageName+"."+dataType;
            dataClass = Class.forName(fullyQualifiedName);
        }catch(ClassNotFoundException cfe){
            throw new IOException("Class not found: "+dataType,cfe);
        }
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        UpsourceDTO data = (UpsourceDTO) mapper.readValue(unparsedData, dataClass);
        return new UpsourceWebhookResponse(majorVersion, minorVersion, projectId, dataType, data);
    }
    
}
