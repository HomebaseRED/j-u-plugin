/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nikon.upsourcedtos.io;

import java.util.Collections;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author RVanhuysse
 */
public class UpsourceEnumValueConverter {
    
    private static final SortedMap<Long, String> reviewStateConversionMap;
    private static final SortedMap<Long, String> roleConversionMap;
    static{
        SortedMap<Long, String> convertStates = new TreeMap<Long, String>();
        convertStates.put(1L, "Unread");
        convertStates.put(2L, "Read");
        convertStates.put(3L, "Accepted");
        convertStates.put(4L, "Rejected");
        reviewStateConversionMap = Collections.unmodifiableSortedMap(convertStates);
        SortedMap<Long, String> convertRoles = new TreeMap<Long, String>();
        convertRoles.put(1L, "Author");
        convertRoles.put(2L, "Reviewer");
        convertRoles.put(3L, "Watcher");
        roleConversionMap = Collections.unmodifiableSortedMap(convertRoles);
    }

    public static SortedMap<Long, String> getReviewStateConversionMap() {
        return reviewStateConversionMap;
    }

    public static SortedMap<Long, String> getRoleConversionMap() {
        return roleConversionMap;
    }
   
    public static String convertReviewState(long state){
        return UpsourceEnumValueConverter.reviewStateConversionMap.get(state);
    }
    
    public static String convertRole(long role){
        return UpsourceEnumValueConverter.roleConversionMap.get(role);
    }
}
