
package com.nikon.upsourcedtos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "projectId",
    "reviewId"
})
public class RemovedReviewDTO implements Serializable, UpsourceDTO
{

    @JsonProperty("projectId")
    private String projectId;
    @JsonProperty("reviewId")
    private String reviewId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -2408324661953846327L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public RemovedReviewDTO() {
    }

    /**
     * 
     * @param projectId
     * @param reviewId
     */
    public RemovedReviewDTO(String projectId, String reviewId) {
        super();
        this.projectId = projectId;
        this.reviewId = reviewId;
    }

    @JsonProperty("projectId")
    public String getProjectId() {
        return projectId;
    }

    @JsonProperty("projectId")
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @JsonProperty("reviewId")
    public String getReviewId() {
        return reviewId;
    }

    @JsonProperty("reviewId")
    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(projectId).append(reviewId).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RemovedReviewDTO) == false) {
            return false;
        }
        RemovedReviewDTO rhs = ((RemovedReviewDTO) other);
        return new EqualsBuilder().append(projectId, rhs.projectId).append(reviewId, rhs.reviewId).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
